
Require Import Definitions.
Require Import Event.
Require Import ListOrder.
Require Import Trace.
Require Import ProgramOrder.
Require Import Race.
Require Import List Arith.

Section CausalEvent.

  Variable C : list Event -> Event -> Event -> Prop.
  Variable causality_trace_order : forall t x y,
      NoDup t -> C t x y -> x = y \/ Lt Event t x y.
  Variable program_order_causality : forall t x y,
      ProgramOrder t x y -> C t x y.
  Variable causality_trans : forall t x y z,
      C t x y ->
      C t y z ->
      C t x z.
  Variable causality_cons : forall t x y z,
      Trace (z :: t) ->
      C t x y ->
      C (z :: t) x y.
  Variable causality_thread_neq_exists_rel : forall t x y,
      NoDup t ->
      C t x y ->
      tid x <> tid y ->
      ~ is_rel x ->
      (exists r, is_rel r /\ ProgramOrder t x r /\ C t r y).
  
  Hint Resolve race_trace.
  Hint Resolve trace_nodup.
  Hint Resolve trace_lock_semantics.
  Lemma observes_write_causality : forall t xr yr xyrace r w,
      ObservesWrite r w t ->
      CausalEvent C t xr yr xyrace r ->
      C t w r.
  Proof.
    intros.
    inversion xyrace; subst.
    apply causality_cons; eauto.
    apply H2.
    apply lt_tl with yr.
    intuition; subst.
    contradict H0; eauto using first_race_yr_not_causal_event.
    eauto using observes_write_trace_order.
    eauto using conflicts_symm, observes_write_conflicts.
  Qed.
  Hint Resolve observes_write_causality.
  
  Lemma isolated_section_causal_event_or_orphaned_acquire exe x y xyrace : forall a r,
      IsolatedSection a r exe ->
      CausalEvent C exe x y xyrace a ->
      CausalEvent C exe x y xyrace r \/ OrphanedAcquire C exe x y xyrace a.
  Proof.
    intros.
    assert (NoDup exe) by eauto.
    assert (tid a = tid r) by eauto using isolated_section_thread_eq.
    assert (r <> x).
    intuition; subst.
    absurd (is_access x).
    eauto using isolated_section_rel.
    destruct (first_race_access C exe x y); auto.
    assert (a <> x).
    intuition; subst.
    absurd (is_access x).
    apply absurd_acq_access.
    eapply isolated_section_acq; eauto.
    destruct (first_race_access C exe x y); auto.
    assert (r <> y).
    intuition; subst.
    absurd (is_access y).
    apply absurd_rel_access.
    eapply isolated_section_rel; eauto.
    destruct (first_race_access C exe x y); auto.
    destruct (lt_total_order Event exe H1 a x); auto.
    eauto using isolated_section_acq_in.
    destruct (first_race_in C exe x y); auto.
    destruct (lt_total_order Event exe H1 r x); auto.
    eauto using isolated_section_rel_in.
    destruct (first_race_in C exe x y); auto.
    - inversion H0; subst.
      + destruct (Nat.eq_dec (tid a) (tid x)).
        * left.
          apply causal_event_x; eauto.
          apply program_order_causality.
          apply program_trace_rule; auto.
          congruence.
        * destruct (causality_thread_neq_exists_rel exe a x); auto.
          apply absurd_acq_rel.
          eapply isolated_section_acq; eauto.
          intuition.
          destruct (Event_eq_dec r x0); subst.
          left.
          apply causal_event_x; eauto.
          left.
          apply causal_event_x; eauto.
          apply causality_trans with x0.
          apply program_order_causality.
          apply program_trace_rule.
          apply lock_semantics_critical_trace_order1 with a; auto.
          eauto.
          inversion H11; subst; congruence.
          auto.
      + destruct (Nat.eq_dec (tid a) (tid y)).
        * left.
          apply causal_event_y; eauto.
          apply program_order_causality.
          apply program_trace_rule.
          destruct (first_race_all_before_yr C exe x y xyrace r).
          eauto using isolated_section_rel_in.
          contradiction.
          auto.
          congruence.
        * destruct (causality_thread_neq_exists_rel exe a y); auto.
          apply absurd_acq_rel.
          eapply isolated_section_acq; eauto.
          intuition.
          destruct (Event_eq_dec r x0); subst.
          left.
          apply causal_event_y; eauto.
          left.
          apply causal_event_y; eauto.
          apply causality_trans with x0.
          apply program_order_causality.
          apply program_trace_rule.
          apply lock_semantics_critical_trace_order1 with a; auto.
          eauto.
          inversion H11; subst; congruence.
          auto.
    - destruct (Nat.eq_dec (tid a) (tid x)).
      + assert (ClosedIsolatedEvent a x r exe).
        apply isolated_section_closed_isolated_event; auto.
        destruct (first_race_access C exe x y); auto.
        right.
        apply orphaned_acquire_isolated_section with r; auto.
        intuition.
        inversion H9; subst.
        apply causality_trace_order in H12; intuition.
        apply H11.
        apply lt_antisymm with exe; eauto using closed_isolated_event_trace_order3.
        inversion xyrace; subst.
        inversion H13; subst.
        apply H17.
        apply causality_trans with r.
        apply program_order_causality.
        apply program_trace_rule.
        eauto using closed_isolated_event_trace_order3.
        congruence.
        auto.
      + destruct (Nat.eq_dec (tid a) (tid y)).
        * left.
          apply causal_event_y; eauto.
          apply program_order_causality.
          apply program_trace_rule.
          destruct (first_race_all_before_yr C exe x y xyrace r).
          eauto using isolated_section_rel_in.
          contradiction.
          auto.
          congruence.
        * inversion H0; subst.
          destruct (causality_thread_neq_exists_rel exe a x); auto.
          apply absurd_acq_rel.
          eapply isolated_section_acq; eauto.
          intuition.
          destruct (Event_eq_dec r x0); subst.
          left.
          apply causal_event_x; eauto.
          left.
          apply causal_event_x; eauto.
          apply causality_trans with x0.
          apply program_order_causality.
          apply program_trace_rule.
          apply lock_semantics_critical_trace_order1 with a; eauto.
          inversion H11; subst; congruence.
          auto.
          destruct (causality_thread_neq_exists_rel exe a y); auto.
          apply absurd_acq_rel.
          eapply isolated_section_acq; eauto.
          intuition.
          destruct (Event_eq_dec r x0); subst.
          left.
          apply causal_event_y; eauto.
          left.
          apply causal_event_y; eauto.
          apply causality_trans with x0.
          apply program_order_causality.
          apply program_trace_rule.
          apply lock_semantics_critical_trace_order1 with a; eauto.
          inversion H11; subst; congruence.
          auto.
    - inversion H0; subst.
      apply causality_trace_order in H9; intuition.
      absurd (a = x); eauto using lt_neq, lt_antisymm.
      destruct (Nat.eq_dec (tid a) (tid y)).
      + left.
        apply causal_event_y; eauto.
        apply program_order_causality.
        apply program_trace_rule.
        destruct (first_race_all_before_yr C exe x y xyrace r).
        eauto using isolated_section_rel_in.
        contradiction.
        auto.
        congruence.
      + destruct (causality_thread_neq_exists_rel exe a y); auto.
        apply absurd_acq_rel.
        eapply  isolated_section_acq; eauto.
        intuition.
        destruct (Event_eq_dec r x0); subst.
        left.
        apply causal_event_y; eauto.
        left.
        apply causal_event_y; eauto.
        apply causality_trans with x0.
        apply program_order_causality.
        apply program_trace_rule.
        apply lock_semantics_critical_trace_order1 with a; eauto.
        inversion H10; subst; congruence.
        auto.
  Qed.

  Lemma causal_event_in exe xr yr xyrace : forall t x,
      CausalEvents C exe xr yr xyrace t ->
      CausalEvent C exe xr yr xyrace x ->
      In x t.
  Proof.
    intros.
    apply H.
    auto.
  Qed.

  Lemma in_causal_event exe xr yr xyrace : forall t x,
      CausalEvents C exe xr yr xyrace t ->
      In x t ->
      CausalEvent C exe xr yr xyrace x.
  Proof.
    intros.
    apply H.
    auto.
  Qed.
  
  Lemma causal_event_in_exe exe xr yr xyrace : forall e,
      CausalEvent C exe xr yr xyrace e ->
      In e exe.
  Proof.
    inversion 1; subst; eauto.
  Qed.

  Lemma causal_event_rec exe xr yr xyrace : forall x y,
      CausalEvent C exe xr yr xyrace y ->
      ProgramOrder exe x y ->
      CausalEvent C exe xr yr xyrace x.
  Proof.
    inversion 1; subst; intros.
    apply causal_event_x.
    intuition; subst.
    destruct (program_order_in exe x y); intuition.
    intuition; subst.
    inversion H3; subst.
    apply causality_trace_order in H2; intuition.
    absurd (y = xr); eauto using lt_neq, lt_antisymm.
    eauto.
    apply causality_trans with y; auto.
    apply causal_event_y.
    destruct (program_order_in exe x y); intuition.
    intuition; subst.
    apply causality_trace_order in H2; intuition.
    inversion H3; subst.
    absurd (y = yr); eauto using lt_neq, lt_antisymm.
    eauto.
    apply causality_trans with y; auto.
  Qed.

  Lemma causal_event_rec' exe xr yr xyrace : forall x y,
      CausalEvent C exe xr yr xyrace y ->
      In x exe ->
      C exe x y ->
      CausalEvent C exe xr yr xyrace x.
  Proof.
    inversion 1; subst; intros.
    destruct (Event_eq_dec x xr); subst.
    destruct (causality_trace_order exe y xr); auto; subst.
    eauto.
    contradiction.
    destruct (causality_trace_order exe xr y); auto; subst.
    eauto.
    contradiction.
    contradict H1; eauto using lt_antisymm.
    apply causal_event_x; eauto.
    destruct (Event_eq_dec x yr); subst.
    destruct (causality_trace_order exe y yr); auto; subst.
    eauto.
    contradiction.
    destruct (causality_trace_order exe yr y); auto; subst.
    eauto.
    contradiction.
    contradict H1; eauto using lt_antisymm.
    apply causal_event_y; eauto.
  Qed.
  
  Lemma causal_event_observes exe xr yr xyrace : forall x y,
      CausalEvent C exe xr yr xyrace y ->
      ObservesWrite y x exe ->
      CausalEvent C exe xr yr xyrace x.
  Proof.
    intros.
    inversion xyrace; subst.
    assert (Lt Event (yr :: t) x y) by eauto using observes_write_trace_order.
    assert (Lt Event t x y).
    apply lt_tl with yr; auto.
    intuition; subst.
    contradict H; auto using first_race_yr_not_causal_event.
    inversion H; subst.
    apply causal_event_x.
    eauto using observes_write_write_in.
    intuition; subst.
    apply causality_trace_order in H7; intuition.
    absurd (y = xr); eauto using lt_neq, lt_antisymm.
    eauto.
    apply causality_trans with y; auto.
    apply causality_cons.
    eauto.
    apply H2; eauto using observes_write_conflicts.
    apply causal_event_y.
    eauto using observes_write_write_in.
    intuition; subst.
    apply causality_trace_order in H7; intuition.
    absurd (y = yr); eauto using lt_neq, lt_antisymm.
    eauto.
    apply causality_trans with y; auto.
    apply causality_cons.
    eauto.
    apply H2; eauto using observes_write_conflicts.
  Qed.
    
  Lemma isolated_section_rel_causal_event_acq_causal_event exe xr yr xyrace :
    forall a r,
      IsolatedSection a r exe ->
      CausalEvent C exe xr yr xyrace r ->
      CausalEvent C exe xr yr xyrace a.
  Proof.
    intros.
    apply causal_event_rec with r; eauto.
  Qed.

End CausalEvent.

Hint Resolve isolated_section_rel_causal_event_acq_causal_event.
Hint Resolve causal_event_in.
Hint Resolve in_causal_event.
Hint Resolve causal_event_in_exe.


Require Import Definitions.
Require Import Event.
Require Import ListOrder.
Require Import Trace.
Require Import ProgramOrder.
Require Import List Arith Relations Relation_Operators Operators_Properties.

Section DC.

  Hint Resolve program_order_trace_order.
  Lemma dc_relation_trace_order (l : list Event) (N: NoDup l) : forall e1 e2,
      DCRelation l e1 e2 -> Lt Event l e1 e2.
  Proof.
    intros x y H; inversion H; subst; auto.
    apply ListOrder.lt_trans with ay; auto.
    apply ListOrder.lt_trans with ay; auto.
    eauto using closed_isolated_event_trace_order1.
  Qed.
  Hint Resolve dc_relation_trace_order.

  Hint Resolve program_order_in.
  Lemma dc_relation_in (l : list Event) : forall e1 e2,
      DCRelation l e1 e2 -> In e1 l /\ In e2 l.
  Proof.
    inversion 1; subst; eauto.
  Qed.
  Hint Resolve dc_relation_in.

  Lemma dc_order_trace_order (l : list Event) (N: NoDup l) : forall e1 e2,
      DCOrder l e1 e2 -> e1 = e2 \/ Lt Event l e1 e2.
  Proof.
    induction 1; intros; auto.
    destruct (IHclos_refl_trans_1n); subst.
    right; eauto using dc_relation_trace_order.
    right.
    apply ListOrder.lt_trans with y; eauto using dc_relation_trace_order.
  Qed.
  Hint Resolve dc_order_trace_order.
  
  Lemma dc_order_antisymm (l : list Event) (N: NoDup l) : forall e1 e2,
      DCOrder l e1 e2 -> DCOrder l e2 e1 -> e1 = e2.
  Proof.
    intros.
    assert (e1 = e2 \/ Lt Event l e1 e2) by eauto.
    assert (e2 = e1 \/ Lt Event l e2 e1) by eauto.
    intuition.
    eauto.
  Qed.

  Lemma absurd_dc_relation_nil : forall i j,
      ~ DCRelation nil i j.
  Proof.
    intuition.
    induction H;
      try match goal with
          | [ H : ProgramOrder _ _ _ |- _ ] =>
            inversion H
          end;
      match goal with
      | [ H0 : Lt _ _ _ _ |- _ ] =>
        inversion H0;
          match goal with
          | [ H1 : IndexOf _ _ _ _ |- _ ] =>
            inversion H1
          end
      end.
  Qed.
  Hint Resolve absurd_dc_relation_nil.

  Hint Constructors Lt.
  Hint Constructors IndexOf.
  Hint Constructors DCRelation.
  Hint Constructors ProgramOrder.
  Hint Constructors ClosedIsolatedEvent.
  Hint Constructors OpenIsolatedEvent.
  Hint Resolve lt_cons.
  Hint Resolve open_isolated_event_open_acquire.
  Hint Resolve maps_top_funp2.
  Lemma dc_relation_cons : forall t i j k,
      Trace (k :: t) -> DCRelation t i j -> DCRelation (k :: t) i j.
  Proof.
    intros.
    inversion H; subst.
    inversion H0; subst.
    inversion H4; subst; eauto.
    destruct k.
    eapply dc_critical_rule1; eauto.
    apply open_isolated_event_cons; unfold is_access; simpl; auto.
    eapply dc_critical_rule1; eauto.
    apply open_isolated_event_cons; unfold is_access; simpl; auto.
    inversion H2; subst.
    contradict H11; eauto using open_isolated_event_open_acquire.
    contradict H10; simpl; auto.
    contradict H10; unfold is_access; simpl; intuition.
    eapply dc_critical_rule2; eauto.
    apply closed_isolated_event_eq; auto.
    assert (exists n, MapsToP Event is_critical ay n t).
    eauto using open_isolated_event_acq_maps_top.
    destruct H8.
    eapply isolated_section_eq; simpl; eauto.
    destruct (Nat.eq_dec (tid ay) n0); auto.
    inversion H2; subst; try event_types.
    eapply open_isolated_event_open_acquire in H5.
    assert (a = ay) by eauto using open_acquire_fun; subst.
    simpl in H13; contradiction.
    unfold is_access in *; simpl in *; intuition.
    eapply dc_critical_rule2; eauto.
  Qed.

  Hint Resolve nodup_tl.
  Lemma dc_relation_cons_inv : forall t x y z,
      NoDup (z :: t) -> z <> y -> DCRelation (z :: t) x y -> DCRelation t x y.
  Proof.
    intros.
    assert (Lt Event (z :: t) x y) by auto.
    assert (Lt Event t x y) by eauto using lt_tl.
    inversion H1; subst.
    - inversion H4; eauto.
    - inversion H4; subst.
      contradict H7; auto using absurd_lt_hd.
      inversion H5; subst.
      contradiction.
      apply dc_critical_rule1 with (x:=x0) (ax:=ax) (ay:=ay); auto.
      eapply closed_open_isolated_event_critical_trace_order; eauto.
    - inversion H4; subst.
      contradict H7; auto using absurd_lt_hd.
      inversion H5; subst.
      apply dc_critical_rule1 with (x:=x0) (ax:=ax) (ay:=ay); auto.
      eapply closed_open_isolated_event_critical_trace_order; eauto.
      apply dc_critical_rule2 with (x:=x0) (ax:=ax) (ay:=ay) (ry:=ry); auto.
      eapply isolated_sections_trace_order; eauto using nodup_tl.
      apply closed_isolated_event_isolated_section with (a:=ax) (x:=x0); eauto.
      apply closed_isolated_event_isolated_section with (x:=y); eauto.
      eapply closed_isolated_event_acq_trace_order; eauto.
      intuition; subst.
      assert (Lt Event t ay x).
      eapply closed_isolated_event_trace_order2; eauto.
      apply lt_cons with (z:=z) in H8.
      assert (x = ay) by eauto using lt_antisymm; subst.
      contradict H8; eauto using lt_irrefl.
      apply ListOrder.lt_trans with x; eauto.
  Qed.

  Lemma dc_order_in : forall t x y,
      x <> y -> DCOrder t x y -> In x t /\ In y t.
  Proof.
    intros.
    induction H0.
    contradiction.
    split.
    destruct (dc_relation_in t x y); auto.
    destruct (Event_eq_dec y z); subst.
    destruct (dc_relation_in t x z); auto.
    intuition.
  Qed.
  
  Lemma dc_order_cons_inv : forall t x y z,
      NoDup (z :: t) -> z <> y -> DCOrder (z :: t) x y -> DCOrder t x y.
  Proof.
    intros.
    induction H1.
    constructor.
    apply IHclos_refl_trans_1n in H0 as Hyz0.
    destruct (Event_eq_dec x y); subst; auto.
    destruct (Event_eq_dec z y); subst.
    inversion H; subst.
    contradict H5.
    destruct (dc_order_in t y z0); auto.
    assert (DCRelation t x y).
    eapply dc_relation_cons_inv; eauto.
    apply Relation_Operators.rt1n_trans with y; auto.
  Qed.
    
  Lemma dc_order_cons : forall t i j k,
      Trace (k :: t) -> DCOrder t i j -> DCOrder (k :: t) i j.
  Proof.
    intros.
    induction H0.
    constructor.
    apply Relation_Operators.rt1n_trans with y.
    eauto using dc_relation_cons.
    eauto using dc_relation_cons.
  Qed.

  Hint Resolve open_isolated_event_in.
  Hint Resolve closed_isolated_event_in.
  Lemma dc_isolated_events_ordered : forall t x y,
      NoDup t ->
      IsolatedEvent x t ->
      IsolatedEvent y t ->
      conflicts x y ->
      Lt Event t x y ->
      DCOrder t x y.
  Proof.
    intros.
    inversion H0; inversion H1; subst.
    apply Operators_Properties.clos_rt1n_step.
    assert (a = a0).
    eapply open_isolated_event_fun; eauto.
    subst.
    apply dc_program_rule.
    eapply open_isolated_events_program_order; eauto.
    assert (Lt Event t r a).
    eapply closed_open_isolated_event_critical_trace_order; eauto.
    absurd (r = a).
    eauto using lt_neq.
    apply lt_antisymm with t; auto.
    apply ListOrder.lt_trans with x; auto.
    eauto using open_isolated_event_trace_order.
    apply ListOrder.lt_trans with y; auto.
    eauto using closed_isolated_event_trace_order3.
    apply Relation_Operators.rt1n_trans with r.
    apply dc_program_rule.
    eauto using closed_isolated_event_release_program_order.
    apply Operators_Properties.clos_rt1n_step.
    eapply dc_critical_rule1; eauto.
    eauto using closed_open_isolated_event_critical_trace_order.
    destruct (Event_eq_dec a a0); subst.
    apply Operators_Properties.clos_rt1n_step.
    apply dc_program_rule.
    assert (r = r0).
    eauto using closed_isolated_event_fun_rel.
    subst.
    eauto using closed_isolated_events_program_order.
    apply Relation_Operators.rt1n_trans with r.
    apply dc_program_rule.
    eauto using closed_isolated_event_release_program_order.
    apply Operators_Properties.clos_rt1n_step.
    eapply dc_critical_rule2; eauto.
    apply closed_isolated_event_isolated_section in H4 as Hx; auto.
    apply closed_isolated_event_isolated_section in H7 as Hy; auto.
    eapply isolated_sections_trace_order; eauto.
    eapply closed_isolated_event_acq_trace_order; eauto.
  Qed.
  
  Lemma dc_relation_program_order : forall t x y,
      DCRelation t x y -> ~ is_rel x -> ProgramOrder t x y.
  Proof.
    inversion 1; intros; subst; auto.
    contradict H6.
    eauto using closed_isolated_event_rel.
    contradict H6.
    eauto using closed_isolated_event_rel.
  Qed.

  Lemma dc_order_thread_neq_rel : forall t x y,
      NoDup t ->
      DCOrder t x y ->
      tid x <> tid y ->
      ~ is_rel x ->
      (exists r, is_rel r /\ ProgramOrder t x r /\ DCOrder t r y).
  Proof.
    intros.
    induction H0.
    - congruence.
    - assert (ProgramOrder t x y).
      eapply dc_relation_program_order; auto.
      assert (tid y <> tid z).
      intuition; subst.
      inversion H4; subst; congruence.
      destruct (is_rel_dec y).
      exists y; intuition.
      destruct (IHclos_refl_trans_1n); auto.
      destruct H7.
      destruct H8.
      exists x0; split; auto.
      split; auto.
      inversion H4; inversion H8; subst.
      apply program_trace_rule; eauto using ListOrder.lt_trans.
      congruence.
  Qed.
      
  Lemma dc_order_transitive : forall t x y z,
    DCOrder t x y -> DCOrder t y z -> DCOrder t x z.
  Proof.
    unfold DCOrder; intros.
    apply clos_rt_rt1n_iff.
    apply clos_rt_rt1n_iff in H.
    apply clos_rt_rt1n_iff in H0.
    apply rt_trans with y; auto.
  Qed.

End DC.

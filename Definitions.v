
Require Import List Relations Arith.

Section ListOrder.

  Variable A:Type.

  Inductive MapsToP (P:A->Prop) : A -> nat -> list A -> Prop :=
  | maps_top_eq: forall x l,
      P x -> MapsToP P x (length l) (x::l)
  | maps_top_cons: forall x l y n,
      ~ P y -> MapsToP P x n l -> MapsToP P x n (y :: l).

  Inductive MappedToP (P:A->Prop) : A -> A -> nat -> list A -> Prop :=
  | mapped_top_eq: forall x y n l,
      MapsToP P x n l -> P y -> MappedToP P y x n (y :: l)
  | mapped_top_cons: forall x l y z n,
      MappedToP P y x n l -> MappedToP P y x n (z :: l).

  Inductive IndexOf (x:A) : nat -> list A -> Prop :=
  | index_of_eq: forall l,
      IndexOf x (length l) (x :: l)
  | index_of_cons: forall y n l,
      IndexOf x n l -> IndexOf x n (y :: l).
  
  Inductive Lt (l:list A) (x:A) (y:A) : Prop :=
  | lt_def: forall xn yn,
      IndexOf x xn l ->
      IndexOf y yn l ->
      xn < yn ->
      Lt l x y.

End ListOrder.

Section Event.

  Inductive Event : Set :=
  | Rd : nat -> nat -> nat -> Event
  | Wr : nat -> nat -> nat -> Event
  | Acq : nat -> nat -> Event
  | Rel : nat -> nat -> Event.
  Scheme Equality for Event.
      
  Definition tid (e : Event) :=
    match e with
    | Rd _ t _ => t
    | Wr _ t _ => t
    | Rel _ t => t
    | Acq _ t => t
    end.
  
  Definition var (e : Event) : option nat :=
    match e with
    | Rd _ _ v => Some v
    | Wr _ _ v => Some v
    | _ => None
    end.
  
  Definition is_read (e : Event) : Prop :=
    match e with
    | Rd _ _ _ => True
    | _ => False
    end.

  Definition is_write (e : Event) : Prop :=
    match e with
    | Wr _ _ _ => True
    | _ => False
    end.
  
  Definition is_rel (e : Event) : Prop :=
    match e with
    | Rel _ _ => True
    | _ => False
    end.

  Definition is_acq (e : Event) : Prop :=
    match e with
    | Acq _ _ => True
    | _ => False
    end.
  
  Definition is_access (e : Event) : Prop := is_read e \/ is_write e.
  
  Definition is_critical (e : Event) : Prop := is_acq e \/ is_rel e.

  Definition is_same_var (e1 e2 : Event) : Prop := var e1 = var e2.
  
  Definition conflicts (e1 e2 : Event) : Prop :=
    is_same_var e1 e2 /\ (is_write e1 \/ is_write e2).

End Event.

Section Trace.

  Inductive OpenAcquire : Event -> list Event -> Prop :=
  | open_acquire_eq : forall e l,
      is_acq e -> OpenAcquire e (e::l)
  | open_acquire_cons : forall a e l,
      is_access e -> OpenAcquire a l -> OpenAcquire a (e::l).

  Inductive LockSemantics : list Event -> Prop :=
  | lock_semantics_nil : LockSemantics nil
  | lock_semantics_acq : forall e l,
      is_acq e -> ~ (exists a, OpenAcquire a l) -> LockSemantics l ->
      LockSemantics (e::l)
  | lock_semantics_rel : forall e a l,
      is_rel e -> OpenAcquire a l ->  tid a = tid e ->
      LockSemantics l -> LockSemantics (e::l)
  | lock_semantics_other : forall e l,
      is_access e -> LockSemantics l -> LockSemantics (e::l).

  Inductive IsolatedSection : Event -> Event -> list Event -> Prop :=
  | isolated_section_eq : forall e a l,
      is_rel e -> OpenAcquire a l -> tid a = tid e ->
      IsolatedSection a e (e::l)
  | isolated_section_cons : forall e r a l,
      IsolatedSection a r l -> IsolatedSection a r (e::l).

  Inductive OpenIsolatedEvent : Event -> Event -> list Event -> Prop :=
  | open_isolated_event_eq : forall e a l,
      is_access e -> OpenAcquire a l ->  tid a = tid e ->
      OpenIsolatedEvent a e (e::l)
  | open_isolated_event_cons : forall e a r l,
      is_access r -> OpenIsolatedEvent a e l -> OpenIsolatedEvent a e (r::l).

  Inductive ClosedIsolatedEvent : Event -> Event -> Event -> list Event -> Prop :=
  | closed_isolated_event_eq : forall e r a l,
      IsolatedSection a r (r::l) -> OpenIsolatedEvent a e l ->
      ClosedIsolatedEvent a e r (r::l)
  | closed_isolated_event_cons : forall x r a y l,
      ClosedIsolatedEvent a x r l -> ClosedIsolatedEvent a x r (y::l).
  
  Inductive IsolatedEvent : Event -> list Event -> Prop :=
  | isolated_event_open : forall e a l,
      OpenIsolatedEvent a e l -> IsolatedEvent e l
  | isolated_event_closed : forall e a r l,
      ClosedIsolatedEvent a e r l -> IsolatedEvent e l.

  Inductive ObservesWrite : Event -> Event -> list Event -> Prop :=
  | observes_write_eq : forall e n w l,
      is_read e -> MapsToP Event (conflicts e) w n l -> ObservesWrite e w (e::l)
  | observes_write_cons : forall e w e' l,
      ObservesWrite e w l -> ObservesWrite e w (e'::l).

  Inductive Trace : list Event -> Prop :=
  | trace_def : forall l,
      NoDup l -> LockSemantics l -> True -> Trace l.

End Trace.

Section Race.

  Variable C : list Event -> Event -> Event -> Prop.
  
  Inductive Race : list Event -> Event -> Event -> Prop :=
  | race_def : forall t x y,
      Trace t ->
      Lt Event t x y ->
      ~ C t x y ->
      conflicts x y ->
      ~ IsolatedEvent x t \/ ~ IsolatedEvent y t ->
      (** this excludes intended data races from the definition of
          race only to reason about the WDC order which leaves some
          intended data races unordered while otherwise remaining sound.
          it is redundant for other partial orders that order conflicting
          events in critical sections. **)
      Race t x y.

  Inductive FirstRace : list Event -> Event -> Event -> Prop :=
  | first_race_def : forall t x y,
      Race (y :: t) x y ->
      (forall i j, Lt Event t i j -> conflicts i j -> C t i j) ->
      FirstRace (y :: t) x y.
  
End Race.

Section ProgramOrder.

  Inductive ProgramOrder (l : list Event) : relation Event :=
  | program_trace_rule : forall e1 e2,
      Lt Event l e1 e2 -> tid e1 = tid e2 -> ProgramOrder l e1 e2.

End ProgramOrder.

Section Witness.

  Variable C : list Event -> Event -> Event -> Prop.
  
  Inductive CausalEvent exe xr yr (xyrace : FirstRace C exe xr yr) :
    Event -> Prop :=
  | causal_event_x : forall e,
      In e exe -> e <> xr -> C exe e xr -> CausalEvent exe xr yr xyrace e
  | causal_event_y : forall e,
      In e exe -> e <> yr -> C exe e yr -> CausalEvent exe xr yr xyrace e.
      
  Definition CausalEvents exe xr yr xyrace (l : list Event) :=
    forall e, In e l <-> CausalEvent exe xr yr xyrace e.

  Inductive OrphanedAcquire exe xr yr xyrace : Event -> Prop :=
  | orphaned_acquire_isolated_section : forall a r,
      IsolatedSection a r exe ->
      CausalEvent exe xr yr xyrace a ->
      ~ CausalEvent exe xr yr xyrace r ->
      OrphanedAcquire exe xr yr xyrace a
  | orphaned_acquire_open : forall a,
      CausalEvent exe xr yr xyrace a ->
      OpenAcquire a exe ->
      OrphanedAcquire exe xr yr xyrace a.
  
  Inductive WitnessRelation exe xr yr xyrace : Event -> Event -> Prop :=
  | witness_step : forall x y,
      C exe x y -> WitnessRelation exe xr yr xyrace x y
  | witness_conflicts : forall x y,
      conflicts x y -> Lt Event exe x y -> WitnessRelation exe xr yr xyrace x y
  | witness_critical1 : forall a r,
      is_acq a -> is_rel r -> Lt Event exe r a -> WitnessRelation exe xr yr xyrace r a
  | witness_critical2 : forall a r,
      OrphanedAcquire exe xr yr xyrace a -> is_rel r -> Lt Event exe a r ->
      WitnessRelation exe xr yr xyrace r a.

  Inductive WitnessOrder exe xr yr xyrace : Event -> Event -> Prop :=
  | witness_order_refl : forall x,
      CausalEvent exe xr yr xyrace x -> WitnessOrder exe xr yr xyrace x x
  | witness_order_trans : forall x y z,
      CausalEvent exe xr yr xyrace x ->
      CausalEvent exe xr yr xyrace y ->
      CausalEvent exe xr yr xyrace z ->
      WitnessRelation exe xr yr xyrace x y ->
      WitnessOrder exe xr yr xyrace y z ->
      WitnessOrder exe xr yr xyrace x z.
  
  Definition PrefixLinearization (R : Event -> Event -> Prop) (t : list Event) :=
    forall x y, In y t -> In x t -> R x y -> Lt Event t x y.
  
  Definition ThreadPrefix exe t :=
    forall y, In y t ->
              (forall x, ProgramOrder exe x y -> ProgramOrder t x y).

  Definition SameObservations exe t :=
    forall r, In r t ->
              (forall w, ObservesWrite r w exe -> ObservesWrite r w t).

  Inductive ValidReordering exe (exetrace : Trace exe) : list Event -> Prop :=
  | feasible_execution_def : forall t,
      Trace t ->
      ThreadPrefix exe t ->
      SameObservations exe t ->
      ValidReordering exe exetrace t.
  
  Inductive Enabled exe (exetrace : Trace exe) : list Event -> Event -> Prop :=
  | enabled_def : forall t y,
      ThreadPrefix exe t ->
      ~ In y t ->
      (forall x, ProgramOrder exe x y -> In x t) ->
      Enabled exe exetrace t y.

  Inductive WitnessTrace exe (exetrace : Trace exe) xr yr (xyrace : FirstRace C exe xr yr) :
    list Event -> Prop :=
  | witness_trace_def : forall t,
      ValidReordering exe exetrace t ->
      Enabled exe exetrace t xr ->
      Enabled exe exetrace t yr ->
      WitnessTrace exe exetrace xr yr xyrace (yr :: xr :: t).
  
End Witness.

Section DC.

  Inductive DCRelation (l : list Event) : relation Event :=
  | dc_program_rule : forall x y,
      ProgramOrder l x y -> DCRelation l x y
  | dc_critical_rule1 : forall x ax rx y ay,
      ClosedIsolatedEvent ax x rx l ->
      OpenIsolatedEvent ay y l ->
      conflicts x y ->
      Lt Event l rx ay ->
      DCRelation l rx y
  | dc_critical_rule2 : forall x ax rx y ay ry,
      ClosedIsolatedEvent ax x rx l ->
      ClosedIsolatedEvent ay y ry l ->
      conflicts x y ->
      Lt Event l rx ay ->
      DCRelation l rx y.

  Definition DCOrder (l : list Event) : relation Event :=
    clos_refl_trans_1n Event (DCRelation l).

End DC.

Section WDC.

  Inductive WDCRelation (l : list Event) : relation Event :=
  | wdc_program_rule : forall x y,
      ProgramOrder l x y -> WDCRelation l x y
  | wdc_critical_rule1 : forall x ax rx y ay,
      ClosedIsolatedEvent ax x rx l ->
      OpenIsolatedEvent ay y l ->
      conflicts x y ->
      is_read x \/ is_read y ->
      Lt Event l rx ay ->
      WDCRelation l rx y
  | wdc_critical_rule2 : forall x ax rx y ay ry,
      ClosedIsolatedEvent ax x rx l ->
      ClosedIsolatedEvent ay y ry l ->
      conflicts x y ->
      is_read x \/ is_read y ->
      Lt Event l rx ay ->
      WDCRelation l rx y.

  Definition WDCOrder (l : list Event) : relation Event :=
    clos_refl_trans_1n Event (WDCRelation l).

End WDC.

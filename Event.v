
Require Import Definitions.
Require Import Arith.

Ltac eval_event :=
  cbv delta ; repeat (cbv beta ; cbv iota) ; auto.

Ltac simpls := unfold conflicts; unfold is_same_var; simpl in *.

Section Event.

  Ltac t e := 
    destruct e; eval_event; intros; intuition; congruence.

  Lemma absurd_access_critical : forall e,
    is_access e -> ~ is_critical e.
  Proof. t e. Qed.

  Lemma absurd_access_rel : forall e,
    is_access e -> ~ is_rel e.
  Proof. t e. Qed.
    
  Lemma absurd_access_acq : forall e,
    is_access e -> ~ is_acq e.
  Proof. t e. Qed.

  Lemma absurd_critical_access : forall e,
    is_critical e -> ~ is_access e.
  Proof. t e. Qed.
  
  Lemma absurd_critical_write : forall e,
    is_critical e -> ~ is_write e.
  Proof. t e. Qed.
  
  Lemma absurd_critical_read : forall e,
    is_critical e -> ~ is_read e.
  Proof. t e. Qed.

  Lemma absurd_read_write : forall e,
    is_read e -> ~ is_write e.
  Proof. t e. Qed.

  Lemma absurd_acq_write : forall e,
    is_acq e -> ~ is_write e.
  Proof. t e. Qed.
    
  Lemma absurd_rel_write : forall e,
    is_rel e -> ~ is_write e.
  Proof. t e. Qed.

  Lemma absurd_write_read : forall e,
    is_write e -> ~ is_read e.
  Proof. t e. Qed.
  
  Lemma absurd_rel_read : forall e,
    is_rel e -> ~ is_read e.
  Proof. t e. Qed.

  Lemma absurd_acq_read : forall e,
    is_acq e -> ~ is_read e.
  Proof. t e. Qed.
  
  Lemma absurd_write_critical : forall e,
    is_write e -> ~ is_critical e.
  Proof. t e. Qed.

  Lemma absurd_read_critical : forall e,
    is_read e -> ~ is_critical e.
  Proof. t e. Qed.

  Lemma absurd_acq_access : forall e,
    is_acq e -> ~ is_access e.
  Proof. t e. Qed.

  Lemma absurd_rel_access : forall e,
    is_rel e -> ~ is_access e.
  Proof. t e. Qed.

  Lemma absurd_rel_acq : forall e,
    is_rel e -> ~ is_acq e.
  Proof. t e. Qed.

  Lemma absurd_write_acq : forall e,
    is_write e -> ~ is_acq e.
  Proof. t e. Qed.

  Lemma absurd_read_acq : forall e,
    is_read e -> ~ is_acq e.
  Proof. t e. Qed.

  Lemma absurd_acq_rel : forall e,
    is_acq e -> ~ is_rel e.
  Proof. t e. Qed.

  Lemma absurd_read_rel : forall e,
    is_read e -> ~ is_rel e.
  Proof. t e. Qed.

  Lemma absurd_write_rel : forall e,
    is_write e -> ~ is_rel e.
  Proof. t e. Qed.

  Lemma conflicts_symm : forall x y,
      conflicts x y -> conflicts y x.
  Proof.
    unfold conflicts; intros; intuition; congruence.
  Qed.

  Lemma conflicts_access : forall x y,
      conflicts x y -> is_access x /\ is_access y.
  Proof.
    unfold conflicts; unfold is_same_var; intros.
    destruct x, y; eval_event; intuition; simpl in *; congruence.
  Qed.
  
  Lemma conflicts_trans : forall x y z,
      conflicts x y ->
      conflicts y z ->
      x <> z ->
      is_write x \/ is_write z ->
      conflicts x z.
  Proof.
    simpls; intuition; try congruence.
  Qed.

  Lemma conflicts_dec : forall a b,
      conflicts a b \/ ~ conflicts a b.
  Proof.
    intros.
    destruct (Event_eq_dec a b); subst.
    - destruct b; eval_event; intuition.
    - destruct a eqn:Ha, b eqn:Hb; simpls; intuition;
        try ((left; intuition; congruence) + (right; intuition; congruence)).
      destruct (Nat.eq_dec n2 n5);
        try ((left; intuition; congruence) + (right; intuition; congruence)).
      destruct (Nat.eq_dec n2 n5);
        try ((left; intuition; congruence) + (right; intuition; congruence)).
      destruct (Nat.eq_dec n2 n5);
        try ((left; intuition; congruence) + (right; intuition; congruence)).
  Qed.

  Lemma is_access_or_is_critical : forall e,
      is_access e \/ is_critical e.
  Proof.
    intros.
    destruct e; eval_event.
  Qed.

  Lemma is_critical_dec : forall e,
      is_critical e \/ ~ is_critical e.
  Proof.
    intros.
    destruct e; eval_event; intuition.
  Qed.

  Lemma is_access_dec : forall e,
      is_access e \/ ~ is_access e.
  Proof.
    intros.
    destruct e; eval_event; intuition.
  Qed.

  Lemma is_rel_dec : forall e,
      is_rel e \/ ~ is_rel e.
  Proof.
    intros.
    destruct e; eval_event; intuition.
  Qed.

  Lemma is_read_dec : forall e,
      is_read e \/ ~ is_read e.
  Proof.
    intros.
    destruct e; eval_event; intuition.
  Qed.

  Lemma is_write_dec : forall e,
      is_write e \/ ~ is_write e.
  Proof.
    intros.
    destruct e; eval_event; intuition.
  Qed.

  Lemma is_acq_dec : forall e,
      is_acq e \/ ~ is_acq e.
  Proof.
    intros.
    destruct e; eval_event; intuition.
  Qed.

End Event.

Hint Resolve conflicts_dec.
Hint Resolve conflicts_symm.
Hint Resolve absurd_access_critical.
Hint Resolve absurd_access_acq.
Hint Resolve absurd_access_rel.
Hint Resolve absurd_critical_access.
Hint Resolve absurd_critical_read.
Hint Resolve absurd_critical_write.
Hint Resolve absurd_read_write.
Hint Resolve absurd_read_rel.
Hint Resolve absurd_read_critical.
Hint Resolve absurd_write_read.
Hint Resolve absurd_write_rel.
Hint Resolve absurd_write_critical.
Hint Resolve absurd_rel_read.
Hint Resolve absurd_rel_write.
Hint Resolve absurd_rel_access.
Hint Resolve absurd_rel_acq.
Hint Resolve absurd_acq_read.
Hint Resolve absurd_acq_write.
Hint Resolve absurd_acq_access.
Hint Resolve absurd_acq_rel.
Hint Resolve Event_eq_dec.
Hint Resolve conflicts_dec.

Ltac event_types :=
  match goal with
  | [ H: is_read ?x |- _ ] =>
    match goal with
    | [H': is_rel x |- _ ] => contradict H; auto using absurd_read_rel
    | [H': is_critical x |- _ ] => contradict H; auto using absurd_read_critical
    | [H': is_acq x |- _ ] => contradict H; auto using absurd_read_acq
    | [H': is_write x |- _ ] => contradict H; auto using absurd_read_write
    end
  | [ H: is_write ?x |- _ ] =>
    match goal with
    | [H': is_rel x |- _ ] => contradict H; auto using absurd_write_rel
    | [H': is_critical x |- _ ] => contradict H; auto using absurd_write_critical
    | [H': is_acq x |- _ ] => contradict H; auto using absurd_write_acq
    end
  | [ H: is_rel ?x |- _ ] =>
    match goal with
    | [H': is_access x |- _ ] => contradict H; auto using absurd_rel_access
    | [H': is_acq x |- _ ] => contradict H; auto using absurd_rel_acq
    end
  | [ H: is_acq ?x |- _ ] =>
    match goal with
    | [H': is_access x |- _ ] => contradict H; auto using absurd_acq_access
    end
  | [ H: is_access ?x |- _ ] =>
    match goal with
    | [H': is_critical x |- _ ] => contradict H; auto using absurd_access_critical
    end
  | [ H: ~ is_read (Rd _ _ _) |- _ ] => contradict H; simpl; auto
  | [ H: ~ is_write (Wr _ _ _) |- _ ] => contradict H; simpl; auto
  | [ H: ~ is_rel (Rel _ _) |- _ ] => contradict H; simpl; auto
  | [ H: ~ is_acq (Acq _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_read (Wr _ _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_read (Rel _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_read (Acq _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_write (Rd _ _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_write (Rel _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_write (Acq _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_rel (Wr _ _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_rel (Rd _ _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_rel (Acq _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_acq (Wr _ _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_acq (Rd _ _ _) |- _ ] => contradict H; simpl; auto
  | [ H: is_acq (Rel _ _) |- _ ] => contradict H; simpl; auto
  | [ H: _ |- is_access (Wr _ _ _) ] => unfold is_access; simpl; auto
  | [ H: _ |- is_access (Rd _ _ _) ] => unfold is_access; simpl; auto
  | [ H: _ |- is_read (Rd _ _ _) ] => simpl; auto
  | [ H: _ |- is_write (Wr _ _ _) ] => simpl; auto
  | [ H: _ |- is_rel (Rel _ _ _) ] => simpl; auto
  | [ H: _ |- is_acq (Acq _ _ _) ] => simpl; auto
  end.


Require Import Definitions.
Require Import List Omega.

Section ListOrder.

  Ltac imm_ind_nil :=
    match goal with
    | [ H: IndexOf _ _ _ nil |- _ ] => inversion H
    | [ H: ?n < 0 |- _ ] => inversion H
    | [ H: In ?x nil |- _ ] => inversion H
    | [ H: MapsToP _ _ _ _ nil |- _ ] => inversion H
    end.
  
  Hint Resolve in_eq.
  Hint Resolve in_cons.
  Hint Constructors IndexOf.
  Hint Constructors MapsToP.
  Hint Constructors NoDup.

  Variable A : Type.
  
  Lemma index_of_to_in:
    forall x n l,
    IndexOf A x n l ->
    In x l.
  Proof.
    induction l; intros; try imm_ind_nil.
    inversion H; auto.
  Qed.

  Hint Resolve index_of_to_in.
  Lemma index_of_fun:
    forall l x n n',
    NoDup l ->
    IndexOf A x n l ->
    IndexOf A x n' l ->
    n' = n.
  Proof.
    induction l; intros; inversion H; try imm_ind_nil.
    inversion H0; subst.
    inversion H1; subst; auto.
    contradict H4; eauto.
    eapply IHl; eauto.
    inversion H1; subst; auto.
    contradict H4; eauto.
  Qed.

  Lemma index_of_lt:
    forall x n l,
    IndexOf A x n l ->
    n < length l.
  Proof.
    induction l; intros; try imm_ind_nil.
    inversion H; subst; auto.
    assert (n < length l) by eauto.
    simpl; auto.
  Qed.

  Hint Resolve index_of_lt.
  Lemma in_to_index_of:
    forall l x,
    In x l ->
    exists n, n < length l /\ IndexOf A x n l.
  Proof.
    induction l; intros; try imm_ind_nil.
    destruct H; subst.
    - exists (length l); eauto.
    - apply IHl in H.
      destruct H as (n, (?,?)).
      exists n; eauto.
  Qed.

  Hint Resolve in_to_index_of.
  Lemma index_of_bij:
    forall l x x' n,
    NoDup l ->
    IndexOf A x n l ->
    IndexOf A x' n l ->
    x' = x.
  Proof.
    intros.
    induction l; inversion H; subst; try imm_ind_nil.
    inversion H; clear H; subst.
    inversion H0; subst; clear H0.
    - inversion H1; subst; clear H1; auto.
      apply index_of_lt in H2.
      omega.
    - inversion H1; subst; clear H1.
      apply index_of_lt in H3.
      omega.
      eauto.
  Qed.

  Lemma index_of_neq:
    forall l x y n n',
    NoDup l ->
    IndexOf A x n l ->
    IndexOf A y n' l ->
    n <> n' ->
    x <> y.
  Proof.
    induction l; intros; try imm_ind_nil.
    inversion H; clear H; subst.
    inversion H0; subst; clear H0.
    - inversion H1; subst; clear H1.
      + omega.
      + intuition; subst.
        contradiction H5; eauto.
    - inversion H1; subst; clear H1.
      + intuition; subst.
        contradiction H5; eauto.
      + eauto.
  Qed.

  Lemma lt_trans (l:list A) (N:NoDup l):
    forall x y z,
    Lt A l x y ->
    Lt A l y z ->
    Lt A l x z.
  Proof.
    intros.
    inversion H; clear H.
    inversion H0; clear H0.
    rename yn0 into zn.
    assert (xn0 = yn) by
    eauto using index_of_fun; subst.
    apply lt_def with (xn:=xn) (yn:=zn); auto.
    omega.
  Qed.

  Lemma lt_irrefl (l:list A) (N:NoDup l):
    forall x,
    ~ Lt A l x x.
  Proof.
    intros.
    intuition.
    inversion H.
    assert (xn=yn) by eauto using index_of_fun.
    intuition.
  Qed.

  Lemma lt_neq (l:list A) (N:NoDup l):
    forall x y,
    Lt A l x y ->
    x <> y.
  Proof.
    intros.
    inversion H; clear H.
    assert (xn <> yn) by omega.
    eauto using index_of_neq.
  Qed.

  Lemma lt_antisymm (l: list A) (N:NoDup l): forall x y,
      Lt A l x y -> Lt A l y x -> y = x.
  Proof.
    intros.
    inversion H ; subst.
    inversion H0 ; subst.
    assert (xn = yn0) by eauto using index_of_fun.
    assert (yn = xn0) by eauto using index_of_fun.
    omega.
  Qed.

  Lemma index_of_tl : forall l x n y,
      x <> y -> IndexOf A x n (y :: l) -> IndexOf A x n l.
  Proof.
    intros l x n y Hxy Hix.
    inversion Hix ; subst.
    - contradiction.
    - auto.
  Qed.
  
  Lemma lt_tl (l: list A) : forall x y z,
      y <> z -> Lt A (z :: l) x y -> Lt A l x y.
  Proof.
    intros.
    inversion H0 ; subst.
    inversion H1 ; subst.
    - apply index_of_lt in H2 ; simpl in H2; omega.
    - apply lt_def with (xn:=xn) (yn:=yn); auto.
      inversion H2 ; subst.
      + contradiction.
      + auto.
  Qed.
  
  Lemma lt_absurd_nil:
    forall x y,
    ~ Lt A nil x y.
  Proof.
    intuition.
    destruct H.
    inversion H.
  Qed.

  Lemma lt_cons:
    forall z l x y,
    Lt A l x y ->
    Lt A (z :: l) x y.
  Proof.
    intros.
    inversion H.
    eauto using lt_def, index_of_cons.
  Qed.

  Lemma absurd_lt_hd (l : list A) : forall x y,
      NoDup (x :: l) -> ~ Lt A (x :: l) x y.
  Proof.
    intros; intuition.
    inversion H0; subst.
    inversion H1; inversion H2; subst; intuition.
    - apply index_of_lt in H8; intuition.
    - assert (Hxn: xn = length l) by eauto using index_of_fun.
      intuition.
    - inversion H; subst.
      contradict H7; eauto using index_of_to_in.
  Qed.

  Lemma lt_hd (l : list A) : forall x y,
      In x l -> Lt A (y::l) x y.
  Proof.
    intros.
    apply in_to_index_of in H.
    destruct H as (n, Hn); intuition.
    eapply lt_def with (xn:=n); eauto using index_of_eq, index_of_cons.
  Qed.

  Lemma lt_total_order (l : list A) (N : NoDup l) : forall x y,
      x <> y -> In x l -> In y l -> Lt A l x y \/ Lt A l y x.
  Proof.
    intros.
    apply in_to_index_of in H0; apply in_to_index_of in H1.
    destruct H0 as (xn, Hxn); destruct H1 as (yn, Hyn); intuition.
    case (Nat.eq_dec xn yn); intros.
    - contradict H; subst; eauto using index_of_bij.
    - apply nat_total_order in n; destruct n.
      + left; eauto using lt_def.
      + right; eauto using lt_def.
  Qed.
      
  Lemma lt_in (l : list A) : forall x y,
      Lt A l x y -> In x l /\ In y l.
  Proof.
    intros x y Hxy; split; induction Hxy; eauto.
  Qed.

  Lemma maps_top_inv_p : forall (P : A -> Prop) (x : A) n vs,
      MapsToP A P x n vs -> P x.
  Proof.
    intros.
    induction H ; auto.
  Qed.
  
  Lemma maps_top_inv_eq : forall (P : A -> Prop) (x : A) n vs,
      MapsToP A P x n (x :: vs) -> n = length vs.
  Proof.
    intros.
    inversion H ; subst.
    - reflexivity.
    - inversion H5 ; subst.
      + contradiction.
      + apply maps_top_inv_p in H1 ; contradiction.
  Qed.
      
  Lemma maps_top_notp : forall (P : A -> Prop) (x:A) y vs n,
      ~ P x -> MapsToP A P y n (x :: vs) -> MapsToP A P y n vs.
  Proof.
    intros.
    inversion H0 ; subst.
    - contradiction.
    - assumption.
  Qed.

  Lemma maps_top_fun_2 : forall vs (P : A -> Prop) (x : A) n n',
      MapsToP A P x n vs -> MapsToP A P x n' vs -> n' = n.
  Proof.
    induction vs ; intros; try imm_ind_nil.
    inversion H; subst; try contradiction.
    inversion H0; subst; auto; try contradiction.
    apply IHvs with (P) (x) ; auto.
    apply maps_top_notp with (a) ; auto.
  Qed.
      
  Lemma maps_top_to_index_of : forall (P : A -> Prop) (x : A) nx vs,
      MapsToP A P x nx vs -> IndexOf A x nx vs.
  Proof.
    intros.
    induction H. {
      auto using index_of_eq.
    }
    auto using index_of_cons.
  Qed.
  
  Lemma maps_top_lt:
    forall (P:A->Prop) (x:A) n vs,
    MapsToP A P x n vs ->
    n < length vs.
  Proof.
    induction vs; intros; try imm_ind_nil.
    inversion H; subst; auto.
    apply IHvs in H5.
    simpl; auto.
  Qed.

  Lemma maps_top_absurd_length:
    forall (P:A->Prop) (x:A) vs,
    ~ MapsToP A P x (length vs) vs.
  Proof.
    intros.
    unfold not; intros.
    apply maps_top_lt in H.
    apply Lt.lt_irrefl in H.
    assumption.
  Qed.

  Lemma maps_top_nimpl : forall l (Q P : A -> Prop) x xn,
      MapsToP A Q x xn l ->
      P x ->
      (forall a : A, ~ Q a -> ~ P a) ->
      MapsToP A P x xn l.
  Proof.
    induction l; intros.
    inversion H.
    inversion H; subst.
    apply maps_top_eq; auto.
    apply maps_top_cons; auto.
    apply IHl with Q; auto.
  Qed.
    
  Lemma index_of_absurd_length:
    forall (x:A) vs,
    ~ IndexOf A x (length vs) vs.
  Proof.
    intuition.
    apply index_of_lt in H.
    omega.
  Qed.

  Lemma maps_top_absurd_cons:
    forall (P:A->Prop) (x:A) y n vs,
    MapsToP A P x n vs ->
    ~ (MapsToP A P y n (y :: vs)).
  Proof.
    intros.
    unfold not; intros.
    assert (n = length vs) by eauto using maps_top_inv_eq; subst.
    apply maps_top_absurd_length in H.
    contradiction.
  Qed.

  Lemma maps_top_inv_key:
    forall (P:A->Prop) (x:A) y l,
    MapsToP A P y (length l) (x :: l) ->
    y = x.
  Proof.
    intros.
    inversion H; subst. {
      trivial.
    }
    apply maps_top_absurd_length in H5; contradiction.
  Qed.

  Lemma index_of_inv_key:
    forall (x:A) y l,
    IndexOf A y (length l) (x :: l) ->
    y = x.
  Proof.
    intros.
    inversion H; subst. {
      trivial.
    }
    apply index_of_absurd_length in H2; contradiction.
  Qed.

  Lemma maps_top_fun_1:
    forall (P:A -> Prop) (x:A) y n vs,
    MapsToP A P x n vs ->
    MapsToP A P y n vs ->
    y = x.
  Proof.
    intros.
    induction H. {
      eauto using maps_top_inv_key.
    }
    inversion H0; subst. {
      apply maps_top_absurd_length in H1.
      contradiction.
    }
    auto.
  Qed.

  Lemma maps_top_to_in:
    forall (P:A->Prop) (x:A) n vs,
    MapsToP A P x n vs ->
    List.In x vs.
  Proof.
    intros.
    induction H. {
      auto using List.in_eq.
    }
    auto using List.in_cons.
  Qed.

  Lemma maps_top_funp1: forall (P:A->Prop) (x:A) nx ny y vs,
      MapsToP A P x nx vs -> MapsToP A P y ny vs -> nx = ny.
  Proof.
    intros.
    induction H.
    inversion H0 ; subst.
    - reflexivity.
    - contradiction.
    - eauto using IHMapsToP, maps_top_notp.
  Qed.
  
  Lemma maps_top_funp2: forall (P:A->Prop) (x:A) nx ny y vs,
      MapsToP A P x nx vs -> MapsToP A P y ny vs -> x = y.
  Proof.
    intros.
    assert (Hindices: nx = ny). {
      apply maps_top_funp1 with (P:=P) (x:=x) (y:=y) (vs:=vs) ; auto.
    }
    rewrite Hindices in H.
    symmetry.
    apply maps_top_fun_1 with (P:=P) (n:=ny) (vs:=vs) ; auto.
  Qed.

  Lemma in_to_maps_top :
    forall l P (a : A) (decP: (forall b : A, P b \/ ~ P b)),
      P a -> In a l -> (exists b n', MapsToP A P b n' l).
  Proof.
    induction l; intros.
    inversion H0.
    destruct (decP a); subst.
    exists a, (length l).
    auto using maps_top_eq.
    assert (exists b n', MapsToP A P b n' l).
    apply IHl with a0; auto.
    inversion H0.
    congruence.
    auto.
    destruct H2 as (b, Hb).
    destruct Hb as (bn, Hbn).
    exists b, bn.
    auto using maps_top_cons.
  Qed.

  Lemma maps_top_lt_p : forall l P (a : A) na b (eqA : forall c d : A, {c = d} + {c <> d}),
      MapsToP A P a na l ->
      In b l ->
      P b ->
      b = a \/ Lt A l b a.
  Proof.
    intros.
    induction H.
    destruct (eqA x b); auto.
    right.
    apply lt_hd.
    destruct H0; subst; contradiction + auto.
    destruct H0; subst; try congruence.
    apply IHMapsToP in H0.
    destruct H0; subst; auto.
    right.
    apply lt_cons; auto.
  Qed.

  Lemma maps_top_notps : forall (P : A -> Prop) l (x:A) vs,
      Forall (fun y => not (P y)) l -> P x -> MapsToP A P x (length vs) (l ++ x :: vs).
  Proof.
    induction l; simpl; intros.
    auto using maps_top_eq.
    apply maps_top_cons.
    inversion H; auto.
    apply IHl; auto.
    inversion H; auto.
  Qed.

  Lemma absurd_lt_nil : forall x y,
      ~ Lt A nil x y.
  Proof.
    intuition.
    inversion H.
    inversion H1.
  Qed.

  Lemma in_app : forall A l (a : A),
      In a l -> (exists l1 l2, l = l1 ++ a :: l2).
  Proof.
    induction l; intros.
    inversion H.
    inversion H; subst.
    exists nil, l; auto.
    destruct IHl with a0; auto.
    destruct H1.
    rewrite H1.
    exists (a :: x), x0; auto.
  Qed.
  
  Lemma lt_hd_in_tl : forall l (a b : A),
      NoDup (a :: l) -> Lt A (a :: l) b a -> In b l.
  Proof.
    intros.
    destruct (lt_in (a :: l) b a); auto.
    inversion H1; subst.
    contradict H0; auto using absurd_lt_hd.
    auto.
  Qed.

  Lemma in_app_hd : forall A l1 l2 (a : A),
      In a (l1 ++ a :: l2).
  Proof.
    induction l1; simpl; auto using in_cons, in_eq.
  Qed.
  
  Lemma lt_hd_in_tl_app : forall l1 l2 (a b : A),
      NoDup (l1 ++ a :: l2) -> Lt A (l1 ++ a :: l2) b a -> In b l2.
  Proof.
    induction l1; simpl; intros.
    eauto using lt_hd_in_tl.
    destruct (lt_in (a :: l1 ++ a0 :: l2) b a0); auto.
    inversion H1; subst.
    contradict H0; auto using absurd_lt_hd.
    apply IHl1 with a0; auto.
    inversion H; auto.
    apply lt_tl with a; auto.
    intuition; subst.
    inversion H; subst.
    contradict H6.
    apply in_app_hd.
  Qed.

  Lemma lt_hd_app : forall t1 t2 (a b : A),
      Lt A (a :: t2) b a -> Lt A (t1 ++ a :: t2) b a.
  Proof.
    induction t1; simpl; intros; auto.
    apply lt_cons.
    auto.
  Qed.

  Lemma lt_app : forall t1 t2 (a b : A),
      Lt A t2 a b -> Lt A (t1 ++ t2) a b.
  Proof.
    induction t1; simpl; intros; auto.
    apply lt_cons.
    auto.
  Qed.

  Lemma nodup_app_inv1 : forall (t1 t2 : list A),
      NoDup (t1 ++ t2) -> NoDup t2.
  Proof.
    induction t1; simpl; intros; auto.
    inversion H; auto.
  Qed.

  Lemma mapped_top_to_in : forall l (P : A -> Prop) (a b : A) n,
      MappedToP A P a b n l -> In a l /\ In b l.
  Proof.
    induction 1.
    split; eauto using in_eq, in_cons, maps_top_to_in.
    intuition; eauto using in_cons.
  Qed.
  Hint Resolve mapped_top_to_in.

  Lemma mapped_top_maps_top : forall l (P : A -> Prop) (a b : A) n,
      NoDup (a :: l) -> MappedToP A P a b n (a :: l) -> MapsToP A P b n l.
  Proof.
    intros.
    inversion H0; subst.
    auto.
    assert (In a l /\ In b l) by eauto.
    intuition; subst.
    inversion H; subst.
    contradiction.
  Qed.
  Hint Resolve mapped_top_maps_top.
  
  Lemma maps_top_decidable : forall l (P : A -> Prop) a,
      (forall b : A, P b \/ ~ P b) ->
      (forall b c : A, {b = c} + {b <> c}) ->
      NoDup l ->
      (exists n, MapsToP A P a n l) \/ ~ (exists n, MapsToP A P a n l).
  Proof.
    induction l; intros.
    right; intuition.
    destruct H1; imm_ind_nil.
    destruct (IHl P a0); auto.
    inversion H0; auto.
    destruct H1.
    destruct (H a).
    right; intuition.
    destruct H3.
    inversion H3; subst.
    inversion H0; subst.
    contradict H6; eauto using maps_top_to_in.
    contradiction.
    left. exists x; auto using maps_top_cons.

    destruct (X a a0); subst.
    destruct (H a0).
    left.
    exists (length l); auto using maps_top_eq.
    right; intuition.
    destruct H3.
    inversion H3; subst; try contradiction.
    apply H1. exists x; auto.
    destruct (H a0).
    right; intuition.
    destruct H3.
    inversion H3; subst; try contradiction.
    apply H1.
    exists x; eauto using maps_top_notp.
    right; intuition.
    inversion H3; subst.
    apply H2.
    eapply maps_top_inv_p; eauto.
  Qed.

  Lemma maps_top_exists_decidable : forall l (P : A -> Prop),
      (forall b : A, P b \/ ~ P b) ->
      (forall b c : A, {b = c} + {b <> c}) ->
      NoDup l ->
      (exists a n, MapsToP A P a n l) \/ ~ (exists a n, MapsToP A P a n l).
  Proof.
    induction l; intros.
    right; intuition. destruct H1. destruct H1. imm_ind_nil.
    assert (NoDup l) by (inversion H0; auto).
    destruct (IHl P); auto.
    destruct H2. destruct H2.
    destruct (H a).
    left. exists a, (length l); auto.
    left. exists x, x0; auto.
    destruct (H a).
    left. exists a, (length l); auto.
    right; intuition.
    apply H2.
    destruct H4. destruct H4.
    exists x, x0; eauto using maps_top_notp.
  Qed.

  Lemma lt_in1 : forall l x y,
      Lt A l x y -> In x l.
  Proof.
    intros.
    destruct (lt_in l x y); auto.
  Qed.

  Lemma lt_in2 : forall l x y,
      Lt A l x y -> In y l.
  Proof.
    intros.
    destruct (lt_in l x y); auto.
  Qed.
    
  Lemma mapped_top_app_inv : forall A (P : A -> Prop) l1 l2 a b n,
      NoDup (l1 ++ l2) ->
      MappedToP A P a b n (l1 ++ l2) ->
      In a l2 ->
      MappedToP A P a b n l2.
  Proof.
    induction l1; intros; simpl in *; auto.
    inversion H0; subst.
    inversion H; subst.
    contradict H4.
    apply in_or_app; auto.
    apply IHl1; auto.
    inversion H; auto.
  Qed.

  Lemma mapped_top_app_hd : forall A P l1 l2 a b n,
      MapsToP A P b n l2 ->
      P a ->
      MappedToP A P a b n (l1 ++ a :: l2).
  Proof.
    induction l1; intros; simpl.
    apply mapped_top_eq; auto.
    apply mapped_top_cons.
    apply IHl1; auto.
  Qed.

  Lemma lt_tl_in_tl : forall t1 t2 a b,
      NoDup (t1 ++ t2) ->
      In b t2 ->
      Lt A (t1 ++ t2) a b ->
      In a t2.
  Proof.
    induction t1; simpl;  intros.
    destruct (lt_in t2 a b); auto.
    apply (IHt1 t2 a0 b); auto.
    inversion H; auto.
    apply lt_tl with a.
    intuition; subst.
    inversion H; subst.
    contradict H4.
    apply in_or_app; auto.
    auto.
  Qed.

  Lemma mapped_top_inv_p : forall l P a b n,
      MappedToP A P a b n l -> P b.
  Proof.
    induction 1.
    eapply maps_top_inv_p; eauto.
    auto.
  Qed.
  
  Lemma absurd_nodup_mapped_top : forall l P a n,
      NoDup l -> ~ MappedToP A P a a n l.
  Proof.
    induction l; intros.
    intuition.
    inversion H0.
    intuition.
    inversion H0; subst.
    inversion H; subst.
    apply H3.
    eauto using maps_top_to_in.
    eapply IHl; eauto.
    inversion H; auto.
  Qed.
    
  Lemma mapped_top_lt : forall l P a b n,
      MappedToP A P a b n l -> Lt A l b a.
  Proof.
    induction l; intros.
    inversion H.
    inversion H; subst.
    apply lt_hd; eauto using maps_top_to_in.
    apply lt_cons.
    eapply IHl; eauto.
  Qed.

  Lemma mapped_top_lt_p_not_overlapping : forall l P a b n c,
      (forall a b: A, {a = b} + {a <> b}) ->
      NoDup l ->
      MappedToP A P a b n l ->
      P c ->
      Lt A l c a ->
      c = b \/ Lt A l c b.
  Proof.
    induction l; intros.
    inversion H0.
    inversion H0; subst.
    destruct (lt_in (a :: l) c a); auto.
    inversion H3; subst.
    contradict H2; auto using lt_irrefl.
    destruct (maps_top_lt_p l P b n c); auto.
    right.
    apply lt_cons; auto.
    destruct (IHl P a0 b n c); auto.
    inversion H; auto.
    apply lt_tl with a; auto.
    intuition; subst.
    inversion H; subst.
    apply H5.
    destruct (mapped_top_to_in l P a b n); auto.
    right.
    apply lt_cons; auto.
  Qed.
  
End ListOrder.

Hint Constructors IndexOf.
Hint Constructors MapsToP.
Hint Constructors Lt.
Hint Resolve lt_neq.
Hint Resolve lt_antisymm.
Hint Resolve lt_irrefl.
Hint Resolve maps_top_funp2.
Hint Resolve absurd_lt_hd.
Hint Resolve absurd_lt_nil.
Hint Resolve nodup_app_inv1.
Hint Resolve mapped_top_to_in.
Hint Resolve maps_top_notp.
Hint Resolve mapped_top_maps_top.
Hint Resolve lt_app.
Hint Resolve lt_hd_app.
Hint Resolve in_or_app.
Hint Resolve in_eq.
Hint Resolve in_cons.
Hint Resolve lt_hd_in_tl.
Hint Resolve lt_hd_in_tl_app.
Hint Resolve lt_in1.
Hint Resolve lt_in2.
Hint Resolve maps_top_to_index_of.
Hint Resolve maps_top_lt.
Hint Resolve lt_cons.

Ltac crush_list_order :=
  match goal with
    | [ H: IndexOf _ _ _ nil |- _ ] => inversion H
    | [ H: In ?x nil |- _ ] => inversion H
    | [ H: MapsToP _ _ _ _ nil |- _ ] => inversion H
    | [ H: Lt _ nil _ _ |- _ ] => contradict H; auto using absurd_lt_nil
    | [ H: Lt _ ?l ?x ?y |- ?x <> ?y ] => eauto using lt_neq
    | [ H: Lt _ ?l ?x ?x |- _ ] => contradict H; eauto using lt_irrefl
    | [ H: Lt _ ?l ?x ?y |- _ ] =>
      match goal with
      | [ H: Lt _ l y x |- _ ] =>
        absurd (x = y); eauto using lt_neq, lt_antisymm
      end
    | [ H : NoDup (?x :: ?l) |- _ ] =>
      match goal with
      | [ H0 : In x l |- _ ] => inversion H; contradiction
      end
    | [ H: Lt ?A ?l ?x ?y |- In ?y ?l ] => destruct (lt_in A l x y); auto
    | [ H: Lt ?A ?l ?x ?y |- In ?x ?l ] => destruct (lt_in A l x y); auto
  end.
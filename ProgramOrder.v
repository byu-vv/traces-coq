
Require Import Definitions.
Require Import Event.
Require Import ListOrder.
Require Import Trace.
Require Import List.

Section ProgramOrder.

  Hint Resolve program_trace_rule.
  Lemma program_order_antisymm (l : list Event) (N: NoDup l) : forall e1 e2,
      ProgramOrder l e1 e2 -> ProgramOrder l e2 e1 -> e1 = e2.
  Proof.
    intros.
    inversion H; inversion H0; eauto.
  Qed.
  
  Lemma program_order_irrefl (l : list Event) (N: NoDup l) : forall e,
      ~ ProgramOrder l e e.
  Proof.
    intros.
    intuition.
    inversion H; subst.
    apply lt_irrefl in H0; auto.
  Qed.
  
  Lemma program_order_trans (l : list Event) (N: NoDup l) : forall e1 e2 e3,
      ProgramOrder l e1 e2 -> ProgramOrder l e2 e3 -> ProgramOrder l e1 e3.
  Proof.
    intros.
    inversion H; inversion H0; subst; eauto.
    constructor.
    + eapply lt_trans; eauto.
    + congruence.
  Qed.

  Lemma program_order_trace_order : forall l x y,
      ProgramOrder l x y -> Lt Event l x y.
  Proof.
    intros l x y H; inversion H; auto.
  Qed.
  
  Lemma program_order_cons (l : list Event) : forall x y z,
      NoDup (z :: l) -> ProgramOrder l x y -> ProgramOrder (z :: l) x y.
  Proof.
    intros.
    inversion H0; subst; auto using lt_cons.
  Qed.

  Hint Resolve lt_tl.
  Lemma program_order_tl (l : list Event) : forall x y z,
      z <> y -> ProgramOrder (z :: l) x y -> ProgramOrder l x y.
  Proof.
    intros.
    constructor; inversion H0; subst; eauto using lt_tl.
  Qed.

  Hint Resolve lt_in.
  Hint Resolve program_order_trace_order.
  Lemma program_order_in : forall l x y,
      ProgramOrder l x y -> In x l /\ In y l.
  Proof.
    eauto.
  Qed.

  Hint Constructors Lt.
  Hint Constructors IndexOf.
  Hint Constructors MapsToP.
  Hint Resolve maps_top_to_index_of.
  Hint Resolve maps_top_lt.
  Hint Resolve nodup_tl.
  Hint Resolve program_order_cons.

  Lemma isolated_section_to_program_order (l : list Event) (N : NoDup l) :
    forall a r, IsolatedSection a r l -> ProgramOrder l a r.
  Proof.
    induction 1; eauto.
  Qed.
  
  Hint Resolve isolated_section_to_program_order.
  Hint Resolve closed_isolated_event_trace_order1.
  Hint Resolve closed_isolated_event_trace_order2.
  Hint Resolve closed_isolated_event_trace_order3.
  Hint Resolve isolated_section_thread_eq.
  Hint Resolve open_isolated_event_thread_eq.
  Lemma closed_isolated_event_release_program_order (l : list Event) (N : NoDup l) :
    forall a r x, ClosedIsolatedEvent a x r l -> ProgramOrder l x r.
  Proof.
    intros.
    apply program_trace_rule; eauto.
    induction H; eauto.
    apply open_isolated_event_thread_eq in H0.
    apply isolated_section_thread_eq in H.
    congruence.
  Qed.
  Hint Resolve closed_isolated_event_release_program_order.

  Lemma closed_isolated_event_acquire_program_order (l : list Event) (N : NoDup l) :
    forall a r x, ClosedIsolatedEvent a x r l -> ProgramOrder l a x.
  Proof.
    intros.
    apply program_trace_rule; eauto.
    induction H; eauto.
  Qed.
  Hint Resolve closed_isolated_event_acquire_program_order.
  
  Hint Resolve open_isolated_events_thread_eq.
  Lemma open_isolated_events_program_order (l : list Event) (N : NoDup l) :
    forall a1 a2 x y,
      OpenIsolatedEvent a1 x l -> OpenIsolatedEvent a2 y l -> Lt Event l x y ->
      ProgramOrder l x y.
  Proof.
    intros.
    apply program_trace_rule; eauto.
  Qed.
  Hint Resolve open_isolated_events_program_order.
  Hint Resolve open_isolated_event_trace_order.

  Lemma open_isolated_event_program_order: forall l a x,
      OpenIsolatedEvent a x l -> ProgramOrder l a x.
  Proof.
    intros; eauto.
  Qed.
  Hint Resolve open_isolated_event_program_order.

  Hint Resolve closed_isolated_events_thread_eq.
  Lemma closed_isolated_events_program_order l (N: NoDup l) : forall a r x y,
      ClosedIsolatedEvent a x r l -> ClosedIsolatedEvent a y r l ->
      Lt Event l x y -> ProgramOrder l x y.
  Proof.
    intros.
    apply program_trace_rule; eauto.
  Qed.

  Lemma program_order_thread_eq : forall l x y,
      ProgramOrder l x y -> tid x = tid y.
  Proof.
    inversion 1; subst; auto.
  Qed.
  
End ProgramOrder.

Hint Resolve program_order_trace_order.
Hint Resolve program_order_thread_eq.
Hint Resolve isolated_section_to_program_order.
Hint Resolve open_isolated_event_program_order.
Hint Resolve program_trace_rule.
# traces-coq

A formalization of partial orders over traces of events from the execution of task parallel programs.

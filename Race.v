
Require Import Definitions.
Require Import Event.
Require Import ListOrder.
Require Import Trace.
Require Import ProgramOrder.
Require Import List Decidable.

Section Race.

  Variable C : list Event -> Event -> Event -> Prop.
  Variable causality_trace_order : forall t x y,
      NoDup t -> C t x y -> x = y \/ Lt Event t x y.
  Variable program_order_causality : forall t x y,
      ProgramOrder t x y -> C t x y.
  Variable causality_trans : forall t x y z,
      C t x y ->
      C t y z ->
      C t x z.

  Lemma race_trace : forall exe xr yr,
      FirstRace C exe xr yr -> Trace exe.
  Proof.
    inversion 1; subst.
    inversion H0; subst.
    auto.
  Qed.
        
  Hint Resolve isolated_event_dec.
  Lemma race_one_not_isolated_event : forall t x y,
      NoDup t -> Race C t x y -> ~ IsolatedEvent x t \/ ~ IsolatedEvent y t.
  Proof.
    intros.
    apply not_and.
    unfold decidable; auto. 
    intuition.
    inversion H0; subst.
    intuition.
  Qed.
  
  Lemma race_kind : forall t x y,
      NoDup t -> Race C t x y ->
      IsolatedEvent x t \/ IsolatedEvent y t \/
      (~ IsolatedEvent x t /\ ~ IsolatedEvent y t).
  Proof.
    intros.
    destruct (race_one_not_isolated_event t x y); auto.
    destruct (isolated_event_dec t y); auto.
    destruct (isolated_event_dec t x); auto.
  Qed.

  Lemma first_race_all_before_yr exe xr yr (xyrace : FirstRace C exe xr yr) : forall x,
      In x exe -> x = yr \/ Lt Event exe x yr.
  Proof.
    inversion xyrace; subst.
    inversion H; subst.
    intros.
    inversion H6; subst; auto using lt_hd.
  Qed.
  
  Lemma first_race_access_y : forall t x y,
      FirstRace C t x y -> is_access y.
  Proof.
    inversion 1; subst.
    inversion H0; subst.
    destruct (conflicts_access x y); auto.
  Qed.

  Lemma first_race_access_x : forall t x y,
      FirstRace C t x y -> is_access x.
  Proof.
    inversion 1; subst.
    inversion H0; subst.
    destruct (conflicts_access x y); auto.
  Qed.
  Hint Resolve first_race_access_x.
  Hint Resolve first_race_access_y.
  
  Lemma first_race_yr_not_causal_event exe xr yr xyrace :
    ~ CausalEvent C exe xr yr xyrace yr.
  Proof.
    intuition.
    inversion xyrace; subst.
    inversion H0; subst.
    inversion H; subst.
    apply causality_trace_order in H9; auto; intuition.
    absurd (xr = yr); eauto using lt_neq, lt_antisymm.
    absurd (xr = yr); eauto using lt_neq, lt_antisymm.
    contradiction.
  Qed.

  Lemma first_race_xr_not_causal_event exe xr yr xyrace :
    ~ CausalEvent C exe xr yr xyrace xr.
  Proof.
    intuition.
    inversion xyrace; subst.
    inversion H0; subst.
    inversion H; subst.
    contradiction.
    contradiction.
  Qed.

  Lemma first_race_in : forall t x y,
      FirstRace C t x y -> In x t /\ In y t.
  Proof.
    inversion 1; subst.
    inversion H0; subst.
    eauto using lt_in.
  Qed.

  Lemma first_race_access : forall t x y,
      FirstRace C t x y -> is_access x /\ is_access y.
  Proof.
    inversion 1; subst.
    inversion H0; subst.
    eauto using conflicts_access.
  Qed.

  Lemma first_race_isolated_event_y_open_isolated_event : forall t x y,
      FirstRace C t x y -> IsolatedEvent y t ->
      (exists a, OpenIsolatedEvent a y t).
  Proof.
    intros.
    inversion H; subst.
    inversion H0; subst.
    eauto.
    absurd (y = r).
    intuition; subst.
    absurd (is_access r).
    apply absurd_rel_access; eauto using closed_isolated_event_rel.
    eauto using closed_isolated_event_access.
    apply lt_antisymm with (y :: t0).
    inversion H1; subst; eauto.
    destruct (first_race_all_before_yr (y :: t0) x y H r); auto.
    eauto using closed_isolated_event_rel_in.
    subst; absurd (is_access y).
    apply absurd_rel_access; eauto using closed_isolated_event_rel.
    eauto using closed_isolated_event_access.
    eapply closed_isolated_event_trace_order3; eauto.
    inversion H1; subst; eauto.
  Qed.

  Hint Resolve race_trace.
  Hint Resolve trace_nodup.
  Hint Resolve causal_event_x.
  Hint Resolve causal_event_y.
  Hint Resolve program_trace_rule.
  Hint Resolve closed_isolated_event_acquire_program_order.
  Hint Resolve closed_isolated_event_isolated_section.
  Lemma first_race_isolated_event_x_open_isolated_event_or_orhpaned_acquire
    t x y (xyrace : FirstRace C t x y) :
    IsolatedEvent x t -> (exists a, OrphanedAcquire C t x y xyrace a /\
                                    ((exists r, ClosedIsolatedEvent a x r t) \/
                                     OpenIsolatedEvent a x t)).
  Proof.
    intros.
    inversion H; subst.
    assert (OpenAcquire a t) by eauto using open_isolated_event_open_acquire.
    assert (exists n, MapsToP Event is_critical a n t).
    eapply open_isolated_event_acq_maps_top; eauto.
    destruct H2.
    exists a.
    split.
    apply orphaned_acquire_open; auto.
    apply causal_event_x; eauto.
    intuition; subst.
    exists a.
    split.
    apply orphaned_acquire_isolated_section with r.
    eauto using closed_isolated_event_isolated_section.
    apply causal_event_x; eauto.
    intuition.
    inversion H1; subst.
    destruct (causality_trace_order t r x); auto; subst.
    eauto.
    contradict H3.
    apply lt_antisymm with t; eauto.
    inversion xyrace; subst.
    inversion H5; subst.
    apply H9.
    apply causality_trans with r.
    apply program_order_causality.
    eauto using closed_isolated_event_release_program_order.
    auto.
    eauto.
  Qed.

End Race.

Hint Resolve race_trace.
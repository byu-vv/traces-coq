
Require Import Definitions.
Require Import Event.
Require Import ListOrder.
Require Import Trace.
Require Import ProgramOrder.
Require Import DC.
Require Import WDC.
Require Import Race.
Require Import CausalEvent.
Require Import WitnessTrace.
Require Import List Relations Relation_Operators Operators_Properties.

Section SoundnessTheorem.

  Hint Resolve dc_order_transitive.
  Hint Constructors DCRelation.
  Hint Resolve dc_order_thread_neq_rel.
  Hint Resolve dc_order_trace_order.
  Hint Resolve dc_order_cons.
  Hint Resolve dc_order_cons_inv.
  Hint Resolve wdc_order_transitive.
  Hint Constructors WDCRelation.
  Hint Resolve clos_rt1n_step.
  Hint Resolve wdc_order_thread_neq_rel.
  Hint Resolve wdc_order_trace_order.
  Hint Resolve wdc_order_cons.
  Hint Resolve wdc_order_cons_inv.
  Hint Resolve linearization_witness_relation_order.
  Hint Resolve in_causal_event.
  Hint Resolve causal_event_in.
  Hint Resolve causal_event_in_exe.

  (** Proof that the first race detected by the DC partial order is always feasible.

      We show that the race is feasible by showing that there always exists a witness
      trace for the race.

      We do this constructively by showing that any reordering of the observed execution
      that contains the causal events of the race and is a linearization of the
      WitnessOrder relation, must always be a witness trace when the racy events are
      appended to it.

      Such a linearization is always possible because the WinessOrder is antisymmetric
      when it strengthens the DC partial order. **)

  Theorem dc_witness_trace exe exetrace xr yr xyrace : forall t,
    NoDup t ->
    PrefixLinearization (WitnessOrder DCOrder exe xr yr xyrace) t ->
    CausalEvents DCOrder exe xr yr xyrace t ->
    WitnessTrace DCOrder exe exetrace xr yr xyrace (yr :: xr :: t).
  Proof.
    constructor.

    (** t is a feasible execution **)
    constructor.
    constructor; auto.

    (** t preserves lock semantics **)
    eapply witness_linearization_causal_events_lock_semantics with (C:=DCOrder); eauto.
    unfold DCOrder; eauto.

    (** each thread in t is a prefix of the same thread in the observed execution **)
    eapply witness_linearization_thread_prefix with (C:=DCOrder); eauto.
    unfold DCOrder; eauto.

    (** each read event in t observes the same write event as in the observed execution **)
    eapply witness_linearization_causal_events_same_observations with (C:=DCOrder); eauto.

    (** the first racy event is enabled at the end of t **)
    eapply first_race_xr_enabled with (C:=DCOrder); eauto.
    unfold DCOrder; eauto.

    (** the second racy event is enabled at the end of t **)
    eapply first_race_yr_enabled with (C:=DCOrder); eauto.
    unfold DCOrder; eauto.
  Qed.

  (** Same proof for WDC. Note that this only goes through because we defined
      Race to exclude intended data races inside critical sections. **)
  Theorem wdc_witness_trace exe exetrace xr yr xyrace : forall t,
    NoDup t ->
    PrefixLinearization (WitnessOrder WDCOrder exe xr yr xyrace) t ->
    CausalEvents WDCOrder exe xr yr xyrace t ->
    WitnessTrace WDCOrder exe exetrace xr yr xyrace (yr :: xr :: t).
  Proof.
    constructor.

    (** t is a feasible execution **)
    constructor.
    constructor; auto.

    (** t preserves lock semantics **)
    eapply witness_linearization_causal_events_lock_semantics with (C:=WDCOrder); eauto.
    unfold WDCOrder; eauto.

    (** each thread in t is a prefix of the same thread in the observed execution **)
    eapply witness_linearization_thread_prefix with (C:=WDCOrder); eauto.
    unfold WDCOrder; eauto.

    (** each read event in t observes the same write event as in the observed execution **)
    eapply witness_linearization_causal_events_same_observations with (C:=WDCOrder); eauto.

    (** the first racy event is enabled at the end of t **)
    eapply first_race_xr_enabled with (C:=WDCOrder); eauto.
    unfold WDCOrder; eauto.

    (** the second racy event is enabled at the end of t **)
    eapply first_race_yr_enabled with (C:=WDCOrder); eauto.
    unfold WDCOrder; eauto.
  Qed.

  (** WitnessOrder is antisymmetric and therefore we can linearize it to create the
      witness trace **)
  Theorem dc_witness_order_antisymm : forall exe xr yr xyrace,
      antisymmetric Event (WitnessOrder DCOrder exe xr yr xyrace).
  Proof.
    intros.
    unfold antisymmetric; intros.
    eapply witness_order_antisymm with (C:=DCOrder); eauto.
    unfold DCOrder; eauto.
  Qed.

  Theorem dc_witness_order_trans : forall exe xr yr xyrace,
      transitive Event (WitnessOrder DCOrder exe xr yr xyrace).
  Proof.
    intros.
    unfold transitive; intros.
    eapply witness_order_transitive with (C:=DCOrder); eauto.
  Qed.

  Theorem dc_witness_order_refl : forall exe xr yr xyrace x,
      CausalEvent DCOrder exe xr yr xyrace x ->
      WitnessOrder DCOrder exe xr yr xyrace x x.
  Proof.
    intros.
    apply witness_order_refl; auto.
  Qed.

  (** Same thing for WDC **)
  Theorem wdc_witness_order_antisymm : forall exe xr yr xyrace,
      antisymmetric Event (WitnessOrder WDCOrder exe xr yr xyrace).
  Proof.
    intros.
    unfold antisymmetric; intros.
    eapply witness_order_antisymm with (C:=WDCOrder); eauto.
    unfold WDCOrder; eauto.
  Qed.

  Theorem wdc_witness_order_trans : forall exe xr yr xyrace,
      transitive Event (WitnessOrder WDCOrder exe xr yr xyrace).
  Proof.
    intros.
    unfold transitive; intros.
    eapply witness_order_transitive with (C:=WDCOrder); eauto.
  Qed.

  Theorem wdc_witness_order_refl : forall exe xr yr xyrace x,
      CausalEvent WDCOrder exe xr yr xyrace x ->
      WitnessOrder WDCOrder exe xr yr xyrace x x.
  Proof.
    intros.
    apply witness_order_refl; auto.
  Qed.

End SoundnessTheorem.



Require Import Definitions.
Require Import Event.
Require Import ListOrder.
Require Import List Arith.

Ltac trace_inv :=
  match goal with
  | [ H : Trace ?x |- NoDup ?x ] => inversion H; auto
  | [ H : Trace ?x |- LockSemantics ?x ] => inversion H; auto
  | [ H : Trace (?x :: ?l) |- Trace ?l ] => inversion H; auto
  | [ H : NoDup (?x :: ?l) |- NoDup ?l ] => inversion H; auto
  | [ H : LockSemantics (?x :: ?l) |- LockSemantics ?l ] => inversion H; auto
  end.

Hint Constructors LockSemantics.
Hint Constructors OpenAcquire.
Hint Constructors OpenIsolatedEvent.
Hint Constructors ClosedIsolatedEvent.
Hint Constructors IsolatedEvent.
Hint Constructors IsolatedSection.
Hint Constructors ObservesWrite.
Hint Constructors Trace.

Section Trace.

  Lemma lock_semantics_tl : forall e l,
    LockSemantics (e :: l) -> LockSemantics l.
  Proof.
    intros e l H; inversion H; auto.
  Qed.
  Hint Resolve lock_semantics_tl.

  Lemma nodup_tl : forall (e : Event) (l : list Event),
      NoDup (e :: l) -> NoDup l.
  Proof.
    intros e l H; inversion H; auto.
  Qed.
  Hint Resolve nodup_tl.
  
  Hint Resolve maps_top_to_in.
  Lemma trace_tl : forall (e : Event) (l : list Event),
      Trace (e :: l) -> Trace l.
  Proof.
    intros e l H; inversion H; eauto.
  Qed.
  Hint Resolve trace_tl.

  Lemma observes_write_tl : forall r w e l,
      r <> e -> ObservesWrite r w (e::l) -> ObservesWrite r w l.
  Proof.
    intros.
    inversion H0; subst; contradiction + auto.
  Qed.
  Hint Resolve observes_write_tl.

  Lemma observes_write_conflicts : forall r w l,
      ObservesWrite r w l -> conflicts r w.
  Proof.
    induction 1.
    eapply maps_top_inv_p; eauto.
    auto.
  Qed.
  Hint Resolve observes_write_conflicts.
  
  Lemma open_acquire_tl : forall x a l,
      ~ is_acq x -> OpenAcquire a (x :: l) -> OpenAcquire a l.
  Proof.
    intros.
    inversion H0; subst; auto; try event_types.
    contradiction.
  Qed.
  Hint Resolve open_acquire_tl.
  
  Lemma isolated_section_tl_open_acquire : forall r a e l,
      ~ (exists e, OpenAcquire e l) -> IsolatedSection a r (e :: l) ->
      IsolatedSection a r l.
  Proof.
    intros.
    inversion H0; subst; contradiction + auto.
    contradict H; eauto.
  Qed.
  Hint Resolve isolated_section_tl_open_acquire.

  Lemma isolated_section_tl_neq : forall r a e l,
      e <> r -> IsolatedSection a r (e :: l) -> IsolatedSection a r l.
  Proof.
    intros.
    inversion H0; subst; contradiction + auto.
  Qed.
  Hint Resolve isolated_section_tl_neq.

  Lemma isolated_section_tl_access : forall r a e l,
      is_access e -> IsolatedSection a r (e :: l) -> IsolatedSection a r l.
  Proof.
    intros.
    inversion H0; subst; contradiction + event_types + auto.
  Qed.
  Hint Resolve isolated_section_tl_access.

  Lemma open_isolated_event_tl : forall l a x y,
      x <> y -> is_access y -> OpenIsolatedEvent a x (y :: l) ->
      OpenIsolatedEvent a x l.
  Proof.
    intros.
    inversion H1; subst; contradiction + auto.
  Qed.
  Hint Resolve open_isolated_event_tl.
  
  Lemma lock_semantics_app_inv : forall l1 l2,
      LockSemantics (l1 ++ l2) -> LockSemantics l2.
  Proof.
    induction l1; simpl; intros; auto.
    apply IHl1.
    eauto using lock_semantics_tl.
  Qed.
  
  Lemma lock_semantics_app : forall l1 l2,
      LockSemantics l2 -> Forall is_access l1 -> LockSemantics (l1 ++ l2).
  Proof.
    induction l1; simpl; intros; auto.
    apply lock_semantics_other.
    inversion H0; auto.
    apply IHl1; auto.
    inversion H0; auto.
  Qed.
    
  Lemma observes_write_read_in : forall e w l,
    ObservesWrite e w l -> In e l.
  Proof.
    intros.
    induction l. {
      inversion H.
    }
    inversion H; subst; eauto.
  Qed.
  Hint Resolve observes_write_read_in.

  Lemma observes_write_write_in: forall e w l,
      ObservesWrite e w l -> In w l.
  Proof.
    induction 1; eauto.
  Qed.
  Hint Resolve observes_write_write_in.

  Lemma observes_write_read : forall r w l,
      ObservesWrite r w l -> is_read r.
  Proof.
    induction 1; auto.
  Qed.
  Hint Resolve observes_write_read.

  Lemma observes_write_write : forall r w l,
      ObservesWrite r w l -> is_write w.
  Proof.
    induction 1; auto.
    apply maps_top_inv_p in H0.
    destruct H0; intuition.
    event_types.
  Qed.
  Hint Resolve observes_write_write.
  
  Hint Resolve maps_top_funp2.
  Lemma observes_write_fun_write : forall r w w' l,
      NoDup l -> ObservesWrite r w l -> ObservesWrite r w' l -> w = w'.
  Proof.
    intros.
    induction H0; inversion H; inversion H1; subst; eauto.
    - apply observes_write_read_in in H10; contradiction.
    - apply IHObservesWrite; eauto.
  Qed.
  Hint Resolve observes_write_fun_write.
  
  Lemma isolated_section_rel : forall r a l,
      IsolatedSection a r l -> is_rel r.
  Proof.
    induction 1; auto.
  Qed.
  Hint Resolve isolated_section_rel.
  
  Lemma open_acquire_maps_top_critical_acq : forall l a,
      OpenAcquire a l ->
      is_acq a.
  Proof.
    induction 1; intros; auto.
  Qed.
  Hint Resolve open_acquire_maps_top_critical_acq.
  
  Lemma isolated_section_acq : forall r a l,
      IsolatedSection a r l -> is_acq a.
  Proof.
    induction 1; eauto.
  Qed.
  Hint Resolve isolated_section_acq.

  Lemma open_isolated_event_acq : forall x a l,
      OpenIsolatedEvent a x l -> is_acq a.
  Proof.
    induction 1; eauto.
  Qed.
  Hint Resolve open_isolated_event_acq.

  Lemma isolated_section_thread_eq (l: list Event) : forall a r,
      IsolatedSection a r l -> tid a = tid r.
  Proof.
    induction 1; auto.
  Qed.
  Hint Resolve isolated_section_thread_eq.
  
  Lemma absurd_acq_open_acquire : forall l e,
      LockSemantics (e :: l) -> is_acq e -> ~ (exists a, OpenAcquire a l).
  Proof.
    intros.
    inversion H; subst; auto; try event_types.
  Qed.
  Hint Resolve absurd_acq_open_acquire.

  Lemma absurd_open_acquire_acq : forall e a l,
      LockSemantics (e :: l) -> OpenAcquire a l -> ~ is_acq e.
  Proof.
    intros.
    inversion H; subst; auto; try event_types.
    contradict H4; eauto.
  Qed.
  Hint Resolve absurd_open_acquire_acq.

  Lemma absurd_open_acquire_rel : forall e a l,
      OpenAcquire a (e :: l) -> ~ is_rel e.
  Proof.
    intros.
    inversion H; intuition; try event_types.
  Qed.
  Hint Resolve absurd_open_acquire_rel.

  Lemma lock_semantics_rel_open_acquire : forall l r,
      LockSemantics (r :: l) -> is_rel r -> (exists a, OpenAcquire a l).
  Proof.
    inversion 1; subst; intros; try event_types; eauto.
  Qed.
  Hint Resolve lock_semantics_rel_open_acquire.

  Lemma open_acquire_tls : forall l a l',
      Forall is_access l -> is_acq a -> OpenAcquire a (l ++ a :: l').
  Proof.
    induction l; intros; simpl.
    apply open_acquire_eq; auto.
    apply open_acquire_cons.
    eapply Forall_inv; eauto.
    apply IHl; auto.
    inversion H; auto.
  Qed.

  Lemma open_isolated_event_access : forall a x l,
      OpenIsolatedEvent a x l -> is_access x.
  Proof.
    induction 1; auto.
  Qed.
  Hint Resolve open_isolated_event_access.

  Lemma closed_isolated_event_acq : forall l a x r,
      ClosedIsolatedEvent a x r l -> is_acq a.
  Proof.
    induction 1; auto.
    eapply open_isolated_event_acq; eauto.
  Qed.
  Hint Resolve closed_isolated_event_acq.
  
  Lemma closed_isolated_event_rel : forall l a x r,
      ClosedIsolatedEvent a x r l -> is_rel r.
  Proof.
    induction 1; eauto.
  Qed.
  Hint Resolve closed_isolated_event_rel.

  Lemma closed_isolated_event_access : forall l a x r,
      ClosedIsolatedEvent a x r l -> is_access x.
  Proof.
    induction 1; eauto.
  Qed.
  Hint Resolve closed_isolated_event_access.

  Lemma isolated_event_access : forall x l,
      IsolatedEvent x l -> is_access x.
  Proof.
    induction 1; eauto.
  Qed.
  Hint Resolve isolated_event_access.

  Lemma open_isolated_event_in : forall l a x,
      OpenIsolatedEvent a x l -> In x l.
  Proof.
    induction 1; simpl; eauto.
  Qed.
  Hint Resolve open_isolated_event_in.
  
  Lemma closed_isolated_event_in : forall l a r x,
      ClosedIsolatedEvent a x r l -> In x l.
  Proof.
    induction 1; simpl; eauto.
  Qed.
  Hint Resolve closed_isolated_event_in.

  Lemma open_acquire_acq_in (l : list Event) : forall a,
      OpenAcquire a l -> In a l.
  Proof.
    induction 1; auto.
  Qed.
  Hint Resolve open_acquire_acq_in.
  
  Lemma isolated_section_acq_in (l : list Event) : forall a r,
      IsolatedSection a r l -> In a l.
  Proof.
    induction 1; eauto using maps_top_to_in.
  Qed.
  Hint Resolve isolated_section_acq_in.
  
  Lemma isolated_section_rel_in (l : list Event) : forall a r,
      IsolatedSection a r l -> In r l.
  Proof.
    induction 1; simpl; eauto.
  Qed.
  Hint Resolve isolated_section_rel_in.

  Lemma open_isolated_event_acq_in : forall l a x,
      OpenIsolatedEvent a x l -> In a l.
  Proof.
    induction 1; simpl; eauto.
  Qed.
  Hint Resolve open_isolated_event_acq_in.
  
  Lemma closed_isolated_event_acq_in : forall l a r x,
      ClosedIsolatedEvent a x r l -> In a l.
  Proof.
    induction 1; simpl; eauto.
  Qed.

  Lemma closed_isolated_event_rel_in : forall l a r x,
      ClosedIsolatedEvent a x r l -> In r l.
  Proof.
    induction 1; simpl; eauto.
  Qed.
  Hint Resolve closed_isolated_event_rel_in.

  Lemma isolated_event_in : forall x l,
      IsolatedEvent x l -> In x l.
  Proof.
    induction 1; simpl; eauto.
  Qed.
  Hint Resolve isolated_event_in.
  
  Lemma acq_hd_not_open_acquire : forall l x,
      LockSemantics (x :: l) ->
      is_acq x ->
      ~ (exists a, OpenAcquire a l).
  Proof.
    intros.
    intuition; inversion H; subst; contradiction + event_types.
  Qed.
  Hint Resolve acq_hd_not_open_acquire.

  Lemma rel_hd_open_acquire : forall l x,
      LockSemantics (x :: l) ->
      is_rel x ->
      (exists a, OpenAcquire a l).
  Proof.
    intros.
    inversion H; subst; eauto; try event_types.
  Qed.
  Hint Resolve rel_hd_open_acquire.
  
  Lemma lock_semantics_cut : forall l1 l2,
      LockSemantics (l1 ++ l2) -> LockSemantics l2.
  Proof.
    induction l1; intros; auto.
    apply IHl1.
    inversion H; auto.
  Qed.
  Hint Resolve lock_semantics_cut.

  Lemma closed_isolated_event_isolated_section (l : list Event) (N : NoDup l) :
    forall a r x,
      ClosedIsolatedEvent a x r l -> IsolatedSection a r l.
  Proof.
    induction 1; eauto.
  Qed.
  Hint Resolve closed_isolated_event_isolated_section.

  Lemma open_isolated_event_thread_eq : forall l a x,
      OpenIsolatedEvent a x l -> tid a = tid x.
  Proof.
    induction 1; auto.
  Qed.
  Hint Resolve open_isolated_event_thread_eq.

  Lemma open_acquire_acq_maps_top : forall l a,
      OpenAcquire a l -> (exists n, MapsToP Event is_critical a n l).
  Proof.
    induction 1.
    exists (length l).
    apply maps_top_eq; unfold is_critical; auto.
    destruct IHOpenAcquire.
    exists x.
    apply maps_top_cons; auto.
  Qed.
    
  Lemma open_isolated_event_acq_maps_top : forall l a x,
      OpenIsolatedEvent a x l -> (exists n, MapsToP Event is_critical a n l).
  Proof.
    induction 1.
    apply open_acquire_acq_maps_top in H0.
    destruct H0.
    exists x.
    apply maps_top_cons; auto.
    destruct IHOpenIsolatedEvent as (n, Hn).
    exists n; eauto.
  Qed.
  Hint Resolve open_isolated_event_acq_maps_top.

  Lemma open_isolated_event_fun : forall l a1 a2 x y,
      OpenIsolatedEvent a1 x l -> OpenIsolatedEvent a2 y l -> a1 = a2.
  Proof.
    intros.
    apply open_isolated_event_acq_maps_top in H.
    apply open_isolated_event_acq_maps_top in H0.
    destruct H as (na1, Hna1).
    destruct H0 as (na2, Hna2).
    eauto.
  Qed.
  Hint Resolve open_isolated_event_fun.

  Lemma open_isolated_events_thread_eq : forall l a1 a2 x y,
      OpenIsolatedEvent a1 x l -> OpenIsolatedEvent a2 y l -> tid x = tid y.
  Proof.
    intros.
    apply open_isolated_event_thread_eq in H as Hx.
    apply open_isolated_event_thread_eq in H0 as Hy.
    assert (a1 = a2) by eauto.
    congruence.
  Qed.

  Lemma closed_isolated_event_acq_thread_eq l (N : NoDup l) : forall a r x,
      ClosedIsolatedEvent a x r l -> tid a = tid x.
  Proof.
    induction 1; eauto.
  Qed.

  Hint Resolve closed_isolated_event_acq_thread_eq.
  Hint Resolve open_isolated_events_thread_eq.
  Lemma closed_isolated_events_thread_eq l (N : NoDup l) : forall a r x y,
      ClosedIsolatedEvent a x r l -> ClosedIsolatedEvent a y r l ->
      tid x = tid y.
  Proof.
    intros.
    apply closed_isolated_event_acq_thread_eq in H; auto.
    apply closed_isolated_event_acq_thread_eq in H0; auto.
    congruence.
  Qed.

  Lemma closed_isolated_event_acq_rel_thread_eq l (N : NoDup l) : forall a r x,
      ClosedIsolatedEvent a x r l -> tid a = tid r.
  Proof.
    induction 1; eauto.
  Qed.
  Hint Resolve closed_isolated_event_acq_rel_thread_eq.

  Lemma absurd_isolated_section_acq_hd (l : list Event) : forall a r,
      NoDup (a :: l) -> ~ IsolatedSection a r (a :: l).
  Proof.
    intros; intuition.
    inversion H0; subst.
    - apply isolated_section_acq in H0; event_types.
    - apply isolated_section_acq_in in H4; inversion H; contradiction.
  Qed.
  
  Lemma absurd_open_acquire_isolated_section (l : list Event) (N : NoDup l) :
    forall a r,
      OpenAcquire a l -> ~ IsolatedSection a r l.
  Proof.
    induction 1; intros.
    - intuition.
      inversion H0; subst; try event_types.
      inversion N; subst.
      contradict H3; eauto.
    - intuition.
      apply IHOpenAcquire; eauto.
  Qed.
  
  Hint Resolve absurd_open_acquire_isolated_section.
  Lemma isolated_section_fun_rel l (N : NoDup l) : forall a r1 r2,
      IsolatedSection a r1 l -> IsolatedSection a r2 l -> r1 = r2.
  Proof.
    induction 1; intros.
    - inversion H2; subst; auto.
      contradict H6; eauto.
    - inversion H0; subst; eauto.
      contradict H; eauto.
  Qed.
  Hint Resolve isolated_section_fun_rel.
      
  Lemma closed_isolated_event_fun_rel l (N : NoDup l) : forall a r1 r2 x y,
      ClosedIsolatedEvent a x r1 l -> ClosedIsolatedEvent a y r2 l -> r1 = r2.
  Proof.
    intros.
    apply closed_isolated_event_isolated_section in H; auto.
    apply closed_isolated_event_isolated_section in H0; eauto.
  Qed.
          
  Lemma open_isolated_event_open_acquire : forall l a x,
      OpenIsolatedEvent a x l -> OpenAcquire a l.
  Proof.
    induction 1; auto.
  Qed.

  Lemma trace_nodup : forall t,
      Trace t -> NoDup t.
  Proof.
    inversion 1; auto.
  Qed.

  Lemma trace_lock_semantics : forall t,
      Trace t -> LockSemantics t.
  Proof.
    inversion 1; auto.
  Qed.

  Lemma open_acquire_dec : forall a t,
      OpenAcquire a t \/ ~ OpenAcquire a t.
  Proof.
    induction t.
    right; intuition; inversion H.
    destruct a0; intuition.
    left; apply open_acquire_cons; simpl; auto; try event_types.
    right; intuition. inversion H0; subst; event_types + contradiction.
    left; apply open_acquire_cons; auto; event_types.
    right; intuition. inversion H0; subst; event_types + contradiction.
    destruct (Event_eq_dec a (Acq n n0)); subst.
    left; apply open_acquire_eq; simpl; auto.
    right; intuition.
    inversion H0; subst; auto.
    unfold is_access in *; simpl in *; intuition.
    destruct (Event_eq_dec a (Acq n n0)); subst.
    left; apply open_acquire_eq; simpl; auto.
    right; intuition.
    inversion H0; subst; auto.
    right; intuition. inversion H0; subst; try event_types.
    unfold is_access in *; simpl in *; intuition.
    right; intuition. inversion H0; subst; try event_types.
    unfold is_access in *; simpl in *; intuition.
  Qed.
    
  Hint Resolve is_critical_dec.
  Hint Resolve Event_eq_dec.
  Lemma open_isolated_event_dec : forall t a x,
      NoDup t -> OpenIsolatedEvent a x t \/ ~ OpenIsolatedEvent a x t.
  Proof.
    induction t; intros.
    - right.
      intuition.
      inversion H0.
    - destruct (IHt a0 x); try trace_inv.
      + destruct (is_access_or_is_critical a).
        * left. apply open_isolated_event_cons; auto.
        * right. intuition. inversion H2; subst; try event_types.
      + destruct (is_access_or_is_critical a0).
        right; intuition.
        absurd (is_acq a0); auto.
        eapply open_isolated_event_acq; eauto.
        destruct H1.
        destruct (is_access_or_is_critical x).
        destruct (open_acquire_dec a0 t).
        (**destruct (maps_top_decidable Event t is_critical a0); auto; try trace_inv.
        destruct H4.**)
        destruct (Nat.eq_dec (tid a0) (tid x)).
        destruct (is_access_or_is_critical a).
        destruct (Event_eq_dec a x); subst.
        left; eauto using open_isolated_event_eq.
        right; intuition.
        inversion H5; subst; try contradiction.
        right; intuition.
        inversion H5; subst; try event_types.
        right; intuition.
        inversion H4; subst; try contradiction.
        right; intuition.
        inversion H4; subst; try contradiction.
        right; intuition.
        inversion H3; subst; try event_types + contradiction.
        right; intuition.
        inversion H2; subst.
        absurd (is_rel a0); eauto.
        contradiction.
  Qed.

  Lemma last_critical_acq_open_acquire : forall t a n,
      MapsToP Event is_critical a n t -> is_acq a -> OpenAcquire a t.
  Proof.
    induction t; intros.
    inversion H.
    inversion H; subst.
    apply open_acquire_eq; auto.
    apply open_acquire_cons.
    destruct (is_access_or_is_critical a); auto.
    contradiction.
    eapply IHt; eauto.
  Qed.

  Lemma last_critical_rel_not_open_acquire : forall t,
      (forall r n, MapsToP Event is_critical r n t -> is_rel r) ->
      ~ (exists a, OpenAcquire a t).
  Proof.
    induction t; intuition.
    destruct H0.
    inversion H0.
    destruct H0.
    apply open_acquire_acq_maps_top in H0 as Hx.
    destruct Hx.
    absurd (is_rel x).
    apply absurd_acq_rel; eauto using open_acquire_maps_top_critical_acq.
    apply H with x0; auto.
  Qed.
      
  Lemma isolated_section_inv : forall t a r,
      IsolatedSection a r t ->
      (exists n, MappedToP Event is_critical r a n t).
  Proof.
    induction t; intros.
    inversion H.
    inversion H; subst.
    apply open_acquire_acq_maps_top in H5.
    destruct H5.
    exists x.
    apply mapped_top_eq; auto.
    unfold is_critical; auto.
    destruct (IHt a0 r); auto.
    exists x.
    apply mapped_top_cons; auto.
  Qed.

  Lemma isolated_section_dec : forall t a r,
      NoDup t -> IsolatedSection a r t \/ ~ IsolatedSection a r t.
  Proof.
    induction t; intros.
    right; intuition. inversion H0.
    assert (NoDup t) by (inversion H; auto).
    destruct (IHt a0 r); auto.
    destruct (is_rel_dec r).
    destruct (is_acq_dec a0).
    destruct (open_acquire_dec a0 t).
    (**destruct (maps_top_decidable Event t is_critical a0); auto.
    destruct H5.**)
    destruct (Event_eq_dec r a); subst.
    destruct (Nat.eq_dec (tid a0) (tid a)).
    left. eapply isolated_section_eq; eauto.
    right; intuition. inversion H5; subst; try contradiction.
    right; intuition. inversion H5; subst; try contradiction.
    right; intuition. inversion H5; subst; try contradiction.
    right; intuition. inversion H4; subst; try contradiction.
    apply H3; eauto.
    right; intuition. inversion H3; subst; try contradiction.
  Qed.
    
  Lemma closed_isolated_event_dec : forall t a x r,
      NoDup t -> ClosedIsolatedEvent a x r t \/ ~ ClosedIsolatedEvent a x r t.
  Proof.
    induction t; intros.
    right; intuition.
    inversion H0.
    assert (NoDup t) by (inversion H; auto).
    destruct (IHt a0 x r); auto.
    destruct (Event_eq_dec a r); subst.
    destruct (isolated_section_dec (r :: t) a0 r); auto.
    destruct (open_isolated_event_dec t a0 x); auto.
    right; intuition. inversion H4; subst; try contradiction.
    right; intuition. inversion H3; subst; try contradiction.
    right; intuition. inversion H2; subst; try contradiction.
  Qed.

  Lemma open_acquire_fun : forall t a b,
      OpenAcquire a t -> OpenAcquire b t -> a = b.
  Proof.
    intros.
    apply open_acquire_acq_maps_top in H.
    apply open_acquire_acq_maps_top in H0.
    destruct H.
    destruct H0.
    eauto using maps_top_funp2.
  Qed.

  Lemma open_acquire_exists_dec : forall t,
      (exists a, OpenAcquire a t) \/ ~ (exists a, OpenAcquire a t).
  Proof.
    induction t; intros.
    right; intuition.
    destruct H.
    inversion H.
    destruct IHt.
    destruct H.
    destruct (is_access_dec a).
    left.
    exists x.
    apply open_acquire_cons; auto.
    destruct (Event_eq_dec x a); subst.
    left.
    exists a; eauto.
    destruct (is_acq_dec a).
    left.
    exists a; eauto.
    right; intuition.
    destruct H2.
    inversion H2; subst; try event_types + contradiction.
    destruct (is_acq_dec a).
    left.
    exists a; eauto.
    right; intuition.
    destruct H1.
    inversion H1; subst; try contradiction.
    eauto.
  Qed.
      
  Hint Resolve last_critical_acq_open_acquire.
  Lemma open_isolated_event_acq_exists_dec : forall t x,
      NoDup t -> (exists a, OpenIsolatedEvent a x t) \/
                 ~ (exists a, OpenIsolatedEvent a x t).
  Proof.
    induction t; intros.
    right; intuition. destruct H0. inversion H0.
    assert (NoDup t) by (inversion H; auto).
    destruct (IHt x); auto.
    destruct H1.
    destruct (is_access_dec a).
    left; exists x0; auto.
    right; intuition.
    destruct H3.
    inversion H3; subst; try contradiction.
    destruct (open_acquire_exists_dec t); auto.
    destruct H2.
    destruct (Event_eq_dec x a); subst.
    destruct (is_access_dec a).
    destruct (Nat.eq_dec (tid x0) (tid a)); subst.
    left. exists x0; eauto.
    right; intuition. destruct H4.
    inversion H4; subst.
    assert (x = x0) by eauto using open_acquire_fun; subst.
    contradiction.
    apply H1; eauto.
    right; intuition. destruct H4.
    absurd (is_access a); eauto.
    right; intuition. destruct H3.
    inversion H3; subst; eauto.
    right; intuition. destruct H3.
    inversion H3; subst; eauto.
  Qed.
  
  Lemma closed_isolated_event_acq_exists_dec : forall t x,
      NoDup t -> (exists a r, ClosedIsolatedEvent a x r t) \/
                 ~ (exists a r, ClosedIsolatedEvent a x r t).
  Proof.
    induction t; intros.
    right; intuition. destruct H0. destruct H0. inversion H0.
    assert (NoDup t) by (inversion H; auto).
    destruct (IHt x); auto.
    destruct H1. destruct H1.
    left. exists x0, x1; auto.
    destruct (open_isolated_event_acq_exists_dec t x); auto.
    destruct H2.
    destruct (isolated_section_dec (a :: t) x0 a); auto.
    left; eauto.
    right; intuition. destruct H4. destruct H4.
    inversion H4; subst.
    assert (x1 = x0) by eauto; subst; contradiction.
    eauto.
    right; intuition. destruct H3. destruct H3.
    inversion H3; subst; eauto.
  Qed.
    
  Lemma isolated_event_dec : forall t x,
      NoDup t -> IsolatedEvent x t \/ ~ IsolatedEvent x t.
  Proof.
    intros.
    destruct (open_isolated_event_acq_exists_dec t x); auto.
    destruct H0.
    left; eauto.
    destruct (closed_isolated_event_acq_exists_dec t x); auto.
    destruct H1. destruct H1.
    left. eauto.
    right; intuition.
    inversion H2; subst; eauto.
  Qed.

  Lemma isolated_section_rel_exists_dec : forall t a,
      NoDup t -> (exists r, IsolatedSection a r t) \/ ~ (exists r, IsolatedSection a r t).
  Proof.
    induction t; intros.
    right; intuition. destruct H0. inversion H0.
    assert (NoDup t) by (inversion H; auto).
    destruct (IHt a0); auto.
    - destruct H1.
      left; eauto.
    - destruct (isolated_section_dec (a :: t) a0 a); auto.
      + left; eauto.
      + right; intuition. destruct H3. inversion H3; subst.
        contradiction.
        eauto.
  Qed.

  Lemma isolated_section_rel_exists_dec' : forall t a,
      LockSemantics t ->
      In a t ->
      is_acq a ->
      NoDup t -> (exists r, IsolatedSection a r t) \/
                 OpenAcquire a t.
  Proof.
    induction t; intros.
    inversion H0.
    assert (NoDup t) by (inversion H2; auto).
    assert (LockSemantics t) by (inversion H; auto).
    inversion H0; subst.
    - inversion H; subst; try event_types.
      right; intuition.
    - destruct (IHt a0); auto.
      + destruct H6.
        left; eauto.
      + destruct (is_access_dec a).
        right; eauto.
        destruct (is_rel_dec a).
        destruct (Nat.eq_dec (tid a) (tid a0)).
        left; eauto.
        inversion H; subst; try event_types.
        assert (a0 = a1) by eauto using open_acquire_fun; subst.
        congruence.
        destruct a; unfold is_access in *; simpl in *; intuition.
        rewrite H9 in H2. crush_list_order.
        inversion H; subst; try contradiction + event_types.
        contradict H13; eauto.
        unfold is_access in *; simpl in *; intuition.
  Qed.
        
  Lemma observes_write_app : forall t1 t2 r w,
      ObservesWrite r w t2 -> ObservesWrite r w (t1 ++ t2).
  Proof.
    induction t1; intros; simpl; auto.
  Qed.

  Lemma observes_write_tls : forall t1 t2 r w,
      NoDup (t1 ++ r :: t2) -> ObservesWrite r w (t1 ++ r :: t2) ->
      ObservesWrite r w (r :: t2).
  Proof.
    induction t1; simpl; intros; auto.
    apply IHt1.
    inversion H; auto.
    apply observes_write_tl with a.
    intuition; subst.
    inversion H; subst.
    contradict H3.
    apply in_app_hd.
    auto.
  Qed.

  Lemma lock_semantics_hd_acq_tl_not_open_acquire : forall t a,
      LockSemantics (a :: t) ->
      OpenAcquire a (a :: t) ->
      ~ (exists a, OpenAcquire a t).
  Proof.
    inversion 1; subst; intros.
    - auto.
    - contradict H2.
      eauto using open_acquire_maps_top_critical_acq.
    - contradict H2.
      eauto using open_acquire_maps_top_critical_acq.
  Qed.
      
  Lemma lock_semantics_not_open_acquire_last_critical_rel : forall t r n,
      LockSemantics t ->
      ~ (exists a, OpenAcquire a t) ->
      MapsToP Event is_critical r n t ->
      is_rel r.
  Proof.
    induction t; intros.
    inversion H1.
    inversion H1; subst.
    destruct H5; auto.
    contradict H0.
    exists a.
    apply open_acquire_eq; auto.
    apply IHt with n.
    inversion H; auto.
    intuition.
    apply H0.
    destruct H2.
    exists x.
    apply open_acquire_cons; auto.
    destruct (is_access_or_is_critical a); contradiction + auto.
    auto.
  Qed.

  Lemma lock_semantics_open_acquire_prev_critical_rel : forall t a r nr,
      NoDup t ->
      LockSemantics t ->
      OpenAcquire a t ->
      MappedToP Event is_critical a r nr t ->
      is_rel r.
  Proof.
    intros.
    induction H2.
    assert (is_acq y).
    eapply open_acquire_maps_top_critical_acq; eauto.
    assert (~ (exists a, OpenAcquire a l))
      by eauto using lock_semantics_hd_acq_tl_not_open_acquire.
    eapply lock_semantics_not_open_acquire_last_critical_rel with (t:=l); eauto.
    assert (y <> z).
    intuition; subst.
    inversion H; subst.
    contradict H5.
    destruct (mapped_top_to_in Event l is_critical z x n); auto.
    apply IHMappedToP; try trace_inv.
    apply open_acquire_tl with z; auto.
    intuition.
    apply H3.
    assert (MapsToP Event is_critical z (length l) (z :: l)).
    apply maps_top_eq; unfold is_critical; auto.
    apply open_acquire_acq_maps_top in H1.
    destruct H1.
    eapply maps_top_funp2; eauto.
  Qed.

  Lemma lock_semantics_acquire_prev_critical_rel : forall t a r n,
      NoDup t ->
      LockSemantics t ->
      In a t ->
      is_acq a ->
      MappedToP Event is_critical a r n t ->
      is_rel r.
  Proof.
    intros.
    apply in_app in H1 as Ha.
    destruct Ha. destruct H4; subst.
    assert (LockSemantics (a :: x0)).
    apply lock_semantics_app_inv with (l1:=x); auto.
    inversion H4; subst; try event_types.
    eapply lock_semantics_open_acquire_prev_critical_rel with (t:=(a::x0)); eauto.
    eapply mapped_top_app_inv with (l1:=x); eauto.
  Qed.

  Lemma lock_semantics_rel_isolated_section : forall t r,
      LockSemantics t ->
      is_rel r ->
      In r t ->
      (exists a, IsolatedSection a r t).
  Proof.
    induction t; intros.
    inversion H1.
    inversion H1; subst.
    inversion H; subst; try event_types.
    exists a; intuition.
    destruct (IHt r); auto.
    inversion H; auto.
    exists x; intuition.
  Qed.
  Lemma open_acquire_access : forall t a x,
      NoDup t ->
      OpenAcquire a t ->
      Lt Event t a x ->
      is_access x.
  Proof.
    induction t; intros.
    inversion H0.
    inversion H0; subst.
    contradict H1.
    apply absurd_lt_hd; auto.
    destruct (Event_eq_dec x a); subst; auto.
    apply IHt with (a0); eauto.
    apply lt_tl with a; auto.
  Qed.

  Lemma isolated_section_access : forall t a r x,
      NoDup t ->
      IsolatedSection a r t ->
      Lt Event t a x ->
      Lt Event t x r ->
      is_access x.
  Proof.
    intros.
    induction t; intros.
    inversion H0.
    inversion H0; subst.
    assert (x <> a0) by eauto using lt_neq.
    eapply open_acquire_access with (t:=t); eauto using lt_tl.
    assert (x <> a0).
    intuition; subst.
    contradict H2.
    apply absurd_lt_hd; auto.
    assert (r <> a0).
    intuition; subst.
    inversion H; subst.
    contradict H7; eauto using isolated_section_rel_in.
    apply IHt; eauto using lt_tl.
  Qed.


End Trace.

Section TraceOrder.

  Ltac resolve_lt l n :=
    (apply lt_def with (yn:=length l) (xn:=n); eauto) +
    (apply lt_cons; auto).

  Lemma observes_write_trace_order (l: list Event) : forall r w,
      ObservesWrite r w l -> Lt Event l w r.
  Proof.
    induction 1; resolve_lt l n.
  Qed.
  
  Lemma isolated_section_trace_order (l: list Event) : forall a r,
      IsolatedSection a r l -> Lt Event l a r.
  Proof.
    induction 1; auto.
    apply lt_hd; eauto using open_acquire_acq_in.
  Qed.

  Lemma open_isolated_event_trace_order : forall x a l,
      OpenIsolatedEvent a x l -> Lt Event l a x.
  Proof.
    induction 1; auto.
    apply lt_hd; eauto using open_acquire_acq_in.
  Qed.

  Hint Resolve open_isolated_event_trace_order.
  Lemma closed_isolated_event_trace_order1 : forall l a x r,
      ClosedIsolatedEvent a x r l -> Lt Event l a x.
  Proof.
    induction 1; resolve_lt l n.
  Qed.
  
  Hint Resolve isolated_section_trace_order.
  Hint Resolve lt_cons.
  Hint Resolve nodup_tl.
  Lemma closed_isolated_event_trace_order2 (l: list Event) (N: NoDup l) : forall a x r,
      ClosedIsolatedEvent a x r l -> Lt Event l a r.
  Proof.
    induction 1; eauto.
  Qed.

  Hint Resolve closed_isolated_event_trace_order1.
  Hint Resolve closed_isolated_event_trace_order2.
  Hint Constructors Lt.
  Lemma closed_isolated_event_trace_order3 (l : list Event) (N: NoDup l) : forall a x r,
      ClosedIsolatedEvent a x r l -> Lt Event l x r.
  Proof.
    induction 1; eauto.
    apply open_isolated_event_in in H0.
    eauto using lt_hd.
  Qed.
  Hint Resolve closed_isolated_event_trace_order3.
    
  Hint Constructors MapsToP.
  Lemma open_acquire_rel_acq_trace_order (l : list Event) (N : NoDup l) :
    forall a r,
      OpenAcquire a l -> is_rel r -> In r l -> Lt Event l r a.
  Proof.
    induction 1; intros.
    inversion H1; subst; try event_types.
    auto using lt_hd.
    inversion H2; subst; try event_types.
    apply lt_cons.
    apply IHOpenAcquire; auto.
    inversion N; auto.
  Qed.
  Hint Resolve open_acquire_rel_acq_trace_order.
  
  Hint Resolve closed_isolated_event_rel.
  Hint Resolve closed_isolated_event_rel_in.
  Hint Resolve open_isolated_event_open_acquire.
  Lemma closed_open_isolated_event_critical_trace_order (l : list Event) (N : NoDup l) :
    forall a1 a2 y r x,
      ClosedIsolatedEvent a1 x r l -> OpenIsolatedEvent a2 y l -> Lt Event l r a2.
  Proof.
    intros.
    apply open_isolated_event_acq_maps_top in H0 as Hn.
    destruct Hn as (n, Hn').
    eapply open_acquire_rel_acq_trace_order; eauto.
  Qed.

  Hint Resolve closed_open_isolated_event_critical_trace_order.
  Lemma closed_open_isolated_events_trace_order (l : list Event) (N : NoDup l) :
    forall a1 a2 y r x,
      ClosedIsolatedEvent a1 x r l -> OpenIsolatedEvent a2 y l -> Lt Event l x y.
  Proof.
    intros.
    apply ListOrder.lt_trans with r; eauto.
    apply ListOrder.lt_trans with a2; eauto.
  Qed.
  Hint Resolve closed_open_isolated_events_trace_order.

  Lemma open_acquire_critical_acq_trace_order (l : list Event) (N : NoDup l) :
    forall a r,
      OpenAcquire a l -> is_critical r -> In r l -> a = r \/ Lt Event l r a.
  Proof.
    induction 1; intros.
    inversion H1; subst; try event_types; auto.
    auto using lt_hd.
    inversion H2; subst; try event_types; auto.
    destruct IHOpenAcquire; auto.
    inversion N; auto.
  Qed.
  Hint Resolve open_acquire_critical_acq_trace_order.
  
  Hint Resolve isolated_section_rel.
  Hint Resolve isolated_section_rel_in.
  Hint Resolve isolated_section_acq_in.
  Lemma isolated_sections_trace_order l (N : NoDup l) :
    forall a1 a2 r1 r2,
      IsolatedSection a1 r1 l -> IsolatedSection a2 r2 l -> Lt Event l a1 a2 ->
      Lt Event l r1 a2.
  Proof.
    induction 1; intros.
    assert (NoDup l) by (inversion N; auto).
    assert (a <> a2) by eauto using lt_neq.
    - inversion H2; subst.
      + contradict H5; eauto using open_acquire_fun.
      + destruct (open_acquire_critical_acq_trace_order l H4 a a2); auto.
        unfold is_critical; eauto using isolated_section_acq.
        eauto.
        contradiction.
        contradict H5.
        apply lt_antisymm with l; auto.
        apply lt_tl with e; auto.
        intuition; subst.
        absurd (is_rel e); eauto using isolated_section_acq.
    - inversion H0; subst.
      + apply lt_cons.
        apply open_acquire_rel_acq_trace_order; eauto.
      + apply lt_cons; eauto.
        apply IHIsolatedSection; eauto.
        apply lt_tl in H1; eauto.
        intuition.
        inversion N; subst.
        contradict H6; eauto.
  Qed.
  Hint Resolve isolated_sections_trace_order.

  Lemma closed_isolated_event_acq_trace_order l (N : NoDup l) :
    forall a1 a2 r1 r2 x y,
      a1 <> a2 -> ClosedIsolatedEvent a1 x r1 l -> ClosedIsolatedEvent a2 y r2 l ->
      Lt Event l x y -> Lt Event l a1 a2.
  Proof.
    induction 2; intros.
    - inversion H2; subst.
      + contradict H; eauto using open_isolated_event_fun.
      + assert (Lt Event l y e) by eauto.
        apply lt_tl in H3; eauto.
        assert (e = y) by eauto using lt_antisymm; subst.
        contradict H4; eauto using lt_irrefl.
        intuition; subst.
        apply closed_isolated_event_in in H8; inversion N; contradiction.
    - inversion H1; subst.
      + apply ListOrder.lt_trans with r; eauto.
      + apply lt_tl in H2; eauto.
        intuition; subst.
        apply closed_isolated_event_in in H7; inversion N; contradiction.
  Qed.
  Hint Resolve closed_isolated_event_acq_trace_order.   

  Lemma lock_semantics_critical_trace_order1 : forall t a r1 r2,
      NoDup t ->
      LockSemantics t ->
      IsolatedSection a r1 t ->
      Lt Event t a r2 ->
      r1 <> r2 ->
      is_rel r2 ->
      Lt Event t r1 r2.
  Proof.
    induction t; intros.
    inversion H1.
    inversion H1; subst.
    eapply open_acquire_critical_acq_trace_order with (r:=r2) in H10 as Ha0.
    intuition; subst.
    contradict H4; eauto using isolated_section_acq.
    absurd (a0 = r2).
    eauto using lt_neq.
    apply lt_antisymm with t; auto.
    inversion H; auto.
    apply lt_tl with a; auto.
    inversion H; auto.
    unfold is_critical; auto.
    destruct (lt_in Event (a :: t) a0 r2); auto.
    inversion H6; subst; contradiction + auto.
    destruct (Event_eq_dec a r2); subst.
    apply lt_hd.
    eauto using isolated_section_rel_in.
    apply lt_cons.
    apply IHt with a0; auto.
    inversion H; auto.
    inversion H0; auto.
    apply lt_tl with a; auto.
  Qed.
    
  Lemma lock_semantics_critical_trace_order2 : forall t a x a',
      NoDup t ->
      LockSemantics t ->
      is_critical a' ->
      a' <> a ->
      Lt Event t a' x ->
      OpenIsolatedEvent a x t ->
      Lt Event t a' a.
  Proof.
    induction t; intros.
    inversion H4.
    inversion H4; subst.
    assert (NoDup t) by (inversion H; auto).
    destruct (open_acquire_critical_acq_trace_order t H5 a0 a'); auto.
    destruct (lt_in Event (a :: t) a' a); intuition.
    inversion H6; subst; try crush_list_order; auto.
    subst.
    contradiction.
    apply lt_cons.
    apply IHt with x; auto.
    inversion H; auto.
    inversion H0; auto.
    apply lt_tl with a.
    intuition; subst.
    inversion H; subst.
    apply H7; eauto using open_isolated_event_in.
    auto.
  Qed.
  
  Hint Resolve isolated_section_rel_in.
  Hint Resolve isolated_section_acq_in.
  Lemma lock_semantics_critical_trace_order3 : forall t r a1 a2,
      NoDup t ->
      LockSemantics t ->
      IsolatedSection a1 r t ->
      Lt Event t a1 a2 ->
      is_acq a2 ->
      Lt Event t r a2.
  Proof.
    induction t; intros.
    inversion H1.
    destruct (lt_in Event (a :: t) a1 a2); auto.
    inversion H5; subst.
    apply lt_hd.
    inversion H1; subst; try event_types; eauto.
    inversion H1; subst.
    absurd (a1 = a2).
    eauto using lt_neq.
    destruct (open_acquire_acq_maps_top t a1); auto.
    assert (a2 = a1 \/ Lt Event (a :: t) a2 a1).
    eapply maps_top_lt_p with (P:=is_acq).
    apply Event_eq_dec.
    apply maps_top_cons; auto.
    apply maps_top_nimpl with (Q:=is_critical).
    eauto.
    eauto using open_acquire_maps_top_critical_acq.
    intros.
    intuition.
    apply H8; unfold is_critical; auto.
    auto using in_cons.
    eauto.
    intuition.
    apply lt_antisymm with (a :: t); auto.
    apply lt_cons.
    apply IHt with a1.
    inversion H; auto.
    inversion H0; auto.
    auto.
    apply lt_tl with a; auto.
    intuition; subst.
    crush_list_order.
    auto.
  Qed.

  Lemma lock_semantics_critical_trace_order4 : forall t a r1 r2,
      NoDup t ->
      LockSemantics t ->
      IsolatedSection a r2 t ->
      Lt Event t r1 r2 ->
      is_rel r1 ->
      Lt Event t r1 a.
  Proof.
    induction t; intros.
    inversion H1.
    destruct (lt_in Event (a :: t) r1 r2); auto.
    inversion H5; subst.
    inversion H1; subst.
    apply lt_cons.
    destruct (open_acquire_acq_maps_top t a0); auto.
    apply maps_top_lt_p with (b:=r1) in H6 as HLt.
    intuition; subst.
    absurd (is_acq a0); eauto using open_acquire_maps_top_critical_acq.
    apply Event_eq_dec.
    inversion H4; subst; auto.
    crush_list_order.
    unfold is_critical; auto.
    inversion H; subst.
    contradict H8; eauto using isolated_section_rel_in.
    apply lt_cons.
    apply IHt with r2; eauto; try trace_inv.
    inversion H1; subst; auto.
    crush_list_order.
    apply lt_tl with a; auto.
    intuition; subst.
    crush_list_order.
  Qed.

  Lemma isolated_section_open_isolated_event_trace_order : forall l a1 r1 a x,
      NoDup l ->
      IsolatedSection a1 r1 l ->
      OpenIsolatedEvent a x l ->
      Lt Event l r1 a.
  Proof.
    intros.
    apply open_isolated_event_acq_maps_top in H1 as Hn.
    destruct Hn as (n, Hn').
    eapply open_acquire_rel_acq_trace_order; eauto.
  Qed.
    
  Lemma isolated_section_fun_acq : forall t a1 a2 r,
      NoDup t ->
      IsolatedSection a1 r t ->
      IsolatedSection a2 r t ->
      a1 = a2.
  Proof.
    induction t; intros.
    inversion H0.
    inversion H0; inversion H1; subst.
    eauto using open_acquire_fun.
    inversion H.
    contradict H6; eauto using isolated_section_rel_in.
    inversion H.
    contradict H4; eauto using isolated_section_rel_in.
    apply IHt with r.
    inversion H; auto.
    auto.
    auto.
  Qed.

  Lemma lock_semantics_critical_trace_order5 : forall t a1 r1 a2 x r2,
      NoDup t ->
      LockSemantics t ->
      IsolatedSection a1 r1 t ->
      ClosedIsolatedEvent a2 x r2 t ->
      Lt Event t r1 x ->
      Lt Event t r1 a2.
  Proof.
    induction t; intros.
    inversion H2.
    inversion H2; subst.
    apply lt_cons.
    eapply isolated_section_open_isolated_event_trace_order; eauto.
    inversion H1; subst; eauto.
    contradict H3; auto using absurd_lt_hd.
    apply lt_cons.
    eapply IHt; eauto.
    inversion H0; auto.
    inversion H1; subst; eauto.
    contradict H3; auto using absurd_lt_hd.
    apply lt_tl with a.
    intuition; subst.
    absurd (Lt Event (a :: t) a r2).
    auto using absurd_lt_hd.
    eauto using closed_isolated_event_trace_order3.
    auto.
  Qed.

  Lemma open_isolated_event_t : forall t a x,
      NoDup t ->
      OpenAcquire a t ->
      Lt Event t a x ->
      is_access x ->
      tid a = tid x ->
      OpenIsolatedEvent a x t.
  Proof.
    induction t; intros.
    inversion H0.
    inversion H0; subst.
    contradict H1; auto using absurd_lt_hd.
    destruct (Event_eq_dec x a); subst.
    eapply open_isolated_event_eq; eauto.
    apply open_isolated_event_cons.
    destruct (is_access_or_is_critical a); contradiction + auto.
    apply IHt; auto.
    inversion H; auto.
    apply lt_tl with a; auto.
  Qed.

  Lemma isolated_section_closed_isolated_event : forall t a x r,
      NoDup t ->
      IsolatedSection a r t ->
      Lt Event t a x ->
      Lt Event t x r ->
      is_access x ->
      tid a = tid x ->
      ClosedIsolatedEvent a x r t.
  Proof.
    induction t; intros.
    inversion H0.
    inversion H0; subst.
    apply closed_isolated_event_eq; eauto.
    eapply open_isolated_event_t; eauto.
    inversion H; auto.
    apply lt_tl with a; auto; crush_list_order.
    apply closed_isolated_event_cons.
    apply IHt; auto; try trace_inv.
    apply lt_tl with a; auto.
    intuition; subst.
    contradict H2.
    apply absurd_lt_hd; auto.
    apply lt_tl with a; auto.
    intuition; subst.
    inversion H; subst.
    contradict H7; eauto using isolated_section_rel_in.
  Qed.

  Lemma lock_semantics_critical_before_open_acquire : forall t a oa,
      NoDup t ->
      OpenAcquire oa t ->
      In a t ->
      a <> oa ->
      is_critical a ->
      Lt Event t a oa.
  Proof.
    intros.
    induction H0.
    apply lt_hd.
    inversion H1; subst; contradiction + auto.
    apply lt_cons.
    apply IHOpenAcquire.
    inversion H; auto.
    inversion H1; subst; event_types + auto.
    auto.
  Qed.

  Lemma lock_semantics_critical_trace_order6 : forall t a1 r1 a2 x r2,
      NoDup t ->
      LockSemantics t ->
      IsolatedSection a1 r1 t ->
      ClosedIsolatedEvent a2 x r2 t ->
      Lt Event t a1 x ->
      a1 <> a2 ->
      Lt Event t a1 a2.
  Proof.
    induction t; intros.
    inversion H2.
    inversion H1; subst.
    inversion H2; subst.
    contradict H4.
    eapply isolated_section_fun_acq; eauto using closed_isolated_event_isolated_section.
    absurd (a1 = x).
    intuition; subst.
    absurd (is_access x); eauto using isolated_section_acq, closed_isolated_event_access.
    apply lt_antisymm with (a :: t); auto.
    apply ListOrder.lt_trans with r2; auto.
    eauto using closed_isolated_event_trace_order3.
    destruct (open_acquire_acq_maps_top t a1); auto.
    destruct (maps_top_lt_p Event t is_critical a1 x0 r2); auto.
    eauto using closed_isolated_event_rel_in.
    unfold is_critical; eauto using closed_isolated_event_rel.
    subst.
    absurd (is_rel a1).
    eauto using isolated_section_acq.
    eapply closed_isolated_event_rel; eauto.
    inversion H2; subst.
    apply ListOrder.lt_trans with r1; auto.
    apply lt_cons.
    eapply isolated_section_open_isolated_event_trace_order with (l:=t); eauto.
    apply lt_cons.
    apply IHt with r1 x r2; auto.
    inversion H; auto.
    inversion H0; auto.
    apply lt_tl with a; auto.
    intuition; subst.
    absurd (Lt Event (a :: t) a r2).
    apply absurd_lt_hd; auto.
    eauto using closed_isolated_event_trace_order3.
  Qed.

  Lemma isolated_section_open_acquire_trace_order : forall t a r oa,
      NoDup t ->
      LockSemantics t ->
      IsolatedSection a r t ->
      OpenAcquire oa t ->
      Lt Event t r oa.
  Proof.
    induction t; intros.
    inversion H1.
    inversion H2; subst.
    apply lt_hd.
    assert (In r (a :: t)) by eauto.
    inversion H3; subst; auto.
    absurd (is_rel r); eauto.
    inversion H1; subst.
    assert (oa = a0) by eauto using open_acquire_fun; subst; event_types.
    apply lt_cons.
    apply IHt with a0; try trace_inv; auto.
  Qed.

  Lemma absurd_closed_open_isolated_event : forall t a1 a2 r y,
      NoDup t ->
      OpenIsolatedEvent a1 y t ->
      ClosedIsolatedEvent a2 y r t ->
      False.
  Proof.
    intros.
    absurd (Lt Event t y y).
    auto using lt_irrefl.
    apply ListOrder.lt_trans with r; eauto using closed_isolated_event_trace_order3.
    apply ListOrder.lt_trans with a1; eauto using closed_open_isolated_event_critical_trace_order.
  Qed.
    
  Lemma closed_isolated_event_fun : forall t a1 r1 a2 r2 x,
      NoDup t ->
      ClosedIsolatedEvent a1 x r1 t ->
      ClosedIsolatedEvent a2 x r2 t ->
      a1 = a2 /\ r1 = r2.
  Proof.
    induction 2; intros.
    inversion H2; subst.
    split; eauto using open_isolated_event_fun.
    absurd (OpenIsolatedEvent a e l /\ ClosedIsolatedEvent a2 e r2 l).
    intuition.
    eapply absurd_closed_open_isolated_event with (t:=l); eauto.
    intuition.
    inversion H; subst; auto.
    intuition.
    inversion H1; subst.
    absurd (OpenIsolatedEvent a2 x l /\ ClosedIsolatedEvent a x r l).
    intuition.
    eapply absurd_closed_open_isolated_event with (t:=l); eauto.
    intuition.
    destruct H2; auto.
    inversion H1; subst.
    absurd (OpenIsolatedEvent a2 x l /\ ClosedIsolatedEvent a x r l).
    intuition.
    eapply absurd_closed_open_isolated_event with (t:=l); eauto.
    intuition.
    destruct H2; auto.
  Qed.
    
End TraceOrder.

Hint Resolve trace_nodup.
Hint Resolve trace_lock_semantics.
Hint Resolve observes_write_conflicts.
Hint Resolve observes_write_trace_order.
Hint Resolve open_acquire_acq_in.
Hint Resolve isolated_section_rel_in.
Hint Resolve isolated_section_acq_in.
Hint Resolve open_isolated_event_acq_in.
Hint Resolve open_isolated_event_in.
Hint Resolve closed_isolated_event_rel_in.
Hint Resolve closed_isolated_event_acq_in.
Hint Resolve closed_isolated_event_in.
Hint Resolve isolated_section_acq.
Hint Resolve open_acquire_maps_top_critical_acq.
Hint Resolve isolated_section_rel.
Hint Resolve open_isolated_event_acq.
Hint Resolve open_isolated_event_access.
Hint Resolve closed_isolated_event_rel.
Hint Resolve closed_isolated_event_acq.
Hint Resolve closed_isolated_event_access.
Hint Resolve isolated_section_thread_eq.
Hint Resolve isolated_section_trace_order.
Hint Resolve closed_isolated_event_trace_order3.
Hint Resolve closed_isolated_event_trace_order2.
Hint Resolve closed_isolated_event_trace_order1.
Hint Resolve open_isolated_event_trace_order.
Hint Resolve lock_semantics_critical_trace_order6.
Hint Resolve lock_semantics_critical_trace_order5.
Hint Resolve lock_semantics_critical_trace_order4.
Hint Resolve lock_semantics_critical_trace_order3.
Hint Resolve lock_semantics_critical_trace_order2.
Hint Resolve lock_semantics_critical_trace_order1.
Hint Resolve open_acquire_critical_acq_trace_order.
Hint Resolve isolated_section_open_acquire_trace_order.

Require Import Definitions.
Require Import Event.
Require Import ListOrder.
Require Import Trace.
Require Import ProgramOrder.
Require Import List Arith Relations Relation_Operators Operators_Properties.

Section WDC.

  Hint Resolve program_order_trace_order.
  Lemma wdc_relation_trace_order (l : list Event) (N: NoDup l) : forall e1 e2,
      WDCRelation l e1 e2 -> Lt Event l e1 e2.
  Proof.
    intros x y H; inversion H; subst; auto.
    apply ListOrder.lt_trans with ay; auto.
    apply ListOrder.lt_trans with ay; auto.
    eauto using closed_isolated_event_trace_order1.
  Qed.
  Hint Resolve wdc_relation_trace_order.

  Hint Resolve program_order_in.
  Lemma wdc_relation_in (l : list Event) : forall e1 e2,
      WDCRelation l e1 e2 -> In e1 l /\ In e2 l.
  Proof.
    inversion 1; subst; eauto.
  Qed.
  Hint Resolve wdc_relation_in.

  Lemma wdc_order_trace_order (l : list Event) (N: NoDup l) : forall e1 e2,
      WDCOrder l e1 e2 -> e1 = e2 \/ Lt Event l e1 e2.
  Proof.
    induction 1; intros; auto.
    destruct (IHclos_refl_trans_1n); subst.
    right; eauto using wdc_relation_trace_order.
    right.
    apply ListOrder.lt_trans with y; eauto using wdc_relation_trace_order.
  Qed.
  Hint Resolve wdc_order_trace_order.
  
  Lemma wdc_order_antisymm (l : list Event) (N: NoDup l) : forall e1 e2,
      WDCOrder l e1 e2 -> WDCOrder l e2 e1 -> e1 = e2.
  Proof.
    intros.
    assert (e1 = e2 \/ Lt Event l e1 e2) by eauto.
    assert (e2 = e1 \/ Lt Event l e2 e1) by eauto.
    intuition.
    eauto.
  Qed.

  Lemma absurd_wdc_relation_nil : forall i j,
      ~ WDCRelation nil i j.
  Proof.
    intuition.
    induction H;
      try match goal with
          | [ H : ProgramOrder _ _ _ |- _ ] =>
            inversion H
          end;
      match goal with
      | [ H0 : Lt _ _ _ _ |- _ ] =>
        inversion H0;
          match goal with
          | [ H1 : IndexOf _ _ _ _ |- _ ] =>
            inversion H1
          end
      end.
  Qed.
  Hint Resolve absurd_wdc_relation_nil.

  Hint Constructors Lt.
  Hint Constructors IndexOf.
  Hint Constructors WDCRelation.
  Hint Constructors ProgramOrder.
  Hint Constructors ClosedIsolatedEvent.
  Hint Constructors OpenIsolatedEvent.
  Hint Resolve lt_cons.
  Hint Resolve open_isolated_event_open_acquire.
  Hint Resolve maps_top_funp2.
  Lemma wdc_relation_cons : forall t i j k,
      Trace (k :: t) -> WDCRelation t i j -> WDCRelation (k :: t) i j.
  Proof.
    intros.
    inversion H; subst.
    inversion H0; subst.
    inversion H4; subst; eauto.
    destruct k.
    eapply wdc_critical_rule1; eauto.
    apply open_isolated_event_cons; unfold is_access; simpl; auto.
    eapply wdc_critical_rule1; eauto.
    apply open_isolated_event_cons; unfold is_access; simpl; auto.
    inversion H2; subst.
    contradict H11; eauto using open_isolated_event_open_acquire.
    contradict H11; simpl; auto.
    contradict H11; unfold is_access; simpl; intuition.
    eapply wdc_critical_rule2; eauto.
    apply closed_isolated_event_eq; auto.
    assert (exists n, MapsToP Event is_critical ay n t).
    eauto using open_isolated_event_acq_maps_top.
    destruct H8.
    eapply isolated_section_eq; simpl; eauto.
    destruct (Nat.eq_dec (tid ay) n0); auto.
    inversion H2; subst; try event_types.
    eapply open_isolated_event_open_acquire in H5.
    assert (a = ay) by eauto using open_acquire_fun; subst.
    simpl in H16; contradiction.
    unfold is_access in *; simpl in *; intuition.
    eapply wdc_critical_rule2; eauto.
  Qed.

  Hint Resolve nodup_tl.
  Lemma wdc_relation_cons_inv : forall t x y z,
      NoDup (z :: t) -> z <> y -> WDCRelation (z :: t) x y -> WDCRelation t x y.
  Proof.
    intros.
    assert (Lt Event (z :: t) x y) by auto.
    assert (Lt Event t x y) by eauto using lt_tl.
    inversion H1; subst.
    - inversion H4; eauto.
    - inversion H4; subst.
      contradict H8; auto using absurd_lt_hd.
      inversion H5; subst.
      contradiction.
      apply wdc_critical_rule1 with (x:=x0) (ax:=ax) (ay:=ay); auto.
      eapply closed_open_isolated_event_critical_trace_order; eauto.
    - inversion H4; subst.
      contradict H8; auto using absurd_lt_hd.
      inversion H5; subst.
      apply wdc_critical_rule1 with (x:=x0) (ax:=ax) (ay:=ay); auto.
      eapply closed_open_isolated_event_critical_trace_order; eauto.
      apply wdc_critical_rule2 with (x:=x0) (ax:=ax) (ay:=ay) (ry:=ry); auto.
      eapply isolated_sections_trace_order; eauto using nodup_tl.
      apply closed_isolated_event_isolated_section with (a:=ax) (x:=x0); eauto.
      apply closed_isolated_event_isolated_section with (x:=y); eauto.
      eapply closed_isolated_event_acq_trace_order; eauto.
      unfold not; intros; subst.
      assert (Lt Event t ay x).
      eapply closed_isolated_event_trace_order2; eauto.
      apply lt_cons with (z:=z) in H9.
      absurd (x = ay); eauto using lt_neq, lt_antisymm.
      apply ListOrder.lt_trans with x; eauto.
  Qed.

  Lemma wdc_order_in : forall t x y,
      x <> y -> WDCOrder t x y -> In x t /\ In y t.
  Proof.
    intros.
    induction H0.
    intuition.
    destruct (Event_eq_dec y z); subst.
    eauto using wdc_relation_in.
    destruct IHclos_refl_trans_1n; auto.
    intuition.
    destruct (wdc_relation_in t x y); auto.
  Qed.
  
  Lemma wdc_order_cons_inv : forall t x y z,
      NoDup (z :: t) -> z <> y -> WDCOrder (z :: t) x y -> WDCOrder t x y.
  Proof.
    intros.
    induction H1.
    constructor.
    apply IHclos_refl_trans_1n in H0 as Hyz0.
    destruct (Event_eq_dec x y); subst; auto.
    destruct (Event_eq_dec z y); subst.
    inversion H; subst.
    contradict H5.
    destruct (wdc_order_in t y z0); auto.
    assert (WDCRelation t x y).
    eapply wdc_relation_cons_inv; eauto.
    apply Relation_Operators.rt1n_trans with y; auto.
  Qed.
    
  Lemma wdc_order_cons : forall t i j k,
      Trace (k :: t) -> WDCOrder t i j -> WDCOrder (k :: t) i j.
  Proof.
    intros.
    induction H0.
    constructor.
    apply Relation_Operators.rt1n_trans with y.
    eauto using wdc_relation_cons.
    eauto using wdc_relation_cons.
  Qed.
      
  Lemma wdc_relation_program_order : forall t x y,
      WDCRelation t x y -> ~ is_rel x -> ProgramOrder t x y.
  Proof.
    inversion 1; intros; subst; auto.
    contradict H7; eauto.
    contradict H7; eauto.
  Qed.
  
  Lemma wdc_order_thread_neq_rel : forall t x y,
      NoDup t ->
      WDCOrder t x y ->
      tid x <> tid y ->
      ~ is_rel x ->
      (exists r, is_rel r /\ ProgramOrder t x r /\ WDCOrder t r y).
  Proof.
    intros.
    induction H0.
    - congruence.
    - assert (ProgramOrder t x y).
      eapply wdc_relation_program_order; auto.
      assert (tid y <> tid z).
      intuition; subst.
      inversion H4; subst; congruence.
      destruct (is_rel_dec y).
      exists y; intuition.
      destruct (IHclos_refl_trans_1n); auto.
      destruct H7.
      destruct H8.
      exists x0; split; auto.
      split; auto.
      inversion H4; inversion H8; subst.
      apply program_trace_rule; eauto using ListOrder.lt_trans.
      congruence.
  Qed.

  Lemma wdc_order_transitive : forall t x y z,
    WDCOrder t x y -> WDCOrder t y z -> WDCOrder t x z.
  Proof.
    unfold WDCOrder; intros.
    apply clos_rt_rt1n_iff.
    apply clos_rt_rt1n_iff in H.
    apply clos_rt_rt1n_iff in H0.
    apply rt_trans with y; auto.
  Qed.

End WDC.
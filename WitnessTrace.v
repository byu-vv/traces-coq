
Require Import Definitions.
Require Import Event.
Require Import ListOrder.
Require Import Trace.
Require Import ProgramOrder.
Require Import Race.
Require Import CausalEvent.
Require Import List Arith.

Section WitnessTrace.

  Variable C : list Event -> Event -> Event -> Prop.
  Variable causality_trace_order : forall t x y,
      NoDup t -> C t x y -> x = y \/ Lt Event t x y.
  Variable program_order_causality : forall t x y,
      ProgramOrder t x y -> C t x y.
  Variable causality_trans : forall t x y z,
      C t x y ->
      C t y z ->
      C t x z.
  Variable causality_cons : forall t x y z,
      Trace (z :: t) ->
      C t x y ->
      C (z :: t) x y.
  Variable causality_thread_neq_exists_rel : forall t x y,
      NoDup t ->
      C t x y ->
      tid x <> tid y ->
      ~ is_rel x ->
      (exists r, is_rel r /\ ProgramOrder t x r /\ C t r y).
  Variable causality_refl : forall t x,
      C t x x.

  Lemma same_observations_t exe : forall t r w,
      NoDup t ->
      ObservesWrite r w exe ->
      In r t ->
      In w t ->
      (forall i j, conflicts i j -> In i t -> In j t ->
                   (Lt Event exe i j <-> Lt Event t i j)) ->
      (forall e, Lt Event t e r -> conflicts r e -> e = w \/ Lt Event t e w) ->
      ObservesWrite r w t.
  Proof.
    intros.
    destruct (in_app Event t r); auto.
    destruct H5; rewrite H5.
    apply observes_write_app.
    assert (NoDup (x ++ r :: x0)) by (rewrite <- H5; auto).
    assert (In w x0).
    apply lt_hd_in_tl_app with (l1:=x) (l2:=x0) (b:=w) (a:=r); auto.
    rewrite <- H5.
    apply H3; eauto.
    eapply in_to_maps_top with (P:=(conflicts r)) in H7 as Htemp; eauto.
    destruct Htemp.
    destruct H8.
    eapply observes_write_eq; eauto.
    eauto using observes_write_read.
    destruct (H4 x1); eauto.
    rewrite H5.
    apply lt_hd_app; eauto using observes_write_trace_order.
    eapply maps_top_inv_p; eauto.
    subst; auto.
    apply H8.
    assert (Lt Event t w x1).
    assert (w = x1 \/ Lt Event x0 w x1).
    eapply maps_top_lt_p; eauto.
    intuition.
    subst; auto.
    assert (w = x1).
    apply lt_antisymm with (x ++ r :: x0); eauto.
    rewrite <- H5; auto.
    subst; auto.
    assert (w = x1).
    apply lt_antisymm with t; auto.
    subst; auto.
  Qed.

  Lemma lock_semantics_t : forall t,
      NoDup t ->
      (forall r1 r2, is_rel r1 -> is_rel r2 -> Lt Event t r1 r2 ->
                     (exists a, is_acq a /\ tid a = tid r2 /\ Lt Event t r1 a
                                /\ Lt Event t a r2)) ->
      (forall r, is_rel r -> In r t ->
                 (exists a n, is_acq a /\ tid a = tid r
                              /\ MappedToP Event is_critical r a n t)) ->
      (forall a, is_acq a -> In a t ->
                 (forall r rn, MappedToP Event is_critical a r rn t -> is_rel r)) ->
      LockSemantics t.
  Proof.
    induction t; intros.
    constructor.
    assert (LockSemantics t).
    apply IHt; try trace_inv.
    clear H1; clear H2; intros.
    destruct (H0 r1 r2); clear H0; auto using lt_cons.
    exists x; intuition.
    destruct a.
    apply lt_tl with (Rd n n0 n1); auto.
    intuition; subst; simpl in *; contradiction.
    apply lt_tl with (Wr n n0 n1); auto.
    intuition; subst; simpl in *; contradiction.
    destruct (Event_eq_dec x (Acq n n0)); subst.
    contradict H7.
    apply absurd_lt_hd; auto.
    apply lt_tl with (Acq n n0); auto.
    apply lt_tl with (Rel n n0); auto.
    intuition; subst; simpl in *; contradiction.
    apply lt_tl with a.
    intuition; subst.
    destruct (lt_in Event t r1 a); auto; crush_list_order.
    auto.
    clear H0; clear H2; intros.
    destruct (H1 r); clear H1; auto using in_cons.
    destruct H3.
    exists x, x0; intuition.
    inversion H5; subst; auto.
    destruct a; try event_types.
    crush_list_order.
    clear H0; clear H1; intros.
    apply H2 with (a0:=a0) (rn:=rn); auto using in_cons.
    apply mapped_top_cons; auto.
    destruct a.
    - apply lock_semantics_other; auto.
      unfold is_access; simpl; auto.
    - apply lock_semantics_other; auto.
      unfold is_access; simpl; auto.
    - assert (~ (exists a, OpenAcquire a t)).
      apply last_critical_rel_not_open_acquire.
      intros.
      apply H2 with (a:=Acq n n0) (rn:=n1); simpl; auto.
      apply mapped_top_eq; auto.
      unfold is_critical; simpl; auto.
      apply lock_semantics_acq; simpl; auto.
    - destruct (H1 (Rel n n0)); simpl; auto.
      destruct H4; intuition.
      eapply lock_semantics_rel; simpl; eauto.
      eapply last_critical_acq_open_acquire with (a:=x).
      eapply mapped_top_maps_top; eauto.
      auto.
  Qed.
    
  Lemma witness_order_step : forall t xr yr xyrace x y,
      CausalEvent C t xr yr xyrace x ->
      CausalEvent C t xr yr xyrace y ->
      WitnessRelation C t xr yr xyrace x y -> WitnessOrder C t xr yr xyrace x y.
  Proof.
    intros.
    apply witness_order_trans with y; auto.
    apply witness_order_refl; auto.
  Qed.

  Lemma linearization_witness_relation_order exe xr yr xyrace : forall t,
      CausalEvents C exe xr yr xyrace t ->
      PrefixLinearization (WitnessOrder C exe xr yr xyrace) t ->
      PrefixLinearization (WitnessRelation C exe xr yr xyrace) t.
  Proof.
    unfold PrefixLinearization; intros.
    apply H0; auto.
    apply witness_order_trans with y; eauto.
    apply witness_order_refl; eauto.
  Qed.

  Lemma conflicts_causal_events_same_order exe xr yr xyrace : forall t x y,
      NoDup t ->
      CausalEvents C exe xr yr xyrace t ->
      PrefixLinearization (WitnessRelation C exe xr yr xyrace) t ->
      CausalEvent C exe xr yr xyrace x ->
      CausalEvent C exe xr yr xyrace y ->
      conflicts x y ->
      (Lt Event exe x y <-> Lt Event t x y).
  Proof.
    split; intros.
    apply H1; eauto.
    apply witness_step.
    inversion xyrace; subst; auto.
    inversion H6; subst.
    apply causality_cons; auto.
    apply H7; auto.
    apply lt_tl with yr; auto.
    unfold not; intros; subst.
    contradict H3; eauto using first_race_yr_not_causal_event.
    inversion xyrace; subst.
    inversion H6; subst.
    assert (Lt Event (yr :: t0) x y \/ Lt Event (yr :: t0) y x).
    apply lt_total_order; eauto.
    destruct H13; auto.
    absurd (y = x); eauto using lt_neq.
    apply lt_antisymm with t; auto.
    apply H1; eauto.
    apply witness_step.
    apply causality_cons; auto.
    apply H7.
    apply lt_tl with yr; auto.
    unfold not; intros; subst.
    contradict H2; eauto using first_race_yr_not_causal_event.
    auto.
  Qed.

  Hint Resolve race_trace.
  Hint Resolve trace_nodup.
  Hint Resolve trace_lock_semantics.
  Lemma rel_causal_events_same_order exe xr yr xyrace : forall t x y,
      NoDup t ->
      CausalEvents C exe xr yr xyrace t ->
      PrefixLinearization (WitnessOrder C exe xr yr xyrace) t ->
      CausalEvent C exe xr yr xyrace x ->
      CausalEvent C exe xr yr xyrace y ->
      is_rel x ->
      is_rel y ->
      (Lt Event exe x y <-> Lt Event t x y).
  Proof.
    split; intros.
    apply H1; eauto.
    destruct (lock_semantics_rel_isolated_section exe y); eauto.
    apply witness_order_trans with x0; eauto.
    apply witness_critical1; auto.
    eapply isolated_section_acq; eauto.
    apply lock_semantics_critical_trace_order4 with y; eauto.
    apply witness_order_step; eauto.
    apply witness_step; eauto using isolated_section_to_program_order.
    assert (NoDup exe) by eauto.
    assert (x <> y) by eauto using lt_neq.
    assert (In x exe) by eauto using causal_event_in_exe.
    assert (In y exe) by eauto using causal_event_in_exe.
    destruct (lt_total_order Event exe H7 x y); auto.
    absurd (x = y).
    intuition; subst; try crush_list_order.
    apply lt_antisymm with t; auto.
    apply H1; eauto.
    destruct (lock_semantics_rel_isolated_section exe x); eauto.
    apply witness_order_trans with x0; eauto.
    apply witness_critical1; auto.
    eapply isolated_section_acq; eauto.
    apply lock_semantics_critical_trace_order4 with x; eauto.
    apply witness_order_step; eauto.
    apply witness_step; eauto using isolated_section_to_program_order.
  Qed.

  Lemma witness_linearization_causal_events_lock_semantics1 exe xr yr xyrace :
    forall t,
      NoDup t ->
      PrefixLinearization (WitnessRelation C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      (forall r, is_rel r -> In r t ->
                 (exists a, is_acq a /\ tid a = tid r /\ Lt Event t a r)).
  Proof.
    intros.
    assert (CausalEvent C exe xr yr xyrace r) by eauto.
    destruct (lock_semantics_rel_isolated_section exe r); eauto.
    exists x; intuition.
    eapply isolated_section_acq; eauto.
    eauto.
    apply H0; eauto.
    apply witness_step.
    eauto using program_trace_rule.
  Qed.

  Lemma witness_linearization_causal_events_lock_semantics3 exe xr yr xyrace :
    forall t,
      NoDup t ->
      PrefixLinearization (WitnessOrder C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      (forall r, is_rel r -> In r t ->
                 (exists a n, is_acq a /\ tid a = tid r /\
                              MappedToP Event is_critical r a n t)).
  Proof.
    intros.
    assert (CausalEvent C exe xr yr xyrace r) by eauto.
    destruct (lock_semantics_rel_isolated_section exe r); eauto.
    assert (Hl: PrefixLinearization (WitnessRelation C exe xr yr xyrace) t).
    eapply linearization_witness_relation_order; eauto.
    destruct (witness_linearization_causal_events_lock_semantics1
                exe xr yr xyrace t H Hl H1 r); auto.
    intuition.
    destruct (in_app Event t r); auto.
    destruct H8; subst.
    assert (Hx: CausalEvent C exe xr yr xyrace x) by eauto.
    assert (In x0 x2).
    eapply lt_hd_in_tl_app with (l1:=x1) (a:=r); auto.
    assert (Hx': In x x2).
    eapply lt_hd_in_tl_app with (l1:=x1) (a:=r); auto.
    apply Hl; eauto.
    apply witness_step; eauto.
    apply in_to_maps_top with (P:=is_critical) in H8.
    destruct H8. destruct H8.
    destruct (Event_eq_dec x3 x); subst.
    exists x, x4; intuition.
    eapply isolated_section_acq; eauto.
    eauto using isolated_section_thread_eq.
    eapply mapped_top_app_hd; eauto.
    unfold is_critical; auto.
    apply maps_top_lt_p with (P:=is_critical) (a:=x3) (na:=x4) in Hx' as H10.
    intuition.
    symmetry in H11; contradiction.
    assert (In x3 exe).
    eapply causal_event_in_exe; eauto.
    assert (Hc: is_critical x3).
    eapply maps_top_inv_p; eauto.
    destruct Hc.
    destruct (isolated_section_rel_exists_dec' exe x3); eauto.
    destruct H13.
    assert (Lt Event exe r x5 \/ Lt Event exe x5 r).
    apply lt_total_order; eauto.
    intuition; subst.
    contradict n.
    eauto using isolated_section_fun_acq.
    destruct H14.
    absurd (r = x3).
    intuition; subst.
    absurd (is_rel x3); auto.
    apply lt_antisymm with (x1 ++ r :: x2); auto.
    apply lt_hd_app.
    apply lt_hd; eauto.
    apply Hl; eauto.
    apply witness_critical1; eauto.
    eapply lock_semantics_critical_trace_order4; eauto.
    eapply isolated_section_causal_event_or_orphaned_acquire
      with (C:=C) (x:=xr) (y:=yr) (xyrace:=xyrace) in H13 as Hx3x5; eauto.
    destruct Hx3x5.
    contradict n.
    apply lt_antisymm with (x1 ++ r :: x2); auto.
    apply H0; eauto.
    apply witness_order_trans with x5; eauto.
    apply witness_step.
    apply program_order_causality.
    eauto using program_trace_rule.
    apply witness_order_step; auto.
    apply witness_critical1.
    eapply isolated_section_acq; eauto.
    eauto using isolated_section_rel.
    eapply lock_semantics_critical_trace_order4; eauto.

    absurd (x3 = r).
    intuition; subst.
    absurd (is_rel r); eauto.
    apply lt_antisymm with (x1 ++ r :: x2); auto.
    apply H0; eauto.
    apply witness_order_step; auto.
    inversion H15; subst; auto.
    apply witness_critical2; auto.
    apply ListOrder.lt_trans with x5; eauto.
    apply lt_app.
    apply lt_hd; eauto.

    assert (Lt Event exe r x3).
    eapply isolated_section_open_acquire_trace_order; eauto.
    absurd (r = x3).
    intuition; subst.
    absurd (is_rel x3); auto.
    apply lt_antisymm with (x1 ++ r :: x2); auto.
    apply lt_hd_app.
    apply lt_hd.
    eauto using maps_top_to_in.
    apply Hl; eauto.
    apply witness_critical1; auto.

    destruct (lock_semantics_rel_isolated_section exe x3); eauto.
    assert (Lt Event exe r x3 \/ Lt Event exe x3 r).
    apply lt_total_order; eauto.
    intuition; subst.
    assert (x5 = x).
    eauto using isolated_section_fun_acq.
    subst.
    absurd (In x3 x2).
    apply nodup_app_inv1 in H.
    inversion H; auto.
    eauto using maps_top_to_in.
    assert (Lt Event (x1 ++ r:: x2) x5 x3).
    apply Hl.
    apply in_or_app.
    right.
    apply in_cons.
    eauto using maps_top_to_in.
    eapply causal_event_in; eauto.
    eapply isolated_section_rel_causal_event_acq_causal_event; eauto.
    apply witness_step.
    apply program_order_causality.
    eauto using program_trace_rule.
    destruct H14.
    absurd (r = x5).
    intuition; subst.
    absurd (is_rel x5); auto.
    apply absurd_acq_rel.
    eapply isolated_section_acq; eauto.
    apply lt_antisymm with (x1 ++ r :: x2); auto.
    apply lt_hd_app.
    assert (In x5 (r :: x2)).
    eapply lt_tl_in_tl. eauto.
    apply in_cons.
    eapply maps_top_to_in with (x:=x3); eauto.
    auto.
    apply lt_hd.
    inversion H16; subst; auto.
    absurd (is_rel x5); auto.
    apply absurd_acq_rel.
    eapply isolated_section_acq; eauto.
    apply Hl; eauto.
    apply witness_critical1.
    eapply isolated_section_acq; eauto.
    eauto using isolated_section_rel.
    eapply lock_semantics_critical_trace_order4; eauto.
    assert (Lt Event exe x3 x).
    eapply lock_semantics_critical_trace_order4; eauto.
    contradict n.
    apply lt_antisymm with (x1 ++ r :: x2); auto.
    apply Hl; eauto.
    apply witness_critical1.
    eapply isolated_section_acq; eauto.
    eauto using isolated_section_rel.
    auto.
    apply Event_eq_dec.
    auto.
    unfold is_critical.
    left; eapply isolated_section_acq; eauto.
    apply is_critical_dec.
    unfold is_critical; auto.
  Qed.

  Lemma witness_linearization_causal_events_lock_semantics2 exe xr yr xyrace :
    forall t,
      NoDup t ->
      PrefixLinearization (WitnessOrder C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      (forall r1 r2, is_rel r1 -> is_rel r2 -> Lt Event t r1 r2 ->
                     (exists a, is_acq a /\ tid a = tid r2 /\ Lt Event t r1 a /\
                                Lt Event t a r2)).
  Proof.
    intros.
    assert (In r1 t /\ In r2 t) by eauto using lt_in; intuition.
    assert (In r2 exe) by eauto.
    destruct (lock_semantics_rel_isolated_section exe r2); eauto.
    exists x; intuition.
    eapply isolated_section_acq; eauto.
    eauto.
    apply H0; auto.
    eapply causal_event_in; eauto.
    apply witness_order_step; eauto.
    apply witness_critical1.
    eapply isolated_section_acq; eauto.
    eauto.
    eapply lock_semantics_critical_trace_order4 with (a:=x); eauto.
    assert (Lt Event exe r1 r2 \/ Lt Event exe r2 r1).
    apply lt_total_order; eauto.
    intuition.
    destruct (lock_semantics_rel_isolated_section exe r1); eauto.
    absurd (r1 = r2).
    intuition; subst; crush_list_order.
    apply lt_antisymm with (l:=t); auto.
    apply H0; auto.
    apply witness_order_trans with x0; eauto.
    apply witness_critical1; auto.
    eapply isolated_section_acq; eauto.
    eapply lock_semantics_critical_trace_order4; eauto.
    apply witness_order_step; eauto.
    apply witness_step; eauto.
    apply H0; eauto.
    apply witness_order_step; eauto.
    apply witness_step; eauto using isolated_section_to_program_order.
  Qed.
    
  Lemma orphaned_acquire_unique exe x y xyrace : forall a b,
      OrphanedAcquire C exe x y xyrace a ->
      OrphanedAcquire C exe x y xyrace b ->
      a = b.
  Proof.
    intros.
    assert (NoDup exe) by eauto.
    assert (is_access x /\ is_access y) by eauto using first_race_access; intuition.
    assert (In x exe /\ In y exe) by eauto using first_race_in; intuition.
    inversion H; subst.
    assert (r <> x).
    intuition; subst.
    absurd (is_access x); auto.
    eauto using isolated_section_rel.
    assert (In r exe).
    eauto using isolated_section_rel_in.
    assert (tid a = tid r) by eauto using isolated_section_thread_eq.
    inversion H0; subst.
    assert (r0 <> x).
    intuition; subst.
    absurd (is_access x); auto.
    eauto using isolated_section_rel.
    assert (In r0 exe).
    eauto using isolated_section_rel_in.
    assert (tid b = tid r0) by eauto using isolated_section_thread_eq.
    inversion H7; subst.
    inversion H13; subst.
    - destruct (Nat.eq_dec (tid a) (tid x)).
      apply causality_trace_order in H20 as Hax; intuition.
      destruct (lt_total_order Event exe H1 r x); auto.
      contradict H8.
      apply causal_event_x; auto.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (Nat.eq_dec (tid b) (tid x)).
      apply causality_trace_order in H23 as Hbx; intuition.
      destruct (lt_total_order Event exe H1 r0 x); auto.
      contradict H14.
      apply causal_event_x; auto.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      assert (ClosedIsolatedEvent a x r exe)
        by eauto using isolated_section_closed_isolated_event.
      assert (ClosedIsolatedEvent b x r0 exe)
        by eauto using isolated_section_closed_isolated_event.
      assert (a = b /\ r = r0) by eauto using closed_isolated_event_fun.
      intuition.
      destruct (causality_thread_neq_exists_rel exe b x); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r0 x0); subst.
      contradict H14.
      apply causal_event_x; auto.
      contradict H14.
      apply causal_event_rec with x0; auto.
      apply causal_event_x; eauto.
      intuition; subst.
      absurd (is_access x); auto.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with b; auto.
      eauto.
      inversion H26; subst; congruence.
      destruct (causality_thread_neq_exists_rel exe a x); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H8.
      apply causal_event_x; auto.
      contradict H8.
      apply causal_event_rec with x0; auto.
      apply causal_event_x; eauto.
      intuition; subst.
      absurd (is_access x); auto.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with a; auto.
      eauto.
      inversion H24; subst; congruence.
    - destruct (Nat.eq_dec (tid a) (tid x)).
      apply causality_trace_order in H20 as Hax; intuition.
      destruct (lt_total_order Event exe H1 r x); auto.
      contradict H8.
      apply causal_event_x; auto.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (Nat.eq_dec (tid b) (tid y)).
      apply causality_trace_order in H23 as Hbx; intuition.
      destruct (first_race_all_before_yr C exe x y xyrace r0); auto; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      contradict H14.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (causality_thread_neq_exists_rel exe b y); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r0 x0); subst.
      contradict H14.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); auto.
      contradict H14.
      apply causal_event_rec with x0; auto.
      apply causal_event_y; eauto.
      intuition; subst.
      absurd (is_access y); auto.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with b; auto.
      eauto.
      inversion H26; subst; congruence.
      destruct (causality_thread_neq_exists_rel exe a x); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H8.
      apply causal_event_x; auto.
      contradict H8.
      apply causal_event_rec with x0; auto.
      apply causal_event_x; eauto.
      intuition; subst.
      absurd (is_access x); auto.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with a; auto.
      eauto.
      inversion H24; subst; congruence.
    - inversion H13; subst.
      destruct (Nat.eq_dec (tid a) (tid y)).
      apply causality_trace_order in H20 as Hay; intuition.
      destruct (first_race_all_before_yr C exe x y xyrace r); auto; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      contradict H8.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (causality_thread_neq_exists_rel exe a y); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H8.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); auto.
      contradict H8.
      apply causal_event_rec with x0; auto.
      apply causal_event_y; eauto.
      intuition; subst.
      absurd (is_access y); auto.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with a; auto.
      eauto.
      inversion H24; subst; congruence.
      destruct (Nat.eq_dec (tid a) (tid y)).
      apply causality_trace_order in H20 as Hay; intuition.
      destruct (first_race_all_before_yr C exe x y xyrace r); auto; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      contradict H8.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (Nat.eq_dec (tid b) (tid y)).
      apply causality_trace_order in H23 as Hby; intuition.
      destruct (first_race_all_before_yr C exe x y xyrace r0); auto; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      contradict H14.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (causality_thread_neq_exists_rel exe b y); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r0 x0); subst.
      contradict H14.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); auto.
      contradict H14.
      apply causal_event_rec with x0; auto.
      apply causal_event_y; eauto.
      intuition; subst.
      absurd (is_access y); auto.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with b; auto.
      eauto.
      inversion H24; subst; congruence.
    - inversion H7; subst.
      destruct (Nat.eq_dec (tid a) (tid x)).
      destruct (lt_total_order Event exe H1 r x); auto.
      contradict H8.
      apply causal_event_x; auto.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      apply causality_trace_order in H16; intuition.
      assert (ClosedIsolatedEvent a x r exe)
             by eauto using isolated_section_closed_isolated_event.
      inversion H12; subst.
      apply causality_trace_order in H21 as Hbx; intuition.
      destruct (Nat.eq_dec (tid b) (tid x)).
      assert (OpenIsolatedEvent b x exe).
      apply open_isolated_event_t; auto.
      contradict H16; unfold not; intros.
      eapply absurd_closed_open_isolated_event with (t:=exe); eauto.
      destruct (causality_thread_neq_exists_rel exe b x); auto.
      apply absurd_acq_rel; eauto using open_acquire_maps_top_critical_acq.
      intuition.
      eapply open_acquire_acq_maps_top in H13 as Hm.
      destruct Hm as (?, Hm).
      destruct (maps_top_lt_p Event exe is_critical b x1 x0); auto.
      destruct (program_order_in exe b x0); auto.
      unfold is_critical; auto.
      subst.
      absurd (is_rel b); auto.
      apply absurd_acq_rel.
      eapply open_acquire_maps_top_critical_acq; eauto.
      inversion H23; subst.
      absurd (b = x0); eauto using lt_neq, lt_antisymm.
      apply causality_trace_order in H21 as Hby; intuition.
      destruct (Nat.eq_dec (tid b) (tid y)).
      assert (OpenIsolatedEvent b y exe).
      apply open_isolated_event_t; auto.
      destruct (race_one_not_isolated_event C exe x y); auto.
      inversion xyrace; auto.
      contradict H24; eauto using isolated_event_closed.
      contradict H24; eauto using isolated_event_open.
      destruct (causality_thread_neq_exists_rel exe b y); auto.
      apply absurd_acq_rel; eauto using open_acquire_maps_top_critical_acq.
      intuition.
      eapply open_acquire_acq_maps_top in H13 as Hm.
      destruct Hm as (?, Hm).
      destruct (maps_top_lt_p Event exe is_critical b x1 x0); auto.
      destruct (program_order_in exe b x0); auto.
      unfold is_critical; auto.
      subst.
      absurd (is_rel b); auto.
      apply absurd_acq_rel.
      eapply open_acquire_maps_top_critical_acq; eauto.
      inversion H23; subst.
      absurd (b = x0); eauto using lt_neq, lt_antisymm.
      destruct (causality_thread_neq_exists_rel exe a x); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H8.
      apply causal_event_x; auto.
      contradict H8.
      apply causal_event_rec with x0; auto.
      apply causal_event_x; eauto.
      intuition; subst; event_types.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with a; auto.
      eauto.
      inversion H17; subst; congruence.
      apply causality_trace_order in H16 as Hay; intuition.
      destruct (first_race_all_before_yr C exe x y xyrace r); auto.
      subst.
      absurd (is_access y); eauto using isolated_section_rel.
      destruct (Nat.eq_dec (tid a) (tid y)).
      contradict H8.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (causality_thread_neq_exists_rel exe a y); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H8.
      apply causal_event_y; auto.
      intuition; subst; event_types.
      contradict H8.
      apply causal_event_rec with x0; auto.
      apply causal_event_y; eauto.
      intuition; subst; event_types.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with a; auto.
      eauto.
      inversion H19; subst; congruence.
    - inversion H0; subst.
      assert (r <> x).
      intuition; subst.
      absurd (is_access x); auto.
      eauto using isolated_section_rel.
      assert (In r exe).
      eauto using isolated_section_rel_in.
      assert (tid b = tid r) by eauto using isolated_section_thread_eq.
      inversion H2; subst.
      apply causality_trace_order in H16 as Hax; intuition.
      destruct (Nat.eq_dec (tid a) (tid x)).
      assert (OpenIsolatedEvent a x exe).
      apply open_isolated_event_t; auto.
      inversion H9; subst.
      apply causality_trace_order in H21 as Hbx; intuition.
      destruct (Nat.eq_dec (tid b) (tid x)).
      destruct (lt_total_order Event exe H1 r x); auto.
      contradict H10.
      apply causal_event_x; auto.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      absurd (ClosedIsolatedEvent b x r exe).
      unfold not; intros.
      eapply absurd_closed_open_isolated_event with (t:=exe); eauto.
      eauto using isolated_section_closed_isolated_event.
      destruct (causality_thread_neq_exists_rel exe b x); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H10.
      apply causal_event_x; auto.
      contradict H10.
      apply causal_event_rec with x0; auto.
      apply causal_event_x; eauto.
      intuition; subst; event_types.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with b; auto.
      eauto.
      inversion H23; subst; congruence.
      apply causality_trace_order in H21 as Hby; intuition.
      destruct (Nat.eq_dec (tid b) (tid y)).
      destruct (first_race_all_before_yr C exe x y xyrace r); auto.
      subst.
      absurd (is_access y); eauto using isolated_section_rel.
      contradict H10.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (causality_thread_neq_exists_rel exe b y); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H10.
      apply causal_event_y; auto.
      intuition; subst; event_types.
      contradict H10.
      apply causal_event_rec with x0; auto.
      apply causal_event_y; eauto.
      intuition; subst; event_types.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with b; auto.
      eauto.
      inversion H23; subst; congruence.
      destruct (causality_thread_neq_exists_rel exe a x); auto.
      apply absurd_acq_rel.
      eapply open_acquire_maps_top_critical_acq; eauto.
      intuition.
      inversion H18; subst.
      assert (a <> x0) by eauto using lt_neq.
      eapply open_acquire_acq_maps_top in H7.
      destruct H7 as (?, H7).
      destruct (maps_top_lt_p Event exe is_critical a x1 x0); auto.
      destruct (lt_in Event exe a x0); auto.
      unfold is_critical; auto.
      symmetry in H24; contradiction.
      contradict H23.
      apply lt_antisymm with exe; auto.
      apply causality_trace_order in H16 as Hay; intuition.
      inversion H9; subst.
      destruct (Nat.eq_dec (tid a) (tid y)).
      assert (OpenIsolatedEvent a y exe).
      eapply open_isolated_event_t; eauto.
      apply causality_trace_order in H20 as Hbx; intuition.
      destruct (Nat.eq_dec (tid b) (tid x)).
      destruct (lt_total_order Event exe H1 r x); auto.
      contradict H10.
      apply causal_event_x; auto.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      absurd (ClosedIsolatedEvent b x r exe).
      unfold not; intros.
      destruct (race_one_not_isolated_event C exe x y); auto.
      inversion xyrace; auto.
      apply H25; eauto using isolated_event_closed.
      apply H25; eauto using isolated_event_open.
      eauto using isolated_section_closed_isolated_event.
      destruct (causality_thread_neq_exists_rel exe b x); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H10.
      apply causal_event_x; auto.
      contradict H10.
      apply causal_event_rec with x0; auto.
      apply causal_event_x; eauto.
      intuition; subst; event_types.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with b; auto.
      eauto.
      inversion H23; subst; congruence.
      destruct (causality_thread_neq_exists_rel exe a y); auto.
      apply absurd_acq_rel.
      eapply open_acquire_maps_top_critical_acq; eauto.
      intuition.
      inversion H21; subst.
      assert (a <> x0) by eauto using lt_neq.
      eapply open_acquire_acq_maps_top in H7.
      destruct H7 as (?, H7).
      destruct (maps_top_lt_p Event exe is_critical a x1 x0); auto.
      destruct (lt_in Event exe a x0); auto.
      unfold is_critical; auto.
      symmetry in H27; contradiction.
      contradict H26.
      apply lt_antisymm with exe; auto.
      apply causality_trace_order in H16 as Hay; intuition.
      apply causality_trace_order in H20 as Hby; intuition.
      destruct (Nat.eq_dec (tid a) (tid y)).
      destruct (Nat.eq_dec (tid b) (tid y)).
      assert (OpenIsolatedEvent a y exe).
      eapply open_isolated_event_t; eauto.
      destruct (first_race_all_before_yr C exe x y xyrace r); auto.
      subst.
      absurd (is_access y); eauto using isolated_section_rel.
      contradict H10.
      apply causal_event_y; auto.
      intuition; subst.
      absurd (is_access y); eauto using isolated_section_rel.
      apply program_order_causality.
      apply program_trace_rule; auto.
      congruence.
      destruct (causality_thread_neq_exists_rel exe b y); auto.
      apply absurd_acq_rel.
      eapply isolated_section_acq; eauto.
      intuition.
      destruct (Event_eq_dec r x0); subst.
      contradict H10.
      apply causal_event_y; auto.
      intuition; subst; event_types.
      contradict H10.
      apply causal_event_rec with x0; auto.
      apply causal_event_y; eauto.
      intuition; subst; event_types.
      apply program_trace_rule.
      apply lock_semantics_critical_trace_order1 with b; auto.
      eauto.
      inversion H23; subst; congruence.
      destruct (causality_thread_neq_exists_rel exe a y); auto.
      apply absurd_acq_rel.
      eapply open_acquire_maps_top_critical_acq; eauto.
      intuition.
      inversion H23; subst.
      eapply open_acquire_acq_maps_top in H7.
      destruct H7 as (?, H7).
      assert (a <> x0) by eauto using lt_neq.
      destruct (maps_top_lt_p Event exe is_critical a x1 x0); auto.
      destruct (lt_in Event exe a x0); auto.
      unfold is_critical; auto.
      symmetry in H29; contradiction.
      contradict H28.
      apply lt_antisymm with exe; auto.
      eapply open_acquire_acq_maps_top in H7.
      destruct H7 as (?, H7).
      eapply open_acquire_acq_maps_top in H9.
      destruct H9 as (?, H9).
      destruct (maps_top_lt_p Event exe is_critical a x0 b); auto.
      eauto using maps_top_to_in.
      eapply maps_top_inv_p; eauto.
      destruct (maps_top_lt_p Event exe is_critical b x1 a); auto.
      eauto using maps_top_to_in.
      eapply maps_top_inv_p; eauto.
      eauto using lt_antisymm.
  Qed.

  Lemma witness_linearization_causal_events_lock_semantics4 exe xr yr xyrace :
    forall t,
      NoDup t ->
      PrefixLinearization (WitnessOrder C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      (forall a,
          is_acq a ->
          In a t ->
          (forall r rn, MappedToP Event is_critical a r rn t -> is_rel r)).
  Proof.
    intros.
    assert (is_critical r) by (eapply mapped_top_inv_p; eauto).
    destruct H5; auto.
    assert (CausalEvent C exe xr yr xyrace a); eauto.
    assert (CausalEvent C exe xr yr xyrace r).
    eapply in_causal_event; eauto.
    destruct (mapped_top_to_in Event t is_critical a r rn); auto.
    assert (In r t); eauto.
    assert (a <> r).
    intuition; subst.
    contradict H4; eauto using absurd_nodup_mapped_top.
    assert (Lt Event t r a) by eauto using mapped_top_lt.
    destruct (isolated_section_rel_exists_dec' exe a); eauto.
    destruct (isolated_section_rel_exists_dec' exe r); eauto.
    - destruct H11, H12.
      assert (Lt Event exe x x0 \/ Lt Event exe x0 x).
      apply lt_total_order; eauto.
      intuition; subst.
      contradict H9; eauto using isolated_section_fun_acq.
      destruct H13.
      + assert (Lt Event exe a r).
        apply ListOrder.lt_trans with x; eauto.
        eapply lock_semantics_critical_trace_order4; eauto.

        eapply isolated_section_causal_event_or_orphaned_acquire
          with (C:=C) (x:=xr) (y:=yr) (xyrace:=xyrace) in H12 as Hrx0; eauto.
        eapply isolated_section_causal_event_or_orphaned_acquire
          with (C:=C) (x:=xr) (y:=yr) (xyrace:=xyrace) in H11 as Hax; eauto.
        intuition.

        contradict H9.
        apply lt_antisymm with t; auto.
        apply H0; auto.
        destruct (mapped_top_to_in Event t is_critical a r rn); auto.
        apply witness_order_trans with x; eauto.
        apply witness_step.
        apply program_order_causality.
        eauto using program_trace_rule.
        apply witness_order_step; auto.
        apply witness_critical1; eauto.
        eapply lock_semantics_critical_trace_order4; eauto.

        absurd (r = x0).
        eauto.
        apply lt_antisymm with t; auto.
        destruct (mapped_top_lt_p_not_overlapping Event t is_critical a r rn x0); auto.
        unfold is_critical; eauto.
        apply H0; eauto.
        apply witness_order_step; auto.
        apply witness_critical2; eauto.
        apply ListOrder.lt_trans with r; eauto.
        subst.
        absurd (is_rel r); eauto.
        apply H0; eauto.
        apply witness_order_step; auto.
        apply witness_step; eauto.

        contradict H9.
        apply lt_antisymm with t; auto.
        apply H0; auto.
        apply witness_order_trans with x; auto.
        apply witness_step; eauto.
        apply witness_order_step; auto.
        apply witness_critical1; eauto.
        eapply lock_semantics_critical_trace_order4; eauto.

        contradict H9; eauto using orphaned_acquire_unique.

      + assert (Lt Event exe x0 a).
        eapply lock_semantics_critical_trace_order4; eauto.
        assert (Lt Event exe r x0); eauto.
        assert (Lt Event exe a x); eauto.
        eapply isolated_section_causal_event_or_orphaned_acquire in H11 as Hx; eauto.
        eapply isolated_section_causal_event_or_orphaned_acquire in H12 as Hx0; eauto.
        intuition.
        * destruct (mapped_top_lt_p_not_overlapping Event t is_critical a r rn x0); auto.
          unfold is_critical; eauto using isolated_section_rel.
          apply H0; eauto.
          apply witness_order_step; eauto.
          apply witness_critical1.
          eauto using isolated_section_acq.
          eauto using isolated_section_rel.
          auto.
          subst.
          eauto using isolated_section_rel.
          absurd (x0 = r).
          eauto using lt_neq.
          apply lt_antisymm with t; auto.
          apply H0; eauto.
          apply witness_order_step; eauto.
          apply witness_step.
          eauto.
        * assert (WitnessRelation C exe xr yr xyrace x r).
          apply witness_critical2; auto.
          eauto using isolated_section_rel.
          apply ListOrder.lt_trans with x0; eauto.
          absurd (r = a).
          eauto using lt_neq.
          apply lt_antisymm with t; auto.
          apply H0; eauto.
          apply witness_order_trans with x; eauto.
          eauto using witness_step.
          eauto using witness_order_step.
        * destruct (mapped_top_lt_p_not_overlapping Event t is_critical a r rn x0); auto.
          unfold is_critical; eauto using isolated_section_rel.
          apply H0; eauto.
          apply witness_order_step; eauto.
          apply witness_critical1.
          eauto using isolated_section_acq.
          eauto using isolated_section_rel.
          auto.
          subst.
          eauto using isolated_section_rel.
          absurd (x0 = r).
          eauto using lt_neq.
          apply lt_antisymm with t; auto.
          apply H0; eauto.
          apply witness_order_step; eauto.
          apply witness_step.
          eauto.
        * contradict H9.
          eapply orphaned_acquire_unique; eauto.
    - destruct H11.
      eapply open_acquire_acq_maps_top in H12 as Hr.
      destruct Hr.
      destruct (maps_top_lt_p Event exe is_critical r x0 x); auto.
      eauto using isolated_section_rel_in.
      unfold is_critical; eauto using isolated_section_rel.
      subst; eauto using isolated_section_rel.

      eapply isolated_section_causal_event_or_orphaned_acquire
        with (C:=C) (x:=xr) (y:=yr) (xyrace:=xyrace) in H11 as Hax; eauto.
      intuition.

      absurd (r = a).
      eauto using lt_neq.
      apply lt_antisymm with t; auto.
      apply H0; auto.
      apply witness_order_trans with x; eauto.
      apply witness_step; eauto.
      apply witness_order_step; eauto.
      apply witness_critical1; auto.
      eauto using isolated_section_rel.

      contradict H9.
      eapply orphaned_acquire_unique; eauto.
      apply orphaned_acquire_open; auto.
    -
      destruct (isolated_section_rel_exists_dec' exe r); eauto.
      destruct H12.
      destruct (open_acquire_acq_maps_top exe a); auto.
      destruct (maps_top_lt_p Event exe is_critical a x0 x); auto.
      eauto using isolated_section_rel_in.
      unfold is_critical; eauto using isolated_section_rel.
      subst.
      absurd (is_acq a); eauto using isolated_section_rel.
      eapply isolated_section_causal_event_or_orphaned_acquire in H12 as Hx0; eauto.
      intuition.
      + destruct (mapped_top_lt_p_not_overlapping Event t is_critical a r rn x); auto.
        unfold is_critical; eauto using isolated_section_rel.
        apply H0; eauto.
        apply witness_order_step; eauto.
        apply witness_critical1; auto.
        eauto using isolated_section_rel.
        subst; eauto using isolated_section_rel.
        absurd (x = r).
        eauto using lt_neq.
        apply lt_antisymm with t; auto.
        apply H0; eauto.
        apply witness_order_step; eauto.
        apply witness_step; eauto.
      + absurd (r = a).
        eauto using lt_neq.
        eapply orphaned_acquire_unique; eauto.
        eapply orphaned_acquire_open; eauto.
      + contradict H9.
        eapply orphaned_acquire_unique.
        eapply orphaned_acquire_open; eauto.
        eapply orphaned_acquire_open; eauto.
  Qed.

  Lemma orphaned_acquire_acq : forall exe xr yr xyrace a,
      OrphanedAcquire C exe xr yr xyrace a ->
      is_acq a.
  Proof.
    inversion 1; subst.
    eapply isolated_section_acq; eauto.
    eapply open_acquire_maps_top_critical_acq; eauto.
  Qed.
  Hint Resolve orphaned_acquire_acq.
      
  Lemma witness_relation_antisymm : forall exe xr yr xyrace x y,
      CausalEvent C exe xr yr xyrace x ->
      CausalEvent C exe xr yr xyrace y ->
      WitnessRelation C exe xr yr xyrace x y ->
      WitnessRelation C exe xr yr xyrace y x ->
      x = y.
  Proof.
    intros.
    inversion H1; subst.
    - inversion H2; subst.
      + destruct (causality_trace_order exe x y); auto.
        eauto.
        destruct (causality_trace_order exe y x); auto.
        eauto.
        eauto using lt_antisymm.
      + destruct (causality_trace_order exe x y); auto.
        eauto.
        eauto using lt_antisymm.
      + destruct (causality_trace_order exe x y); auto.
        eauto.
        eauto using lt_antisymm.
      + destruct (Nat.eq_dec (tid x) (tid y)).
        destruct (lock_semantics_rel_isolated_section exe y); eauto.
        inversion H4; subst.
        assert (Lt Event exe r y).
        eapply lock_semantics_critical_trace_order1; eauto.
        intuition; subst; contradiction.
        contradict H10.
        apply causal_event_rec with y; auto.
        apply program_trace_rule; auto.
        assert (tid x0 = tid y) by eauto using isolated_section_thread_eq.
        assert (tid x = tid r) by eauto using isolated_section_thread_eq.
        congruence.
        destruct (open_acquire_acq_maps_top exe x); auto.
        destruct (maps_top_lt_p Event exe is_critical x x1 y); auto.
        eauto using isolated_section_rel_in.
        unfold is_critical; auto.
        eauto using lt_antisymm.
        destruct (lock_semantics_rel_isolated_section exe y); eauto.
        destruct (causality_thread_neq_exists_rel exe x y); auto.
        eauto.
        apply absurd_acq_rel.
        eapply orphaned_acquire_acq; eauto.
        intuition.
        inversion H4; subst.
        destruct (Event_eq_dec r y); subst.
        contradiction.
        destruct (Event_eq_dec r x1); subst.
        contradict H13.
        apply causal_event_rec' with y; eauto.
        contradict H13.
        apply causal_event_rec with x1; auto.
        apply causal_event_rec' with y; eauto.
        apply program_trace_rule.
        apply lock_semantics_critical_trace_order1 with x; eauto.
        assert (tid x0 = tid y) by eauto using isolated_section_thread_eq.
        assert (tid x = tid r) by eauto using isolated_section_thread_eq.
        inversion H8; subst; congruence.
        destruct (open_acquire_acq_maps_top exe x); auto.
        destruct (maps_top_lt_p Event exe is_critical x x2 x1); auto.
        destruct (program_order_in exe x x1); auto.
        unfold is_critical; auto.
        subst.
        absurd (is_acq x); auto.
        eapply orphaned_acquire_acq; eauto.
        absurd (x1 = x); eauto using lt_neq, lt_antisymm.
    - inversion H2; subst.
      + destruct (causality_trace_order exe y x); eauto using lt_antisymm.
      + eauto using lt_antisymm.
      + eauto using lt_antisymm.
      + absurd (is_access x).
        apply absurd_acq_access.
        eapply orphaned_acquire_acq; eauto.
        destruct (conflicts_access x y); auto.
    - inversion H2; subst.
      + destruct (causality_trace_order exe y x); eauto using lt_antisymm.
      + eauto using lt_antisymm.
      + event_types.
      + event_types.
    - inversion H2; subst.
      + destruct (causality_trace_order exe y x); auto.
        eauto.
        destruct (lock_semantics_rel_isolated_section exe x); eauto.
        inversion H3; subst.
        destruct (Event_eq_dec x r); subst.
        contradiction.
        assert (Lt Event exe r x).
        apply lock_semantics_critical_trace_order1 with y; eauto.
        destruct (Nat.eq_dec (tid x) (tid y)).
        contradict H11.
        apply causal_event_rec with x; auto.
        apply program_trace_rule; auto.
        assert (tid y = tid r) by eauto using isolated_section_thread_eq.
        congruence.
        destruct (causality_thread_neq_exists_rel exe y x); auto.
        eauto.
        apply absurd_acq_rel.
        eapply isolated_section_acq; eauto.
        intuition.
        destruct (Event_eq_dec r x1); subst.
        contradict H11.
        apply causal_event_rec' with x; eauto.
        contradict H11.
        apply causal_event_rec with x1; auto.
        apply causal_event_rec' with x; eauto.
        apply program_trace_rule.
        apply lock_semantics_critical_trace_order1 with y; eauto.
        assert (tid y = tid r) by eauto using isolated_section_thread_eq.
        inversion H13; subst; congruence.
        destruct (open_acquire_acq_maps_top exe y); auto.
        destruct (maps_top_lt_p Event exe is_critical y x1 x); auto.
        eauto using isolated_section_rel_in.
        unfold is_critical; auto.
        eauto using lt_antisymm.
      + absurd (is_rel x); auto.
        apply absurd_access_rel.
        destruct (conflicts_access y x); auto.
      + event_types.
      + eauto using lt_antisymm.
  Qed.
  
  Lemma witness_order_backwards_exists_orphaned_acquire : forall exe xr yr xyrace x y,
      WitnessOrder C exe xr yr xyrace x y ->
      Lt Event exe y x ->
      (exists a r, OrphanedAcquire C exe xr yr xyrace a /\
                   is_rel r /\
                   Lt Event exe a r /\
                   WitnessOrder C exe xr yr xyrace r a /\
                   WitnessOrder C exe xr yr xyrace x r /\
                   WitnessOrder C exe xr yr xyrace a y).
  Proof.
    induction 1; intros.
    crush_list_order.
    destruct (Event_eq_dec y z); subst.
    - inversion H2; subst.
      absurd (z = x); eauto.
      destruct (causality_trace_order exe x z); eauto.
      absurd (z = x); eauto.
      absurd (z = x); eauto.
      exists z, x; intuition.
      apply witness_order_step; auto.
      apply witness_order_refl; auto.
    - apply lt_total_order with (l:=exe) in n as HLt; eauto; intuition.
      + destruct (Event_eq_dec x y); subst.
        * contradict n; eauto.
        * apply lt_total_order with (l:=exe) in n0 as HLt; eauto; intuition.
          absurd (z = x); eauto.
          apply lt_antisymm with exe; eauto using ListOrder.lt_trans.
          inversion H2; subst.
          destruct (causality_trace_order exe x y); eauto; subst.
          contradiction.
          contradict n0; eauto.
          contradict n0; eauto.
          contradict n0; eauto.
          exists y, x; intuition.
          apply witness_order_step; auto.
          apply witness_order_refl; auto.
      + destruct H6.
        destruct H6.
        exists x0, x1; intuition.
        apply witness_order_trans with y; auto.
        inversion H9; subst; auto.
  Qed.

  Lemma orphaned_acquire_not_exists_rel_program_order_causal_event :
    forall exe xr yr xyrace a,
      OrphanedAcquire C exe xr yr xyrace a ->
      ~ (exists r, ProgramOrder exe a r /\ CausalEvent C exe xr yr xyrace r /\ is_rel r).
  Proof.
    intuition.
    destruct H0; intuition.
    destruct (lock_semantics_rel_isolated_section exe x); eauto.
    inversion H; subst.
    destruct (Event_eq_dec x r); subst.
    contradiction.
    contradict H6.
    apply causal_event_rec with x; auto.
    apply program_trace_rule.
    apply lock_semantics_critical_trace_order1 with a; eauto.
    assert (tid a = tid r) by eauto.
    inversion H1; subst; congruence.
    absurd (a = x).
    eauto.
    apply lt_antisymm with exe; eauto.
    apply isolated_section_open_acquire_trace_order with x0; eauto.
  Qed.
    
      
  Lemma orphaned_acquire_witness_order_causality : forall exe xr yr xyrace a x y,
      OrphanedAcquire C exe xr yr xyrace a ->
      x = a \/ ProgramOrder exe a x ->
      WitnessOrder C exe xr yr xyrace x y ->
      C exe x y.
  Proof.
    intros.
    eapply orphaned_acquire_not_exists_rel_program_order_causal_event in H as Ha; eauto.
    induction H1.
    eauto.

    intuition; subst.
    destruct (Event_eq_dec a y); subst; auto.
    inversion H4; subst.
    destruct (causality_trace_order exe a y); subst; eauto.
    destruct (Nat.eq_dec (tid a) (tid y)); eauto.
    destruct (causality_thread_neq_exists_rel exe a y); eauto.
    apply absurd_acq_rel.
    eapply orphaned_acquire_acq; eauto.
    intuition.
    contradict Ha.
    exists x; intuition.
    apply causal_event_rec' with y; auto.
    destruct (program_order_in exe a x); auto.
    absurd (is_acq a).
    apply absurd_access_acq.
    destruct (conflicts_access a y); auto.
    eapply orphaned_acquire_acq; eauto.
    absurd (is_acq a); auto.
    eapply orphaned_acquire_acq; eauto.
    contradict n; eauto using orphaned_acquire_unique.
    inversion H4; subst.
    destruct (Event_eq_dec a y); subst; eauto.
    destruct (causality_trace_order exe x y); subst; eauto.
    destruct (Nat.eq_dec (tid a) (tid y)).
    apply causality_trans with y; auto.
    apply H7.
    apply program_trace_rule; auto.
    apply ListOrder.lt_trans with x; inversion H6; subst; eauto.
    destruct (causality_thread_neq_exists_rel exe x y); auto.
    eauto.
    intuition; inversion H6; subst; congruence.
    intuition.
    contradict Ha.
    exists x; intuition.
    intuition.
    contradict Ha.
    exists x0; intuition.
    apply program_trace_rule.
    apply ListOrder.lt_trans with x; eauto.
    inversion H10; inversion H6; subst; congruence.
    apply causal_event_rec' with y; auto.
    destruct (program_order_in exe x x0); auto.

    assert (C exe x y).
    inversion xyrace; subst.
    apply causality_cons.
    eauto.
    apply H11; auto.
    apply lt_tl with yr; auto.
    intuition; subst.
    contradict H2; eauto using first_race_yr_not_causal_event.
    destruct (Nat.eq_dec (tid a) (tid y)).
    apply causality_trans with y; auto.
    apply H7.
    apply program_trace_rule; auto.
    apply ListOrder.lt_trans with x; inversion H6; subst; eauto.
    destruct (causality_thread_neq_exists_rel exe x y); auto.
    eauto.
    intuition; inversion H6; subst; congruence.
    intuition.
    contradict Ha.
    exists x; intuition.
    intuition.
    contradict Ha.
    exists x0; intuition.
    apply program_trace_rule.
    apply ListOrder.lt_trans with x; eauto.
    inversion H11; inversion H6; subst; congruence.
    apply causal_event_rec' with y; auto.
    destruct (program_order_in exe x x0); auto.

    contradict Ha.
    exists x; intuition.
    contradict Ha.
    exists x; intuition.
  Qed.

  Lemma orphaned_acquire_witness_order_program_order : forall exe xr yr xyrace a x y,
      OrphanedAcquire C exe xr yr xyrace a ->
      x = a \/ ProgramOrder exe a x ->
      WitnessOrder C exe xr yr xyrace x y ->
      x = y \/ ProgramOrder exe x y.
  Proof.
    intros.
    eapply orphaned_acquire_not_exists_rel_program_order_causal_event in H as Ha; eauto.
    induction H1; auto.
    intuition; subst.
    destruct (Event_eq_dec a y); subst; auto.
    inversion H4; subst.
    destruct (causality_trace_order exe a y); subst; eauto.
    destruct (Nat.eq_dec (tid a) (tid y)); eauto.
    destruct H7; auto.
    subst.
    right; auto.
    right.
    apply program_trace_rule.
    eauto using ListOrder.lt_trans.
    inversion H7; subst; congruence.
    destruct (causality_thread_neq_exists_rel exe a y); eauto.
    apply absurd_acq_rel.
    eapply orphaned_acquire_acq; eauto.
    intuition.
    contradict Ha.
    exists x; intuition.
    apply causal_event_rec' with y; auto.
    destruct (program_order_in exe a x); auto.
    absurd (is_acq a).
    apply absurd_access_acq.
    destruct (conflicts_access a y); auto.
    eapply orphaned_acquire_acq; eauto.
    absurd (is_acq a); auto.
    eapply orphaned_acquire_acq; eauto.
    contradict n; eauto using orphaned_acquire_unique.
    inversion H4; subst.
    destruct (Event_eq_dec a y); subst.
    destruct H0; auto; subst.
    left.
    destruct (causality_trace_order exe x z); eauto.
    assert (x = y).
    destruct (causality_trace_order exe x y); eauto.
    subst.
    auto.
    destruct (causality_trace_order exe x y); subst; eauto.
    destruct (Nat.eq_dec (tid a) (tid y)).
    destruct H7.
    apply program_trace_rule; eauto using ListOrder.lt_trans.
    subst.
    destruct (causality_trace_order exe x z); eauto.
    right.
    apply program_trace_rule; auto.
    inversion H6; subst; congruence.
    right.
    apply program_trace_rule.
    eauto using ListOrder.lt_trans.
    inversion H6; inversion H7; subst; congruence.
    destruct (causality_thread_neq_exists_rel exe x y); eauto.
    intuition; inversion H6; subst; congruence.
    intuition.
    contradict Ha.
    exists x0; intuition.
    apply program_trace_rule.
    apply ListOrder.lt_trans with x; eauto.
    inversion H6; inversion H10; subst; congruence.
    apply causal_event_rec' with y; auto.
    destruct (program_order_in exe x x0); auto.

    assert (C exe a y).
    apply causality_trans with x.
    eauto.
    inversion xyrace; subst.
    apply causality_cons.
    eauto.
    apply H11; auto.
    apply lt_tl with yr; auto.
    intuition; subst.
    contradict H2; eauto using first_race_yr_not_causal_event.
    destruct (causality_trace_order exe a y); subst; eauto.
    absurd (x = y); eauto.
    destruct (Nat.eq_dec (tid a) (tid y)); eauto.
    destruct H7; auto.
    subst.
    right.
    apply program_trace_rule; auto.
    inversion H6; subst; congruence.
    right.
    apply program_trace_rule.
    eauto using ListOrder.lt_trans.
    inversion H7; inversion H6; subst; congruence.
    destruct (causality_thread_neq_exists_rel exe a y); eauto.
    apply absurd_acq_rel.
    eapply orphaned_acquire_acq; eauto.
    intuition.
    contradict Ha.
    exists x0; intuition.
    apply causal_event_rec' with y; auto.
    destruct (program_order_in exe a x0); auto.
    contradict Ha.
    exists x; intuition.
    contradict Ha.
    exists x; intuition.
  Qed.

  Theorem witness_order_antisymm : forall exe xr yr xyrace x y,
      WitnessOrder C exe xr yr xyrace x y ->
      WitnessOrder C exe xr yr xyrace y x ->
      x = y.
  Proof.
    intros.
    destruct (Event_eq_dec x y); subst; auto.
    apply lt_total_order with (l:=exe) in n as HLt; intuition.
    apply witness_order_backwards_exists_orphaned_acquire
      with (exe:=exe) (xr:=xr) (yr:=yr) (xyrace:=xyrace) in H0; auto.
    destruct H0 as (a, H0). destruct H0 as (r, H0); intuition.
    eapply orphaned_acquire_not_exists_rel_program_order_causal_event in H2 as Ha; eauto.
    assert (a = x \/ ProgramOrder exe a x).
    eapply orphaned_acquire_witness_order_program_order; eauto.
    assert (x = y \/ ProgramOrder exe x y).
    eapply orphaned_acquire_witness_order_program_order; eauto.
    intuition.
    assert (y = r \/ ProgramOrder exe y r).
    eapply orphaned_acquire_witness_order_program_order; eauto.
    intuition; subst; auto.
    inversion H9; inversion H6; subst; right.
    apply program_trace_rule.
    eauto using ListOrder.lt_trans.
    congruence.
    assert (CausalEvent C exe xr yr xyrace r) by (inversion H4; subst; auto).
    intuition; subst.
    contradict Ha; exists r; intuition.
    contradict Ha; exists r; intuition.
    inversion H6; inversion H8; subst.
    apply program_trace_rule; congruence + (eauto using ListOrder.lt_trans).
    contradict Ha; exists r; intuition.
    inversion H11; inversion H6; subst.
    apply program_trace_rule; congruence + (eauto using ListOrder.lt_trans).
    contradict Ha; exists r; intuition.
    inversion H6; inversion H8; inversion H11; subst.
    apply program_trace_rule; congruence + (eauto using ListOrder.lt_trans).

    apply witness_order_backwards_exists_orphaned_acquire
      with (exe:=exe) (xr:=xr) (yr:=yr) (xyrace:=xyrace) in H; auto.
    destruct H as (a, H). destruct H as (r, H); intuition.
    eapply orphaned_acquire_not_exists_rel_program_order_causal_event in H2 as Ha; eauto.
    assert (a = y \/ ProgramOrder exe a y).
    eapply orphaned_acquire_witness_order_program_order; eauto.
    assert (y = x \/ ProgramOrder exe y x).
    eapply orphaned_acquire_witness_order_program_order; eauto.
    intuition.
    assert (x = r \/ ProgramOrder exe x r).
    eapply orphaned_acquire_witness_order_program_order; eauto.
    intuition; subst; auto.
    inversion H9; inversion H6; subst; right.
    apply program_trace_rule; congruence + (eauto using ListOrder.lt_trans).
    assert (CausalEvent C exe xr yr xyrace r) by (inversion H4; subst; auto).
    intuition; subst.
    contradict Ha; exists r; intuition.
    contradict Ha; exists r; intuition.
    inversion H6; inversion H8; subst.
    apply program_trace_rule; congruence + (eauto using ListOrder.lt_trans).
    contradict Ha; exists r; intuition.
    inversion H6; inversion H11; subst.
    apply program_trace_rule; congruence + (eauto using ListOrder.lt_trans).
    contradict Ha; exists r; intuition.
    inversion H6; inversion H8; inversion H11; subst.
    apply program_trace_rule; congruence + (eauto using ListOrder.lt_trans).
    eauto.
    eapply causal_event_in_exe.
    inversion H; subst; eauto.
    eapply causal_event_in_exe.
    inversion H; subst; eauto.
  Qed.

  Lemma witness_linearization_thread_prefix exe xr yr xyrace : forall t,
      PrefixLinearization (WitnessRelation C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      ThreadPrefix exe t.
  Proof.
    unfold ThreadPrefix; intros.
    apply program_trace_rule.
    apply H; auto.
    eapply causal_event_in; eauto.
    apply causal_event_rec with y; auto.
    eapply in_causal_event; eauto.
    apply witness_step; auto.
    inversion H2; auto.
  Qed.

  Lemma witness_linearization_causal_events_same_observations exe xr yr xyrace :
    forall t,
      NoDup t ->
      PrefixLinearization (WitnessRelation C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      SameObservations exe t.
  Proof.
    unfold SameObservations; intros.
    apply same_observations_t with exe; auto.
    eapply causal_event_in; eauto; eauto using causal_event_observes.
    intros.
    eapply conflicts_causal_events_same_order; eauto.
    intros.
    destruct (in_app Event exe r).
    eauto using causal_event_in_exe.
    destruct H6; subst.
    apply observes_write_tls in H3.
    inversion H3; subst.
    assert (e = w \/ Lt Event (r :: x0) e w).
    eapply maps_top_lt_p with (P:=(conflicts r)).
    apply Event_eq_dec.
    apply maps_top_cons; eauto.
    simpls; intuition; event_types.
    apply in_cons.
    eapply lt_hd_in_tl_app; eauto.
    eapply conflicts_causal_events_same_order; eauto.
    auto.
    intuition.
    apply lt_app with (t1:=x) in H8.
    right.
    rewrite <- conflicts_causal_events_same_order; eauto.
    eapply causal_event_observes; eauto.
    apply observes_write_app; auto.
    apply conflicts_trans with r; eauto using conflicts_symm, observes_write_conflicts.
    right; eauto using observes_write_write.
    assert (NoDup (x ++ r :: x0)) by eauto.
    apply nodup_app_inv1 in H6.
    inversion H6; subst.
    contradict H10; eauto using observes_write_read_in.
    eauto.
  Qed.

  Lemma witness_linearization_causal_events_lock_semantics exe xr yr xyrace :
    forall t,
      NoDup t ->
      PrefixLinearization (WitnessOrder C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      LockSemantics t.
  Proof.
    intros.
    apply lock_semantics_t; auto.
    eapply witness_linearization_causal_events_lock_semantics2; eauto.
    eapply witness_linearization_causal_events_lock_semantics3; eauto.
    eapply witness_linearization_causal_events_lock_semantics4; eauto.
  Qed.

  Lemma witness_order_transitive : forall exe xr yr xyrace x y z,
      WitnessOrder C exe xr yr xyrace x y ->
      WitnessOrder C exe xr yr xyrace y z ->
      WitnessOrder C exe xr yr xyrace x z.
  Proof.
    intros.
    induction H; auto.
    inversion H0; subst; auto.
    apply witness_order_trans with y; auto.
    apply witness_order_trans with y; auto.
  Qed.

  Lemma first_race_xr_enabled : forall exe exetrace xr yr xyrace t,
      PrefixLinearization (WitnessOrder C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      Enabled exe exetrace t xr.
  Proof.
    intros.
    constructor.
    eapply witness_linearization_thread_prefix; eauto.
    eapply linearization_witness_relation_order; eauto.
    intuition.
    absurd (CausalEvent C exe xr yr xyrace xr).
    eapply first_race_xr_not_causal_event; eauto.
    eauto.
    intros.
    eapply causal_event_in; eauto.
    apply causal_event_x; eauto.
  Qed.

  Lemma first_race_yr_enabled : forall exe exetrace xr yr xyrace t,
      PrefixLinearization (WitnessOrder C exe xr yr xyrace) t ->
      CausalEvents C exe xr yr xyrace t ->
      Enabled exe exetrace t yr.
  Proof.
    intros.
    constructor.
    eapply witness_linearization_thread_prefix; eauto.
    eapply linearization_witness_relation_order; eauto.
    intuition.
    absurd (CausalEvent C exe xr yr xyrace yr).
    eapply first_race_yr_not_causal_event; eauto.
    eauto.
    intros.
    eapply causal_event_in; eauto.
    apply causal_event_y; eauto.
  Qed.
End WitnessTrace.
